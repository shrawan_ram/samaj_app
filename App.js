/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import {
  Pressable,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import Icon from 'react-native-ico-material-design';
import ComingSoonScreen from './src/ComingSoonScreen';
import HeaderRightComponent from './src/component/HeaderRightComponent';
import SearchScreenHeader from './src/component/SearchScreenHeader';
import PostScreen from './src/home/PostScreen';
import LoginScreen from './src/login/LoginScreen';
import RegisterScreen from './src/login/RegisterScreen';
import MainTabScreen from './src/MainTabScreen';
import NewsDetailScreen from './src/news/NewsDetailScreen';
import NotificationScreen from './src/notification/NotificationScreen';
import PlaceDetailScreen from './src/place/PlaceDetailScreen';
import SearchScreen from './src/search/SearchScreen';
import OtpVerifyScreen from './src/login/OtpVerifyScreen';
import SendOtpScreen from './src/login/SendOtpScreen';
import ForgotPasswordScreen from './src/login/ForgotPasswordScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import EditProfileScreen from './src/profile/EditProfileScreen';
import ConnectionScreen from './src/profile/ConnectionScreen';
import AddPost from './src/post/AddPost';
import SecondRegisterScreen from './src/login/SecondRegisterScreen';
import ChangePassword from './src/profile/ChangePassword';
import ProfileScreen from './src/profile/ProfileScreen';

const Stack = createStackNavigator();


export default class App extends Component{
  state={
    screenText:'Press a botton',
    token: '',
    loading: false
  }
  changeText = (text) => {
    console.log(text + 'has been pressed')
    this.setState({
      screenText :  text
    })
  }
  async componentDidMount () {
    let token = await AsyncStorage.getItem('@token');
    token = token !== null ? token : '';
    this.setState({token: token});

    let user = await AsyncStorage.getItem('@user');
    user = user !== null ? JSON.parse(user) : '';
    this.setState({user_detail: user});

    
}
  render(){
    let token = this.state.token;
    setTimeout(() => {
      this.setState({ loading: true })
    }, 3000)
    
    //  let {navigation} = this.props;
     if (this.state.loading) {
    return (
      <NavigationContainer>
        <StatusBar backgroundColor="#ce4061" barStyle="light-content" />
        <Stack.Navigator
        initialRouteName={ token == '' ? "Login" : "Home"}
        >
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={({navigation}) => ({
            headerShown:false, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            
          })}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={({navigation}) => ({
            headerShown:false, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'Place',
            
          })}
        />
        <Stack.Screen
          name="SecondRegister"
          component={SecondRegisterScreen}
          options={({navigation}) => ({
            headerShown:false, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'Place',
            
          })}
        />
        <Stack.Screen
          name="ChangePassword"
          component={ChangePassword}
          options={({navigation}) => ({
            headerShown:false, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'Change Password',
            
          })}
        />
        <Stack.Screen
          name="OtpVerify"
          component={OtpVerifyScreen}
          options={({navigation}) => ({
            headerShown:true, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'OTP Verify',
            
          })}
        />
        <Stack.Screen
          name="SendOtp"
          component={SendOtpScreen}
          options={({navigation}) => ({
            headerShown:true, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'Send OTP',
            
          })}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPasswordScreen}
          options={({navigation}) => ({
            headerShown:true, 
            headerStyle: { backgroundColor: '#ce4061' }, 
            headerTintColor: '#fff', 
            headerTitle:'New Password',
            
          })}
        />
        <Stack.Screen 
            name="Home" 
            component={MainTabScreen} 
            options={({navigation}) => ({
              headerShown:true, 
              headerLeft:false,
              headerRight: () => <HeaderRightComponent onPress={(target) => navigation.navigate(target)} />, 
              headerStyle: { backgroundColor: '#ce4061' }, 
              headerTintColor: '#fff', 
              headerTitle:'JAT Samaj',
              headerTitleStyle:{
                color:'#fff',
                fontFamily:'monospace',
                fontWeight:'bold',
                fontSize:30
              }  
            })} />
            
            <Stack.Screen
              name="Notification"
              component={NotificationScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Notification',
                
              })}
            />
            <Stack.Screen
              name="Post"
              component={PostScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Post',
                
              })}
            />
            <Stack.Screen
              name="EditProfile"
              component={EditProfileScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Edit Profile',
                
              })}
            />
            
            <Stack.Screen
              name="NewsDetail"
              component={NewsDetailScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'News',
                
              })}
            />
            <Stack.Screen
              name="ComingSoon"
              component={ComingSoonScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Coming Soon',
                
              })}
            />
            <Stack.Screen
              name="SearchScreen"
              component={SearchScreen}
              options={({navigation}) => ({
                headerShown:true, 
                // headerRight: () => <SearchScreenHeader />,
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Search Users',
                
              })}
            />
            <Stack.Screen
              name="Connections"
              component={ConnectionScreen}
              options={({navigation}) => ({
                headerShown:true, 
                // headerRight: () => <SearchScreenHeader />,
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Connections',
                
              })}
            />
            <Stack.Screen
              name="PlaceDetail"
              component={PlaceDetailScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Place',
                
              })}
            />
            <Stack.Screen
              name="AddPost"
              component={AddPost}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Add Post',
                
              })}
            />
            <Stack.Screen
              name="Profile"
              component={ProfileScreen}
              options={({navigation}) => ({
                headerShown:true, 
                headerStyle: { backgroundColor: '#ce4061' }, 
                headerTintColor: '#fff', 
                headerTitle:'Profile',
                
              })}
            />
          {/* <Stack.Screen name="Notification" component={NotificationScreen} options={{ headerShown:()=><View><Text>Hello</Text><Text>Hello</Text></View> }} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    );
  } else {
    return (
      <View style={{flex:1,backgroundColor:'#ce4061',justifyContent:'center'}}>
        <StatusBar backgroundColor="#ce4061" barStyle="light-content" />
        <View>
          <Text style={{ color: 'black',textAlign:'center',fontSize:22 }}>Loading...</Text>
        </View>
      </View>
    )
  }
  }
}


const styles = StyleSheet.create({
  
  container:{
    flex:1,
    backgroundColor:'pink',
    alignItems:'center',
    justifyContent:'center'
  },
  NavContainer:{
    backgroundColor:'#ce4061',
    position:'absolute',
    alignItems:'center',
    bottom:0,
    left:0,
    right:0,
  },
  NavBar: {
    flexDirection:'row',
    
    justifyContent:'space-evenly',
    borderRadius: 40
  },
  IconBehave: {
    paddingVertical:20,
    paddingHorizontal:28
  }
});
