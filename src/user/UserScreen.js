import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import {View, Text, ScrollView, SafeAreaView, Image, Modal, Pressable, TouchableOpacity} from "react-native";
import Icon from "react-native-ico-material-design";
import RBSheet from "react-native-raw-bottom-sheet";
import { add_connection, get_users } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";

export default class UserScreen extends Component {
    state = {
        auth_user_detail: '',
        users           : [ ],
        modalVisible    : false,
    }

    getUsers = async() => {
        let response = await get_users();
        if(response.data.status){
            this.setState({users: response.data.data});
        }
    }
    addConnection = async(connection_id, status) => {
        let data = {
            user_id         : this.state.auth_user_detail.id,
            connection_id   : connection_id,
            status          : status,
        }
        let response = await add_connection(data);
        // console.log('data',data);
        // if(response.data.status){
        //     this.setState({users: response.data.data});
        // }
    }

    async componentDidMount(){
        this.getUsers();

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});
    }
    render(){
        let image_url = url + 'public/storage/';
        let { navigation } = this.props;
        return(
            <SafeAreaView>
                <ScrollView>
                    <View style={styles.container}>
                        {
                            this.state.users.map((user, index) => {
                                return(
                                    <>                                                   
                                        <View style={styles.notification_container}>
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                    <TouchableOpacity onPress={() => navigation.navigate('Profile',user)}>
                                                        <Image source={{uri: image_url+user.image}} style={styles.post_profile_image} />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                    <TouchableOpacity onPress={() => navigation.navigate('Profile',user)}>
                                                        <Text style={{fontWeight:'bold',fontSize:16}}>{user.user_name} </Text>
                                                        <Text style={{color:'#aaa'}}>{user.fname} {user.lname}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                    <TouchableOpacity onPress={()=>this.addConnection(user.id, user.privacy_status == 'private' ? 'requested' : 'followed')}>
                                                        <Text style={styles.follow_btn}>Follow</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                       
                                        {/* <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                                            <View style={styles.notification_container}>
                                                <View style={{flexDirection:'row'}}>
                                                    <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                        <Image source={user.dp} style={styles.post_profile_image} />
                                                    </View>
                                                    
                                                    <View style={{flex:.64, paddingHorizontal:10,justifyContent:'center',}}>
                                                    <Text><Text style={{fontWeight:'bold',fontSize:16}}>{user.name} </Text>{user.desc}. <Text style={{color:'#ccc'}}>{user.time}</Text></Text>
                                                        
                                                    </View>
                                                    <View style={{flex:.21, justifyContent:'center',alignItems:'flex-end'}}>
                                                        <TouchableOpacity>
                                                            <Text style={styles.following_btn}>Following</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity> */}
                                        
                                    </>

                                    
                                )
                            })
                        }
                        
                        {/* <View style={styles.notification_container}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                    <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                </View>
                                
                                <View style={{flex:.7, paddingHorizontal:10,justifyContent:'center',}}>
                                <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                </View>
                                <View style={{flex:.15, justifyContent:'center',alignItems:'flex-end'}}>
                                    <TouchableOpacity>
                                        <Text style={styles.follow_btn}>Follow</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={styles.notification_container}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                    <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                </View>
                                
                                <View style={{flex:.64, paddingHorizontal:10,justifyContent:'center',}}>
                                <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                </View>
                                <View style={{flex:.21, justifyContent:'center',alignItems:'flex-end'}}>
                                    <TouchableOpacity>
                                        <Text style={styles.following_btn}>Following</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={styles.notification_container}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                    <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                </View>
                                
                                <View style={{flex:.8, paddingHorizontal:10,justifyContent:'center',}}>
                                    <Text style={{fontWeight:'bold',fontSize:16}}>Jaipal Saini</Text>
                                </View>
                                <View style={{flex:.1, justifyContent:'center',alignItems:'flex-end'}}>
                                    <Icon name='show-more-button-with-three-dots' width="50%" />
                                </View>
                            </View>
                        </View> */}

                        
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}