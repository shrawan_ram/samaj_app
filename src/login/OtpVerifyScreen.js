import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import CodeInput from "react-native-confirmation-code-input";
import { verifyOtp } from "../api/auth.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class OtpVerifyScreen extends Component{
    state={
        user_name    : '',
        password: '',
        mobile:'',
        otp_code:'',
    }

    verifyOtp = async () => {
        let {route} = this.props;
        let type = route.params ? route.params.type : '';
        if(type == 'register'){

            let data = {
              user_name : route.params.user_d.user_name,
              otp : this.state.otp_code,
          }
          
            let response = await verifyOtp(data);
            if(response.data.status){
                
                this.props.navigation.push('Login'); 
            }
        } else {
            let data = {
                user_name : route.params.user_name,
                otp : this.state.otp_code,
            }
            
              let response = await verifyOtp(data);
              if(response.data.status){
                  
                  this.props.navigation.push('ForgotPassword',response.data.data); 
              }
        }
    
      }
    async componentDidMount(){
        let {route } = this.props;
        this.setState({user_name:route.params.user_name});







        
        this.setState({mobile:route.params.otp});
    }
    render(){
        let {navigation} = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            <View style={[styles.container,{justifyContent:'center'}]}>
                
                        
                <View style={{paddingHorizontal:15}}>
                <View style={styles.section}>
                <View style={{padding:15,backgroundColor:'#ce4061aa',borderRadius:50,textAlign:'center',marginVertical:30}}>
                        <Image source={require('../../assests/images/otp.png')} style={styles.post_profile_image} />
                    </View>
                    
                    <Text style={{fontSize:40,fontWeight:'bold'}}>Verification</Text>
                    <Text style={{color:'#444b55',fontSize:16,paddingVertical:10}}>You will get a OTP via SMS</Text>
                    <Text style={{fontSize:19,fontWeight:'bold'}}>{this.state.mobile}</Text>
                    

                    <CodeInput
                        ref="codeInputRef1"
                        codeLength={6}
                        // secureTextEntry
                        activeColor='#000'
                        inactiveColor='#000'
                        className={'border-b'}
                        space={6}
                        size={40}
                        keyboardType='numeric'
                        inputPosition='left'
                        onFulfill={(otp_code) => this.setState({otp_code})}
                        // onCodeChange={(otp_code) => this.setState({otp_code})}
                        />
                                     
                    <Text style={{color:'#444b55',marginTop:90,fontSize:16,marginBottom:10}}>Didn't not receive the OTP?<Text  style={{fontSize:19,color:'#ce4061',fontWeight:'bold'}}> Resend OTP</Text></Text>
                    </View>
                    <TouchableOpacity onPress={() => this.verifyOtp()}>
                        <Text style={styles.submit_btn}>VERIFY</Text>
                    </TouchableOpacity>
                    
                </View>
            </View>
        )
    }
}