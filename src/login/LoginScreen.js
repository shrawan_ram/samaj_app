import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { login } from "../api/auth.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class LoginScreen extends Component{
    state={
        user_name       : '',
        errorUsername   : true,
        password        : '',
        errorPassword   : true,

    }

    login = async () => {
       
        let data = {
          user_name : this.state.user_name,
          password : this.state.password
      }
        
        
      
      let response = '';
      if(this.state.user_name != ''){
        this.setState({errorUsername: true});
      } else {
        this.setState({errorUsername: false});
      }
      if(this.state.password != ''){
        this.setState({errorPassword: true});
      } else {
        this.setState({errorPassword: false});
      }
      if(this.state.user_name == '' || this.state.password == ''){
        
        
          
      }else{
    
        response = await login(data);
      }
        console.log('response',response);
        // console.log('res',response.data.status);
        if (response.data.status == false) {
          alert(response.data.message);
        }
        if(response.data.status){
            
            await AsyncStorage.setItem('@user', JSON.stringify(response.data.data));        
            await AsyncStorage.setItem('@token', response.data.token);
            // await AsyncStorage.setItem('@type', type);
            
            this.props.navigation.push('Home'); 
        }
    
      }

    render(){
        let {navigation} = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            // <View style={styles.container}>
            //     <View style={{paddingHorizontal:15}}>
            //         <Text style={{fontSize:35,fontWeight:'bold'}}>Login</Text>
            //         <TextInput style = {styles.input}
            //             underlineColorAndroid = "transparent"
            //             placeholder = "Username"
            //             placeholderTextColor = "#000"
            //             selectionColor={'#000'}
            //             autoCapitalize = "none"
            //             value={this.state.name}  
            //             onChangeText={name => this.setState({ name })}
            //         />
            //         <TextInput style = {styles.input}
            //             underlineColorAndroid = "transparent"
            //             placeholder = "Password"
            //             placeholderTextColor = "#000"
            //             selectionColor={'#000'}
            //             secureTextEntry={true}
            //             autoCapitalize = "none"
            //             value={this.state.password}  
            //             onChangeText={password => this.setState({ password })}
            //         />
            //         <TouchableOpacity>
            //             <Text style={{color:'#ce4061',fontSize:16,textAlign:'right'}}>Forgot Password ?</Text>
            //         </TouchableOpacity>
            //         <TouchableOpacity>
            //             <Text style={styles.submit_btn}>Login</Text>
            //         </TouchableOpacity>
            //         <TouchableOpacity onPress={() => navigation.push('Register')}>
            //             <Text style={{color:'#ce4061',fontSize:18,textAlign:'right'}}>Create New Account</Text>
            //         </TouchableOpacity>
                    
            //     </View>
            // </View>
            <View style={styles.container}>
                <ScrollView>
                    {/* <Image source={require('../../assests/images/Veer-Teja.jpg')} style={[styles.post_image, { width:deviceWidth,
                            height: scaleHeight({
                                source: require('../../assests/images/Veer-Teja.jpg'),
                                desiredWidth: deviceWidth/1.5
                            }),borderBottomLeftRadius:30,borderBottomRightRadius:30
                        }]}/> */}
                        <ImageBackground source={require('../../assests/images/social-bg.png')} resizeMode="contain" style={[styles.image, { width:deviceWidth,
                            height: scaleHeight({
                                source: require('../../assests/images/social-bg.png'),
                                desiredWidth: deviceWidth
                            }),borderBottomLeftRadius:30,borderBottomRightRadius:30,overflow:'hidden'
                        }]}>
                            <View style={styles.text_container}>
                                {/* <Text style={styles.text}>Register</Text> */}
                            </View>
                        </ImageBackground>
                    {/* <View style={{backgroundColor:'#ce4061',paddingTop:150,borderBottomLeftRadius:30,borderBottomRightRadius:30,paddingBottom:20}}>
                        <Text style={{fontSize:35,fontWeight:'bold',textAlign:'center',color:'#fff'}}>Register</Text>
                    </View> */}
                    <Text style={styles.text}>Login</Text>
                <View style={{paddingHorizontal:15}}>
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Username"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        autoCapitalize = "none"
                        value={this.state.user_name}  
                        onChangeText={user_name => this.setState({ user_name })}
                    />
                    {
                        !this.state.errorUsername ? <><Text  style={styles.errorMessage}>Please Enter a Username.!</Text></> : <></>
                    }
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Password"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        secureTextEntry={true}
                        autoCapitalize = "none"
                        value={this.state.password}  
                        onChangeText={password => this.setState({ password })}
                    />
                    {
                        !this.state.errorPassword ? <><Text  style={styles.errorMessage}>Please Enter a Password.!</Text></> : <></>
                    }
                    <TouchableOpacity onPress={() => navigation.push('SendOtp')}>
                        <Text style={{color:'#ce4061',fontSize:16,textAlign:'right'}}>Forgot Password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.login()}>
                        <Text style={styles.submit_btn}>Login</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.push('Register')}>
                        <Text style={{color:'#ce4061',fontSize:18,textAlign:'right'}}>Create New Account?</Text>
                    </TouchableOpacity>
                    
                </View>
                </ScrollView>
            </View>
        )
    }
}