import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import CodeInput from "react-native-confirmation-code-input";
import Icon from "react-native-ico-material-design";
import { forgotpassword } from "../api/auth.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class ForgotPasswordScreen extends Component{
    state = {
        new_pw              : '',
        confirm_pw          : '',
        new_pw_secure       : true, 
        confirm_pw_secure   : true,
        errorNewpassword    : true, 
        errorConfirmpassword    : true, 
    }
  
    ForgotPassword = async () => {
        let {route} = this.props;
        let data = {
            user_name           : route.params.user_name,
            new_password        : this.state.new_pw,
            confirm_password    : this.state.confirm_pw,
        }
        let response = '';
        if(this.state.new_pw != ''){
            this.setState({errorNewpassword: true});
          } else {
            this.setState({errorNewpassword: false});
          }
          if(this.state.confirm_pw != ''){
            this.setState({errorConfirmpassword: true});
          } else {
            this.setState({errorConfirmpassword: false});
          }
          if(this.state.new_pw == '' || this.state.confirm_pw == ''){
            
            
      
          } else {
            if(this.state.new_pw != this.state.confirm_pw){         
            
                alert('Please enter the same password in both password fields');
            } else{

                response = await forgotpassword(data);
                if (response.data.status == false) {
                    alert(response.data.message);
                  }
            }
          }
    //   console.log('res',response);
    
      if(response.data.status){
        this.props.navigation.push('Login'); 
              }      
    }
    newEyePress () {
        if(this.state.new_pw_secure == true){
            this.setState({new_pw_secure : false});
        } else {
            this.setState({new_pw_secure : true});
        }
    }
    confirmEyePress () {
        if(this.state.confirm_pw_secure == true){
            this.setState({confirm_pw_secure : false});
        } else {
            this.setState({confirm_pw_secure : true});
        }
    }  
     

      async componentDidMount () {
        
      }
    render(){
        let {navigation} = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            <View style={[styles.container,{justifyContent:'center'}]}>
                
                        
                <View style={{paddingHorizontal:15}}>
                <View style={styles.section}>
                    </View>
                <View style={{marginVertical:20,justifyContent:'center'}}>
                
                <Text style={styles.label}>New Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "New Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.new_pw_secure}
                            value={this.state.new_pw}  
                            onChangeText={new_pw => this.setState({ new_pw })}
                        
                    />
                     
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.newEyePress()} color='#a1a1a1' size={20}  name={this.state.new_pw_secure == true ? 'turn-visibility-off-button' : 'visibility-button'} />
                    </View>
                </View>
                {
                  !this.state.errorNewpassword ? <><Text  style={styles.errorMessage}>Please Enter a New Password</Text></> : <></>
                }
                <Text style={styles.label}>Confirm Password</Text>
                <View >
                    <TextInput style = {[styles.input]}
                            underlineColorAndroid = "transparent"
                            placeholder = "Confirm Password"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            secureTextEntry={this.state.confirm_pw_secure}
                            value={this.state.confirm_pw}  
                            onChangeText={confirm_pw => this.setState({ confirm_pw })}
                        
                    />
                   
                    <View  style={{position:'absolute',right:20,bottom:20}}>
                        <Icon onPress={() => this.confirmEyePress()} color='#a1a1a1' size={20}  name={this.state.confirm_pw_secure == true ? 'turn-visibility-off-button' : 'visibility-button'} />
                    </View>
                </View>
                {
                  !this.state.errorConfirmpassword ? <><Text  style={styles.errorMessage}>Please Enter a Confirm Password</Text></> : <></>
                }
                
                        
                </View>
                    <TouchableOpacity onPress={() => this.ForgotPassword()}>
                        <Text style={styles.submit_btn}>CHANGE PASSWORD</Text>
                    </TouchableOpacity>
                    
            </View>
            </View>
        )
    }
}