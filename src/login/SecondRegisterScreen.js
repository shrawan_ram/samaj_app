import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, Modal, Picker, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { RadioGroup } from "react-native-radio-buttons-group";
import SelectDropdown from "react-native-select-dropdown";
import { sendotp, sign_up } from "../api/auth.api";
import { get_city, get_gotra } from "../api/user.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class SecondRegisterScreen extends Component{
    state={
        modalVisible:false,
        cities          :['jodhpur', 'barmer'],
        gotra          :['Mundan', 'Saran'],
        radioButtonsData: [{
            id: '1', // acts as primary key, should be unique and non-empty string
            label: 'Male',
            selected: true,
            color:'#ce4061',
            
        }, {
            id: '2',
            label: 'Female',
           
            color:'#ce4061',
        }],
        fname           :'shrawan',
        lname           :'choudhary',
        email           :'rams50288@gmail.com',
        gender          :'male',
        city_id         :'',
        gotra_id        :'',
        address         :'korna',
        pincode         :'344026',
         
        image           :null,
    }
    getCity = async() => {
        let response = await get_city();
        if(response.data.status){
            // this.setState({cities:response.data.data});
        }
    }

    onPressRadioButton(radioButtonsArray) {
        console.log('radop btns: ', radioButtonsArray);
        this.setState({radioButtonsData: radioButtonsArray});
    }

    handleChoosePhoto (type = 'camera', field = 'image') {
        // console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        
        type === 'camera' ? 
            launchCamera(options, (response) => {

                console.log('res',response);
               

                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });                                
                }
            })
            : launchImageLibrary(options, (response) => {
                    if (response.assets[0].uri) {
                            self.setState({ [this.state.field]: response.assets[0] });
                    }
            });
    }
    SignUp = async () => {
        let {route} = this.props;
        
        
        let data = {
            user_name   : route.params.user_name,
            fname       : this.state.fname,
            lname       : this.state.lname,
            password    : route.params.password,
            mobile      : route.params.mobile,
            email       : this.state.email,
            city_id     : this.state.city_id,
            role_id     : 2,
            gotra_id    : this.state.gotra_id,
            site_id     : 7,
            address     : this.state.address,
            pincode     : this.state.pincode,
            
        } 
      console.log('data',data);
        data = this.createFormData(this.state.image, data);
        let response = await sign_up(data);
        console.log('responseee', response);
     
      if (response.data.status) {
        let u_data = {
            user_name : response.data.data.user_name,
        }
        let u_response = await sendotp(u_data);
        if(u_response.data.status){

            this.props.navigation.navigate('OtpVerify',{user_d:response.data.data,type:'register'}); 
        }


      }
    }
    getCity = async() => {
        let response = await get_city();
        console.log('city',response);
        if(response.status){
            // let res = JSON.parse(response.data);
                // console.log('response:', response.data);
            this.setState({cities:response.data});
        }
    }
    getGotra = async() => {
        let response = await get_gotra();
        console.log('city',response);
        if(response.status){
            // let res = JSON.parse(response.data);
                // console.log('response:', response.data);
            this.setState({gotra:response.data});
        }
    }
    createFormData = (image, body = {}) => {
        const data = new FormData();
        
        if(image) {
            data.append('image', {
                name: image.fileName,
                type: image.type,
                uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
            });
        }
        
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }
    async componentDidMount(){
        this.getCity();
        this.getGotra();
    }
    render(){
        let {navigation, route} = this.props;
        console.log('route' ,route);
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            
            <View style={styles.container}>
                <ScrollView style={{marginBottom:40}}>
                    
                <Text style={[styles.text,{fontSize:25}]}>Register</Text>
                <View style={{
                        paddingHorizontal:10,
                        paddingVertical:10
                    }}>
                        <View>
                            <View style={{backgroundColor:"",paddingHorizontal:15,paddingVertical:10,alignItems:'center'}}>
                                <View style={{borderRadius:50,overflow:'hidden',width:100,height:100,justifyContent:'center',backgroundColor:'#fff',alignSelf:'center',borderWidth:.5, borderColor:'#aaa'}}>
                                <Image source={ this.state.image ? this.state.image : require('../../assests/images/male_user.jpg')} style={styles.profile_image} />
                                </View>
                                <TouchableOpacity onPress={() =>this.setState({modalVisible: true, field: 'image'})}>
                                    <Text style={{fontSize:20,paddingTop:8,color:'#ce4061'}}>Add Profile Photo</Text>
                                </TouchableOpacity>
                                
                            </View>
                        </View>
                        <View >
                            {/* <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Username"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.user_name}  
                                onChangeText={user_name => this.setState({ user_name })}
                            /> */}
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "First name"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.fname}  
                                onChangeText={fname => this.setState({ fname })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Last name"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.lname}  
                                onChangeText={lname => this.setState({ lname })}
                            />

                            {/* <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Mobile No."
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.mobile}  
                                onChangeText={mobile => this.setState({ mobile })}
                            /> */}
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Email"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.email}  
                                onChangeText={email => this.setState({ email })}
                            />
                            <View style={{flexDirection:'row',justifyContent:'center'}}>
                                <View style={{flex:.2,justifyContent:'center'}}>
                                    <Text style={{justifyContent:'flex-start',fontWeight:'bold'}}>Gender : </Text>
                                </View>
                                <View style={{flex:.8}}>
                                    <RadioGroup 
                                        radioButtons={this.state.radioButtonsData} 
                                        onPress={() => this.onPressRadioButton(this.state.radioButtonsData)}
                                        layout='row'
                                        containerStyle={{width:'100%',color:'#fff'}}                                
                                        
                                    />
                                </View>                                
                            </View>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#000' }}>
                                <Picker
                                    selectedValue={this.state.city_id}
                                    onValueChange={city_id => this.setState({ city_id })}
                                >
                                    <Picker.Item label='Select City' value='' />
                                    {
                                        this.state.cities.length && this.state.cities.map((city,index) => {
                                            return(
                                                <Picker.Item label={city.name} value={city.id} />
                                            )
                                        })
                                    }
                                    
                                </Picker>
                            </View>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#000' }}>
                                <Picker
                                    selectedValue={this.state.gotra_id}
                                    onValueChange={gotra_id => this.setState({ gotra_id })}
                                >
                                    <Picker.Item label='Select Gotra' value='' />
                                    {
                                        this.state.gotra.length && this.state.gotra.map((item,index) => {
                                            return(
                                                <Picker.Item label={item.name} value={item.id} />
                                            )
                                        })
                                    }
                                    
                                </Picker>
                            </View>
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Address"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.address}  
                                onChangeText={address => this.setState({ address })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Pincode"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.pincode}  
                                onChangeText={pincode => this.setState({ pincode })}
                            />
                            
                        </View>
                            
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({modalVisible: false});
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                    <Text style={styles.modalText}>Take Photo...</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                    <Text style={styles.modalText}>Choose From library...</Text>
                                </TouchableOpacity>
                            
                                <TouchableOpacity
                                onPress={() => this.setState({modalVisible: false})}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
            </ScrollView>
            <View style={{position:'absolute',bottom:0,left:0,right:0,backgroundColor:'#fff',paddingVertical:5,paddingHorizontal:10,borderTopWidth:1,borderTopColor:'#ccc'}}>
                    {
                        this.state.fname && this.state.lname && this.state.email && this.state.gender && this.state.city_id && this.state.gotra_id && this.state.address && this.state.pincode
                        ?
                        <TouchableOpacity onPress={()=>this.SignUp()}>
                            <Text style={[styles.submit_btn,{marginVertical:0}]}>Next</Text>
                        </TouchableOpacity>
                        :
                        <Text style={[styles.disable_submit_btn]}>Next</Text>

                    }
                    
                </View>
            </View>
        )
    }
}