import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class RegisterScreen extends Component{
    state={
        user_name   : '',
        mobile      : '',
        password    : ''
    }
    render(){
        let {navigation} = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            
            <View style={styles.container}>
                <ScrollView>
                    {/* <Image source={require('../../assests/images/Veer-Teja.jpg')} style={[styles.post_image, { width:deviceWidth,
                            height: scaleHeight({
                                source: require('../../assests/images/Veer-Teja.jpg'),
                                desiredWidth: deviceWidth/1.5
                            }),borderBottomLeftRadius:30,borderBottomRightRadius:30
                        }]}/> */}
                        <ImageBackground source={require('../../assests/images/social-bg.png')} resizeMode="contain" style={[styles.image, { width:deviceWidth,
                            height: scaleHeight({
                                source: require('../../assests/images/social-bg.png'),
                                desiredWidth: deviceWidth
                            }),borderBottomLeftRadius:30,borderBottomRightRadius:30,overflow:'hidden'
                        }]}>
                            <View style={styles.text_container}>
                                {/* <Text style={styles.text}>Register</Text> */}
                            </View>
                        </ImageBackground>
                    {/* <View style={{backgroundColor:'#ce4061',paddingTop:150,borderBottomLeftRadius:30,borderBottomRightRadius:30,paddingBottom:20}}>
                        <Text style={{fontSize:35,fontWeight:'bold',textAlign:'center',color:'#fff'}}>Register</Text>
                    </View> */}
                    <Text style={styles.text}>Register</Text>
                <View style={{paddingHorizontal:15}}>
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Username"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        autoCapitalize = "none"
                        value={this.state.user_name}  
                        onChangeText={user_name => this.setState({ user_name })}
                    />
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Mobile"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        autoCapitalize = "none"
                        value={this.state.mobile}  
                        onChangeText={mobile => this.setState({ mobile })}
                    />
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Password"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        secureTextEntry={true}
                        autoCapitalize = "none"
                        value={this.state.password}  
                        onChangeText={password => this.setState({ password })}
                    />
                    <TouchableOpacity onPress={() => navigation.push('SecondRegister',{user_name: this.state.user_name, mobile: this.state.mobile,password:this.state.password})}>
                        <Text style={styles.submit_btn}>Register</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.push('Login')}>
                        <Text style={{color:'#ce4061',fontSize:18,textAlign:'right'}}>Already have an account ?</Text>
                    </TouchableOpacity>
                    
                </View>
            </ScrollView>
            </View>
        )
    }
}