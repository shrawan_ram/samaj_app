import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import CodeInput from "react-native-confirmation-code-input";
import Icon from "react-native-ico-material-design";
import { sendotp } from "../api/auth.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class SendOtpScreen extends Component{
    state={
        user_name       : '',
        errorUsername   : true
    }
    sendOtp = async () =>{

        if(this.state.user_name != ''){
          this.setState({errorUsername: true});
        } else {
          this.setState({errorUsername: false});
        }
        
        if(this.state.user_name == ''){
          
          
    
        } else {
    
          let data = {
            user_name : this.state.user_name,
        }
        let response = await sendotp(data);
        console.log('response',response);
        if(response.data.status == false){
          alert(response.data.message);
        }
        if(response.data.status){
          this.props.navigation.push('OtpVerify',response.data.data);
        }
        }
      }
    render(){
        let {navigation} = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        return(
            <View style={[styles.container,{justifyContent:'center'}]}>
                
                        
                <View style={{paddingHorizontal:15}}>
                <View style={styles.section}>
                    <View style={{padding:15,backgroundColor:'#ce4061aa',borderRadius:50,textAlign:'center',marginVertical:30}}>
                        <Image source={require('../../assests/images/otp.png')} style={styles.post_profile_image} />
                    </View>
                    <Text style={{fontSize:40,fontWeight:'bold'}}>Verification</Text>
                    <Text style={{color:'#444b55',fontSize:16,paddingVertical:10,textAlign:'center',paddingBottom:30}}>We will send you a <Text style={{fontWeight:'bold'}}>One Time Password</Text> on your phone number</Text>
                    <Text>Enter a Username</Text>
                    <TextInput style = {[styles.input,{fontSize:20,letterSpacing:2,textAlign:'center',marginTop:0}]}
                        underlineColorAndroid = "transparent"
                        placeholder = "Enter a Username"
                        placeholderTextColor = "#000"
                        selectionColor={'#000'}
                        autoCapitalize = "none"
                        value={this.state.user_name}  
                        onChangeText={user_name => this.setState({ user_name })}
                    />
                    {
                        !this.state.errorUsername ? <><Text  style={styles.errorMessage}>Please Enter a Mobile No. or Username!</Text></> : <></>
                    }
                     </View>
                    <TouchableOpacity onPress={() => this.sendOtp()}>
                        <Text style={styles.submit_btn}>GET OTP</Text>
                    </TouchableOpacity>
                    
                </View>
            </View>
        )
    }
}