import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'#fff',
        // justifyContent:'center'
    },
    logo:{
        height:100,
        width:100
    },
    input: {
        marginVertical: 15,
        height: 40,
        color:'#000',
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        width:'100%'
     },
    submit_btn:{
        backgroundColor:'#ce4061',
        color:'#fff',
        textAlign:'center',
        // width:200,
        // alignSelf:'center',
        borderRadius:5,
        paddingVertical:10,
        fontSize:20,
        fontWeight:'bold',
        marginVertical:15
    },
    image: {
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        // marginTop:-9,
        elevation: 9,
        backgroundColor:'#fff'

      },
      text_container: {
        
        position:'absolute',
        top:0,
        right:0,
        left:0,
        bottom:0,
        
        backgroundColor: "#00000070",
        justifyContent:'flex-end',
        
      },
      text:{
        color: "#000",
        fontSize: 42,
        fontWeight: "bold",
        textAlign: "center",
        paddingVertical:20
      },
      section: {
        // flex: 1,
        paddingHorizontal: 40,
        justifyContent: 'center',
        alignItems: 'center',
      },
      post_profile_image: {
        width:50,
        resizeMode:"contain",
        height: 50,
        
      },
      label:{
        fontSize:17,
        fontWeight:'bold'
      },
      errorMessage:{
        alignSelf:'flex-start',
        color:'red',
        paddingBottom:5
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 0,
      backgroundColor:'rgba(52, 52, 52, .3)',
      
  },
  modalView: {
      margin: 0,
      width:300,
      backgroundColor: "white",
      borderRadius: 5,
      padding: 35,
      // alignItems: "left",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
  },
  button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
  },
  buttonOpen: {
      backgroundColor: "#F194FF",
  },
  buttonClose: {
      backgroundColor: "#2196F3",
  },
  textStyle: {
      color: "#000",
      fontWeight: "bold",
      textAlign: "right",
      fontSize:17,
      textTransform:'uppercase'
  },
  modalText: {
      marginBottom: 15,
      paddingVertical:8,
      fontSize:17
  },
  profile_image: {
    width:'100%',
    resizeMode:"cover",
    height: 100,
    
    // borderRadius:50,
    
},
// submit_btn:{
//   backgroundColor:'#ce4061',
//   color:'#fff',
//   textAlign:'center',
//   // width:200,
//   // alignSelf:'center',
//   borderRadius:5,
//   paddingVertical:10,
//   fontSize:20,
//   fontWeight:'bold',
//   // marginVertical:15
// },
disable_submit_btn:{
  backgroundColor:'#ce4061',
  color:'#fff',
  textAlign:'center',
  // width:200,
  // alignSelf:'center',
  borderRadius:5,
  paddingVertical:10,
  fontSize:20,
  fontWeight:'bold',
  // marginVertical:15,
  opacity:.5
},
});