import { Dimensions, StyleSheet } from "react-native";
var deviceWidth = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    post_content:{
        paddingHorizontal:15,
        paddingBottom:5
    },
    // paddingHorizontal:{
    //     paddingHorizontal:15
    // },
    postContainer:{
        marginTop:10,
        backgroundColor:'#fff',
        // height:120,
        
        width:deviceWidth
    },
    swiperView:{
        marginTop:10,
        backgroundColor:'#fff',
        // height:120,
        padding:10,
        width:deviceWidth
    },
    storiesContainer:{
        // width:deviceWidth,
        // paddingRight:deviceWidth/5
    },
    scrollContent:{
        width: 90,
        // height:100,
        marginRight:8
    },
    scrollImageView:{
        width:'100%',
        // height:100
    },
    story_image_seen: {
        width: '100%',
        height: 100,
        resizeMode: 'cover',
        borderRadius:15,
        borderWidth:3,
        borderColor:'#bbb'     
        
    },
    story_image: {
        width: '100%',
        height: 100,
        resizeMode: 'cover',
        borderRadius:15,
        borderWidth:3,
        borderColor:'#ce4061cc'     
        
    },
    scrollTextView:{
        textAlign:'center',
        fontSize:12,
        paddingTop:5
    },
    add_post_container:{
        // flex: 1,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        backgroundColor: '#fff',
        padding:15
    },
    postCol_first:{
        flex: 2, //why this doesnt work???
        // width: 150, //using fixed item width instead of flex: 0.5 works
        height: 100,
        padding: 10,
        backgroundColor: 'red',
        // flexGrow: 1,
        // flexShrink: 0,
    },
    postCol_mid:{
        flex: 8, //why this doesnt work???
        // width: 150, //using fixed item width instead of flex: 0.5 works
        // height: 100,
        padding: 10,
        backgroundColor: 'red',
        // flexGrow: 1,
        // flexShrink: 0,
    },
    post_user:{
        // width:100
    },
    post_user_image:{
        width:'100%',
        resizeMode:'contain'
    },
    profile_image: {
        width:'100%',
        resizeMode:"cover",
        height: 50,
        borderRadius:50,
        
      },
      notification_container: {
        // borderBottomColor:'#ccc',
        // borderBottomWidth:.8,
        backgroundColor:"#fff",
        padding:15
    },

    follow_btn:{
        backgroundColor:'#ce4061',
        textAlign:'center',
        color:'#fff',
        padding:8,
        borderRadius:5
    },
      post_profile_image: {
        width:'100%',
        resizeMode:"cover",
        height: 35,
        borderRadius:50,
        borderWidth:.5,
        borderColor:'#aaa'
      },
      post_profile_reply_image:{
        width:'100%',
        resizeMode:"cover",
        height: 30,
        borderRadius:50,
        borderWidth:.5,
        borderColor:'#aaa'
      },
      post_image:{
        width: deviceWidth
      },
    story_row:{
        // flex: 1,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        // justifyContent: 'center',
        // alignItems: 'center',
        marginHorizontal: 15
    },
    story_box_space:{
        width: '50%',
        paddingHorizontal: 15,
        marginBottom: 15
        
    },
    comment_react:{
        flexDirection:'row',
        justifyContent:'center',
        paddingVertical:5
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 8,
        padding: 25,
        
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "#000",
        fontWeight: "bold",
        letterSpacing:1
        // textAlign: "center"
      },
      modalHeading:{
        fontSize:18,
        fontWeight:'bold',
        paddingBottom:7
      },
      modalText: {
        marginBottom: 15,
      },
      cancel:{
          textAlign:'right',
          flex:.3
      }
    
})