import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import { Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-ico-material-design";

export default class HeaderRightComponent extends Component{
    render() {
        let { navigation } = this.props;
        let onPress = target => this.props.onPress(target);
        return(
            <View style={{flexDirection:'row'}}>
                <View style={{flex:.2}}>
                    <TouchableOpacity onPress={() => onPress('SearchScreen')}>
                        <Icon name="searching-magnifying-glass" color="#fff" />     
                    </TouchableOpacity>
                </View>
                <View style={{flex:.2}}>
                    <TouchableOpacity onPress={() => onPress('Notification')}>
                        <Icon name="notifications-bell-button" color="#fff" />     
                    </TouchableOpacity>
                </View>
                <View style={{flex:.2}}>
                    <TouchableOpacity onPress={() => onPress('AddPost')}>
                        <Icon name="square-add-button" color="#fff" />     
                    </TouchableOpacity>
                </View>
                <View style={{flex:.15}}>
                    <TouchableOpacity onPress={() => onPress('ComingSoon')}>
                        <Icon name="chat-bubbles" color="#fff" />     
                    </TouchableOpacity>              
                </View>  
            </View>
        );
    }
}