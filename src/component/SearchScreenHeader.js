import React ,{ Component } from "react";
import { StyleSheet, TextInput, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ico-material-design";

export default class SearchScreenHeader extends Component{
    state = {
        name : ''
    }
    render(){
        let { navigation } = this.props;
        let onPress = target => this.props.onPress(target);
        return(
            <View style={{flexDirection:'row',paddingRight:15}}>
                <View style={{flex:1}}>
                    <View >
                        <TextInput style={styles.input}
                            underlineColorAndroid = "transparent"
                            placeholder = "Search"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            value={this.state.name}  
                            onChangeText={name => this.setState({ name })}
                            
                        />
                        
                        <View  style={{position:'absolute',right:10,bottom:'17%'}}>
                            <Icon color='#a1a1a1' size={20}  name={this.state.name != '' ? 'turn-visibility-off-button' : ''} />
                        </View>
                    </View>
                </View>
                  
            </View>
        );
    }
}
const styles = StyleSheet.create({
    input:{
        backgroundColor:'#fff',
        // height:25,
        paddingVertical:2,
        borderRadius:8,
        marginLeft:-30
    }
})