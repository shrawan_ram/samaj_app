import { apiExecute } from "."

export const homepost = async(data) => {
    let res = await apiExecute("homepage-posts?type="+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const user = async(data) => {
    let res = await apiExecute("user?type=vip&site_id=7", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const post_detail = async(data) => {
    
    let res = await apiExecute("post/"+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}