import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import DeviceInfo from 'react-native-device-info';
import md5 from "react-native-md5";
import { API_BASE_URL } from "../config/constants.config";

export const apiExecute = async (url, method = 'GET', data = null, params = {}) => {
    // let devId = uuid
   

    

    let headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        
    }



    if(params.auth) {
        let token = await AsyncStorage.getItem('@token');
        
        
        headers["Authorization"] = "Bearer " + token
    }
    if(params.files) {
        headers["Content-Type"] = "multipart/form-data";
    }

    let instance = axios.create({
        baseURL: API_BASE_URL,
        timeout: 2000,
        headers
    });

    return instance.request({
        url,
        method,
        data
    })
        .then(res => {
            // console.log('main api res: ', res);
            return {
                status: true,
                data: res.data
            }
        })
        .catch(err => {
            console.log('API Error: ', url, err);
            return {
                status: false,
                error: err.response.data
            }
        });
}