import { apiExecute } from "."

export const get_news = async(data) => {
    let res = await apiExecute("event?type="+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_news_detail = async(data) => {
    let res = await apiExecute("event/"+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const event_like = async(data) => {
    let res = await apiExecute("event/reaction", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
