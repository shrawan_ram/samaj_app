import { apiExecute } from "."

export const user_detail = async(data) => {
    let res = await apiExecute("user/"+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_city = async(data) => {
    let res = await apiExecute("city?state_id=33", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_gotra = async(data) => {
    let res = await apiExecute("gotra", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}


export const edit_profile = async(data, user_id) => {
    let res = await apiExecute("user/"+user_id, "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const edit_user_detail = async(data, user_id) => {
    let res = await apiExecute("user-detail/"+user_id, "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const change_password = async(data) => {
    let res = await apiExecute("changepassword", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_users = async(data) => {
    let res = await apiExecute("user?site_id=7", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const search_users = async(data) => {
    let res = await apiExecute("search_user?site_id=7&name="+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}


export const add_connection = async(data) => {
    let res = await apiExecute("connection", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_connection = async(data) => {
    let res = await apiExecute("connection/list?user_id="+data.user_id+"&type="+data.type, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const update_connection = async(data) => {
    let res = await apiExecute("connection/"+data.id+"?status="+data.status, "PUT", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const remove_connection = async(data) => {
    let res = await apiExecute("connection/delete", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
