import { apiExecute } from "."

export const place = async(data) => {
    let res = await apiExecute("place", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
