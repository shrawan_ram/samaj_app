import { apiExecute } from "."

export const sign_up = async(data) => {
    let res = await apiExecute("user", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const login = async(data) => {
    let res = await apiExecute("login", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const sendotp = async(data) => {
    let res = await apiExecute("sendotp", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const verifyOtp = async(data) => {
    let res = await apiExecute("verifyotp", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const forgotpassword = async(data) => {
    let res = await apiExecute("forgotpassword", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}
