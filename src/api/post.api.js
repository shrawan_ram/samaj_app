import { apiExecute } from "."

export const post = async(data) => {
    let res = await apiExecute("post", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const add_post = async(data) => {
    let res = await apiExecute("post", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}


export const comment = async(data) => {
    let res = await apiExecute("comment", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const edit_comment = async(data, comment_id) => {
    let res = await apiExecute("comment/"+comment_id, "PUT", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const delete_comment = async(data) => {
    let res = await apiExecute("comment/"+data, "DELETE", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const post_like = async(data) => {
    let res = await apiExecute("post/reaction", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_like = async(data) => {
    // console.log('data_a',data);
    let res = await apiExecute("post/reaction?post_id="+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const comment_like = async(data) => {
    let res = await apiExecute("comment/reaction", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const get_comment_like = async(data) => {
    console.log('data_a',data);
    let res = await apiExecute("comment/reaction?comment_post_id="+data, "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

export const post_report = async(data) => {
    let res = await apiExecute("post/report", "POST", data, {auth: true});
    // console.log('res',res.data);
    return res;
}

