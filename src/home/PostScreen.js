import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React,{ Component } from "react";
import { Dimensions, Modal, Pressable, RefreshControl, Share, Text, TextInput, TouchableOpacity } from "react-native";
import { Image, SafeAreaView, ScrollView, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import ReadMore from "react-native-read-more-text";
import { post_detail, user } from "../api/home.api";
import { comment, comment_like, edit_comment, get_comment_like, get_like, post_like, delete_comment, post_report } from "../api/post.api";
import { add_connection, remove_connection } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
const scaleHeight = ({ source, desiredWidth }) => {
    const { width, height } = Image.resolveAssetSource(source)

    return desiredWidth / width * height
}
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class PostScreen extends Component{
    state = {
        user_detail         : '',
        post_detail         : [],
        comment             : '',
        post_like           : false,
        comment_id          : '',
        postLikes           : [],
        postcommentLikes    : [],
        edit_id             : '',
        reply_comment_id    :'', 
        modalVisible        : false,
        comment_delete      :false,
        reportPost_reasons  : [
            {
                reason  : "It's spam"
            },
            {
                reason  : "I just don't like it"
            },
            {
                reason  : "Nudity or sexual activity"
            },
            {
                reason  : "Hate speech or symbols"
            },
            {
                reason  : "Voilence or dangerous organisations"
            },
            {
                reason  : "False information"
            },
            {
                reason  : "Scam or froud"
            },
            {
                reason  : "Bullying or harassment"
            },
            {
                reason  : "Intellectual property voilation"
            },
            {
                reason  : "Suicide or self-injury"
            },
            {
                reason  : "Sale of illegal or regulated goods"
            },
            {
                reason  : "Eating disorders"
            },
        ]
    }

    postDetail = async() => {
        let {route} = this.props;
        let post_id = route.params.id;
        console.log('post_id',post_id);
        let response = await post_detail(post_id);

        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            this.setState({post_detail:response.data.data});
        }
    }
    postComment = async() => {
        if(this.state.edit_id == ''){

            let data = {
                comment     : this.state.comment,
                post_id     : this.state.post_detail.id,
                user_id     : this.state.user_detail.id,
                comment_id  : this.state.comment_id
            }
            let response = await comment(data);
            console.log('response',response);
            // console.log('res',response.data.status);
            let item = this.state.post_detail;
            if(response.data.status){
                this.props.navigation.push('Post',item);
            }

        } else {
            let data = '';
            if(this.state.reply_comment_id == ''){
                data = {
                    comment : this.state.comment,                
                }
            } else {
                data = {
                    comment     : this.state.comment,
                    comment_id  : this.state.reply_comment_id                
                }
            }
            
            let response = await edit_comment(data, this.state.comment_id);
            console.log('response',response);
            // console.log('res',response.data.status);
            let item = this.state.post_detail;
            if(response.data.status){
                this.props.navigation.push('Post',item);
            }
        }
    }
    postLike = async() => {
        let { post_detail } = this.state;
        post_detail.auth_post_like = !post_detail.auth_post_like;
        if(post_detail.auth_post_like == true){

            post_detail.count_post_like++ ;
        } else {
            post_detail.count_post_like--;
            
        }
        this.setState({post_detail})
        let data = {
            post_id : this.state.post_detail.id,
            user_id : this.state.user_detail.id
        }
        let response = await post_like(data);
        console.log('response',response);
        // console.log('res',response.data.status);
        let item = this.state.post_detail;
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }
    getLike = async() => {
        this.getLikeSheet.open();
        let data = {
            post_id : this.state.post_detail.id
        }
        console.log('data',data);
        let response = await get_like(this.state.post_detail.id);
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            this.setState({postLikes: response.data.data})
        }
    }
    deleteComment = async() => {
        this.setState({modalVisible:true});
        
        console.log('comment_delete',this.state.comment_delete);
        if(this.state.comment_delete == true){
            this.setState({modalVisible:false});
            let response = await delete_comment(this.state.comment_id);
            console.log('res',response);
            let item = this.state.post_detail;
            if(response.data.status){
                this.props.navigation.push('Post',item);
            }
        } 
    }
    commentLike = async(comment_post_id, index) => {

        let post_detail = this.state.post_detail;

        post_detail.comments[index].auth_comment_like = !post_detail.comments[index].auth_comment_like;
        
        if(post_detail.comments[index].auth_comment_like == true){
            post_detail.comments[index].count_comment_like++ ;
        } else {
            post_detail.comments[index].count_comment_like-- ;            
        }
        this.setState({ post_detail });

        let data = {
            comment_post_id : comment_post_id,
            user_id         : this.state.user_detail.id
        }
        let response = await comment_like(data);
        // console.log('response',response);
        // console.log('res',response.data.status);
        let item = this.state.post_detail;
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }

    replyLike = async(comment_post_id,comment_index, index) => {

        let post_detail = this.state.post_detail;

        post_detail.comments[comment_index].reply[index].auth_comment_reply_like = !post_detail.comments[comment_index].reply[index].auth_comment_reply_like;
        
        if(post_detail.comments[comment_index].reply[index].auth_comment_reply_like == true){
            post_detail.comments[comment_index].reply[index].count_comment_reply_like++ ;
        } else {
            post_detail.comments[comment_index].reply[index].count_comment_reply_like-- ;            
        }
        this.setState({ post_detail });

        let data = {
            comment_post_id : comment_post_id,
            user_id         : this.state.user_detail.id
        }
        let response = await comment_like(data);
        // console.log('response',response);
        // console.log('res',response.data.status);
        let item = this.state.post_detail;
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }
    getcommentLike = async(comment_id) => {

        let data = {
            post_id : this.state.post_detail.id
        }
        console.log('data',comment_id);
        let response = await get_comment_like(comment_id);
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            this.setState({postcommentLikes: response.data.data});
            this[RBSheet+'comment'+ comment_id].open();
        }
    }
    getreplyLike = async(comment_id) => {
        let data = {
            post_id : this.state.post_detail.id
        }
        console.log('data',comment_id);
        let response = await get_comment_like(comment_id);
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            this.setState({postcommentLikes: response.data.data});
            this[RBSheet+'reply'+ comment_id].open();
        }
    }
    postReport = async(user_id, post_id, description) => {
        let data = {
            user_id     : user_id,
            post_id     : post_id,
            description : description
        }
        let response = await post_report(data);
        this.reportPost.close();
        console.log('res',response);
    }
    addConnection = async(connection_id, status, index, where) => {
        let data = {
            user_id         : this.state.user_detail.id,
            connection_id   : connection_id,
            status          : status,
        }
        let response = await add_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){

            if(where == 'comment_like'){

                let postcommentLikes = this.state.postcommentLikes;
                if(status == 'requested'){
                    postcommentLikes[index].auth_user_request    = true;
                    // postcommentLikes[index].auth_user_follower   = false;            
                    postcommentLikes[index].auth_user_following  = false;            
                } else {
                    postcommentLikes[index].auth_user_request    = false;
                    // postcommentLikes[index].auth_user_follower   = true;
                    postcommentLikes[index].auth_user_following  = true;            
                }
                
                this.setState({ postcommentLikes });

            } else {

                let postLikes = this.state.postLikes;
                if(status == 'requested'){
                    postLikes[index].auth_user_request    = true;
                    // postLikes[index].auth_user_follower   = false;            
                    postLikes[index].auth_user_following  = false;            
                } else {
                    postLikes[index].auth_user_request    = false;
                    // postLikes[index].auth_user_follower   = true;
                    postLikes[index].auth_user_following  = true;            
                }
                
                this.setState({ postLikes });
            }

            

        }

    }

    removeConnection = async(user_id,status, index, where) => {
        let data = {
            user_id         : user_id,
            status          : status,
        }
        let response = await remove_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){
            
            if(where == 'comment_like'){
                let postcommentLikes = this.state.postcommentLikes;
                postcommentLikes[index].auth_user_following  = false;            
                postcommentLikes[index].auth_user_request    = false;            
                
                this.setState({ postcommentLikes });
                

            } else {

                let postLikes = this.state.postLikes;
                postLikes[index].auth_user_following  = false;            
                postLikes[index].auth_user_request    = false;            
                
                this.setState({ postLikes });
                
            }
            
        }
            
    }
    onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'https://www.instagram.com/p/CVIKmn9vsua/?utm_medium=share_sheet',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };
    async componentDidMount() {
        this.postDetail();
        

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
    }
    onRefresh = async() =>  {
          
        this.postDetail();
        

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        // const initialUrl = await Linking.getInitialURL();
        // console.log('initialUrl',initialUrl);
          wait(2000).then(() => this.setState({refreshing:false}));
        }
    
    render(){
        // console.log('model',this.state.post_detail);
        let image_url = url + 'public/storage/';
        let {navigation, route} = this.props;
        let item = this.state.post_detail;
        let like_status = '';

        // console.log('likes',this.state.postLikes);
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <>
                        <ScrollView 
                        contentContainerStyle={styles.scrollView}
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={()=>this.onRefresh()}
                          />
                        }
                        style={{marginBottom:40}}>
                        <View style={{backgroundColor:'#fff'}}>
                                
                                <View>
                                    <View style={{backgroundColor:"#fff",flexDirection:'row',paddingHorizontal:10,paddingVertical:10}}>
                                        <View style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <TouchableOpacity onPress={()=>item.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                <Image source={ item.user ? {uri : image_url+item.user.image} : require('../../assests/images/male_user.jpg')} style={styles.post_profile_image} />
                                            </TouchableOpacity>
                                        </View>
                                        
                                        <View style={{flex:.8, paddingHorizontal:10,justifyContent:'center',}}>
                                            <TouchableOpacity onPress={()=>item.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                <Text style={{fontWeight:'bold',fontSize:16}}>{item.user ? item.user.user_name : ''}</Text>
                                                <Text style={{fontSize:14,color:'#aaa'}}>{moment.utc(item.created_at).local().startOf('seconds').fromNow()}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity onPress={() => this.moreOption.open()} style={{flex:.1, alignItems:'flex-end'}}>                                    
                                            <Icon name='show-more-button-with-three-dots' width="40%" />                                    
                                        </TouchableOpacity>
                                        
                                    </View>
                                    <View>
                                        {
                                            item.image ?
                                            <>
                                                <View style={styles.post_content}>
                                                
                                                        <Text>{item.caption}</Text>
                                                </View>
                                                <Image source={{uri: image_url + item.image}} style={[styles.post_image, {
                                                    resizeMode:'center',
                                                    height: scaleHeight({
                                                        source: require('../../assests/images/Veer-Teja.jpg'),
                                                        desiredWidth: deviceWidth
                                                    })
                                                    // height:100
                                                }]}/>
                                            </>
                                            :
                                            <View style={[styles.post_content,{paddingBottom:40,paddingTop:40}]}>
                                                
                                                
                                                    <Text style={{fontSize:30}}>{item.caption}</Text>
                                                
                                            </View>
                                        }
                                        <View style={[styles.post_content,{flexDirection:'row',paddingVertical:8,borderBottomWidth:.5,borderBottomColor:'#ccc'}]}>
                                            <TouchableOpacity onPress={() =>  this.getLike()} style={{flex:.07}}>
                                                <Icon name='thumb-up-button' width="60%" color="#ce4061" />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.getLikeSheet.open()} style={{flex:.1,justifyContent:'center'}}>
                                                <Text style={{textAlign:'left',alignSelf:'flex-start'}}>{item.count_post_like == 0 ? '' : item.count_post_like}</Text>
                                            </TouchableOpacity>
                                            <View style={{flex:.66}}></View>
                                            <View style={{flex:.1,justifyContent:'center'}}>
                                                <Text style={{textAlign:'right',alignSelf:'flex-end'}}>{item.view_post ? item.view_post.length : ''}</Text>
                                            </View>
                                            <View style={{flex:.07,alignItems:'flex-end'}}>
                                                <Icon name='visibility-button' width="60%" color="#ce4061" />
                                            </View>
                                        </View>
                                        {/* <View style={{flexDirection:'row'}}>
                                            <TouchableOpacity style={{flex:.25,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='thumb-up-button' width="50%" />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{flex:.25,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='add-comment-button' width="50%" />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{flex:.25,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='visibility-button' width="50%" />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{flex:.25,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='share-button' width="50%" />
                                            </TouchableOpacity>
                                        </View> */}
                                        <View style={{flexDirection:'row'}}>
                                        
                                            <TouchableOpacity onPress={()=>this.postLike()} style={{flex:.33,justifyContent:'center',alignItems:'flex-start',paddingVertical:15}}>
                                                <Icon color={item.auth_post_like == true ? "#ce4061" : ''} name='thumb-up-button' width="50%" />
                                            </TouchableOpacity>                                                            
                                                                    
                                                                    
            
                                            

                                            <TouchableOpacity onPress={() => this.inputText.focus(this.setState({comment_id:'',edit_id:''}))} style={{flex:.33,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='add-comment-button' width="50%" />
                                            </TouchableOpacity>
                                            {/* <TouchableOpacity style={{flex:.33,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                <Icon name='visibility-button' width="50%" />
                                            </TouchableOpacity> */}
                                            <TouchableOpacity onPress={() => this.onShare()} style={{flex:.33,justifyContent:'center',alignItems:'flex-end',paddingVertical:15}}>
                                                <Icon name='share-button' width="50%" />
                                            </TouchableOpacity>
                                        </View>
            
                                        <View style={[styles.post_content]}>
                                            <Text style={{fontSize:18,fontWeight:'bold'}}> Comments</Text>
                                            <Text style={{fontSize:18,fontWeight:'bold',textAlign:'center'}}> 
                                                {
                                                item.comments ? item.comments.length == 0 
                                                ? 
                                                <View style={{alignItems:'center',paddingVertical:100}}>
                                                    <Text style={{fontSize:20,textAlign:'center'}}>No Comments Yet</Text> 
                                                    <Text style={{fontSize:14,textAlign:'center'}}>Be the first to comment.</Text>
                                                </View>
                                                : '' : ''}
                                            </Text>
                                            {
                                                item.comments && item.comments.map((comment, comment_index) => {
                                                    return(
                                                        <View style={{flexDirection:'row',paddingVertical:10}}>
                                                            <TouchableOpacity onPress={()=>comment.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',comment.user) : navigation.push('Profile',comment.user)} style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'flex-start'}}>
                                                                <Image source={ comment.user ? {uri : image_url+comment.user.image} : require('../../assests/images/male_user.jpg')} style={styles.post_profile_image} />
                                                            </TouchableOpacity>
                                                            
                                                            <View style={{flex:.9, paddingLeft:10,justifyContent:'center'}}>
                                                                <View style={{backgroundColor:'#eee',padding:8  ,borderRadius:5}}>
                                                                    <View style={{flexDirection:'row'}}>
                                                                        <View style={{flex:.9}}>
                                                                            <TouchableOpacity onPress={()=>comment.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',comment.user) : navigation.push('Profile',comment.user)}>                                                                        
                                                                                <Text style={{fontWeight:'bold',fontSize:16}}>{comment.user ? comment.user.user_name : ''}</Text>
                                                                                <Text style={{color:'#7d7d7d'}}>{comment.user ? comment.user.fname : ''}{comment.user ? comment.user.lname : ''}</Text>
                                                                            </TouchableOpacity>
                                                                            <Text style={{color:'#7d7d7d',paddingBottom:5}}>{moment.utc(comment.created_at).local().startOf('seconds').fromNow()}</Text>
                                                                        </View>
                                                                        {
                                                                            comment.user_id == this.state.user_detail.id ?
                                                                            <TouchableOpacity onPress={() => this[RBSheet+'commentOption'+ comment.id].open()} style={{flex:.1, alignItems:'flex-end'}}>                                    
                                                                                <Icon name='show-more-button-with-three-dots' width="40%" />                                    
                                                                            </TouchableOpacity>
                                                                            :
                                                                            <></>
                                                                        }
                                                                        
                                                                    </View>
                                                                    <Text>{comment.comment}</Text>
                                                                </View> 
                                                                <View style={styles.comment_react}>
                                                                    
                                                                                        <TouchableOpacity onPress={() => this.commentLike(comment.id, comment_index)} style={{flex:.1}}>
                                                                                            <Text style={{textAlign:'center',color:comment.auth_comment_like == true ? '#ce4061' : '#000'}}>Like</Text>                                                                
                                                                                        </TouchableOpacity>
                                                                                                                                
                                                                    <View style={{flex:.1,}}>
                                                                        <Text style={{textAlign:'center'}}>|</Text>
                                                                    </View>
                                                                    <View style={{flex:.2}}>
                                                                        <TouchableOpacity onPress={() => this.inputText.focus(this.setState({comment_id:comment.id,edit_id:''}))}>
                                                                            <Text style={{textAlign:'center'}}>Reply </Text>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                    <View style={{flex:.6}}></View>
                                                                    <View style={{flex:.1}}>
                                                                    {
                                                                    comment.count_comment_like == 0 ? <Text></Text>  :
                                                                    
                                                                        <View style={{flexDirection:'row'}}>
                                                                            <View style={{flex:.6}}>
                                                                                <Text style={{textAlign:'center'}}>{comment.count_comment_like}</Text>
                                                                            </View>
                                                                            <TouchableOpacity onPress={() => this.getcommentLike(comment.id) } style={{flex:.6,justifyContent:'flex-start'}}>
                                                                                <Icon name='thumb-up-button' color="#ce4061"  width="100%" style={{marginTop:-4}} />
                                                                            </TouchableOpacity>
                                                                            
                                                                        </View>
                                                                
                                                                    }
                                                                    </View>
                                                                    {/* <Text>Like   |</Text>    
                                                                    <Text>   Reply</Text>     */}
                                                                </View>
                                                                {
                                                                    comment.reply && comment.reply.map((reply, index) => {
                                                                        return(
                                                                            <View style={{flexDirection:'row',paddingVertical:10}}>
                                                                                <TouchableOpacity onPress={()=>reply.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',reply.user) : navigation.push('Profile',reply.user)} style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'flex-start'}}>
                                                                                    <Image source={ reply.user ? {uri : image_url+reply.user.image} : require('../../assests/images/male_user.jpg')} style={styles.post_profile_reply_image} />
                                                                                </TouchableOpacity>
                                                                                
                                                                                <View style={{flex:.9, paddingLeft:10,justifyContent:'center'}}>
                                                                                    <View style={{backgroundColor:'#eee',padding:5,borderRadius:5}}>
                                                                                    <View style={{flexDirection:'row'}}>
                                                                                        <View style={{flex:.9}}>
                                                                                            <TouchableOpacity onPress={()=>reply.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',reply.user) : navigation.push('Profile',reply.user)}>
                                                                                                <Text style={{fontWeight:'bold',fontSize:16}}>{reply.user ? reply.user.user_name :''}</Text>
                                                                                                <Text style={{color:'#7d7d7d'}}>{reply.user ? reply.user.fname : ''}{reply.user ? reply.user.lname : ''}</Text>
                                                                                            </TouchableOpacity>
                                                                                            <Text style={{color:'#7d7d7d',paddingBottom:5}}>{moment.utc(reply.created_at).local().startOf('seconds').fromNow()}</Text>
                                                                                        </View>
                                                                                        {
                                                                                            reply.user_id == this.state.user_detail.id ?
                                                                                            <TouchableOpacity onPress={() => this[RBSheet+'replyOption'+ reply.id].open()} style={{flex:.1, alignItems:'flex-end'}}>                                    
                                                                                                <Icon name='show-more-button-with-three-dots' width="40%" />                                    
                                                                                            </TouchableOpacity>
                                                                                            :
                                                                                            <></>
                                                                                        }
                                                                                        
                                                                                    </View>
                                                                                        <Text>{reply.comment}</Text>
                                                                                    </View> 
                                                                                    <View style={styles.comment_react}>
                                                                                        {/* <View style={{flex:.1}}>
                                                                                            <Text style={{textAlign:'center'}}>Like</Text>
                                                                                            
                                                                                        </View> */}
                                                                                        
                                                                                        <TouchableOpacity onPress={() => this.replyLike(reply.id,comment_index, index)} style={{flex:.1}}>
                                                                                            <Text style={{textAlign:'left',color:reply.auth_comment_reply_like == true ? '#ce4061' : '#000'}}>Like</Text>                                                                
                                                                                        </TouchableOpacity>
                                                                                                            
                                                                                        <View style={{flex:.1,}}>
                                                                                            {/* <Text style={{textAlign:'center'}}>|</Text> */}
                                                                                        </View>
                                                                                        <View style={{flex:.15}}>
                                                                                            {/* <TouchableOpacity onPress={() => this.inputText.focus()}>
                                                                                                <Text style={{textAlign:'center'}}>Reply</Text>
                                                                                            </TouchableOpacity> */}
                                                                                        </View>
                                                                                        <View style={{flex:.6}}></View>
                                                                                        <View style={{flex:.1}}>
                                                                                        {
                                                                                        reply.count_comment_reply_like == 0 ? <Text></Text> :
                                                                                        
                                                                                            <View style={{flexDirection:'row'}}>
                                                                                                <View style={{flex:.6}}>
                                                                                                    <Text style={{textAlign:'center'}}>{reply.count_comment_reply_like}</Text>
                                                                                                </View>
                                                                                                <TouchableOpacity onPress={() => this.getreplyLike(reply.id) } style={{flex:.6,justifyContent:'flex-start'}}>
                                                                                                    <Icon name='thumb-up-button' color="#ce4061"  width="100%" style={{marginTop:-4}} />
                                                                                                </TouchableOpacity>
                                                                                                
                                                                                            </View>
                                                                                        
                                                                                        }
                                                                                        
                                                                                        </View>
                                                                                        {/* <Text>Like   |</Text>    
                                                                                        <Text>   Reply</Text>     */}
                                                                                    </View>                                           
                                                                                </View>
                                                                                <RBSheet
                                                                
                                                                                    ref={ref => {
                                                                                        this[RBSheet +'reply'+ reply.id] = ref;
                                                                                    }}
                                                                                    draggableIcon={true}
                                                                                    closeOnDragDown={true}
                                                                                    // height={500}
                                                                                    customStyles={{
                                                                                        container: {
                                                                                        //   justifyContent: "center",
                                                                                        //   alignItems: "center",
                                                                                            borderTopLeftRadius:20,
                                                                                            borderTopRightRadius:20,
                                                                                            height:'90%'
                                                                                        }
                                                                                        }}
                                                                                >
                                                                                    {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                                                    </View> */}
                                                                                        <View style={styles.notification_container}>

                                                                                            <Text style={{fontSize:18,textAlign:'center'}}> 
                                                                                                {
                                                                                                this.state.postcommentLikes ? this.state.postcommentLikes.length == 0 
                                                                                                ? 
                                                                                                <View style={{alignItems:'center',paddingVertical:100}}>
                                                                                                    <Icon color="#ce4061" name='thumb-up-button' style={{}} />
                                                                                                    <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold',color:'#7d7d7d',paddingTop:20,paddingBottom:5}}>No Likes Yet</Text> 
                                                                                                    <Text style={{fontSize:14,textAlign:'center'}}>Be the first to like this.</Text>
                                                                                                </View>
                                                                                                : '' : ''}
                                                                                            </Text>
                                                                                            {
                                                                                                this.state.postcommentLikes.map((post_comment_like,index) => {
                                                                                                    return(
                                                                                                        <View style={{flexDirection:'row',paddingVertical:7}}>
                                                                                                            <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                                                                <TouchableOpacity onPress={()=>post_comment_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_comment_like.user) : navigation.push('Profile',post_comment_like.user)}>
                                                                                                                    <Image source={post_comment_like.user ? {uri : image_url+post_comment_like.user.image} : require('../../assests/images/male_user.jpg')} style={[styles.post_profile_image,{height:50}]} />
                                                                                                                </TouchableOpacity>
                                                                                                            </View>
                                                                                                            <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                                                                                <TouchableOpacity onPress={()=>post_comment_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_comment_like.user) : navigation.push('Profile',post_comment_like.user)}>
                                                                                                                    <Text style={{fontWeight:'bold',fontSize:16}}>{post_comment_like.user ? post_comment_like.user.user_name : ''} </Text>
                                                                                                                    <Text style={{color:'#aaa'}}>{post_comment_like.user ? post_comment_like.user.fname : ''} {post_comment_like.user ? post_comment_like.user.lname : ''}</Text>
                                                                                                                </TouchableOpacity>
                                                                                                            </View>
                                                                                                           
                                                                                                                <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                                                                    {
                                                                                                                        this.state.user_detail.id != post_comment_like.user.id ?
                                                                                                                        post_comment_like.auth_user_request
                                                                                                                        ?
                                                                                                                        <TouchableOpacity onPress={()=>this.removeConnection(post_comment_like.user.id, 'request_me', index, 'comment_like')}>
                                                                                                                            <Text style={styles.following_btn}>Requested</Text>
                                                                                                                        </TouchableOpacity>
                                                                                                                        :
                                                                                                                        post_comment_like.auth_user_following
                                                                                                                        ?
                                                                                                                        <TouchableOpacity onPress={()=>this.removeConnection(post_comment_like.user.id, 'following', index, 'comment_like')}>
                                                                                                                            <Text style={styles.following_btn}>Following</Text>
                                                                                                                        </TouchableOpacity>
                                                                                                                        :
                                                                                                                        <TouchableOpacity onPress={()=>this.addConnection(post_comment_like.user.id, post_comment_like.user.privacy_status == 'private' ? 'requested' : 'followed', index, 'comment_like')}>
                                                                                                                            <Text style={styles.follow_btn}>Follow</Text>
                                                                                                                        </TouchableOpacity>
                                                                                                                        :
                                                                                                                        <></>

                                                                                                                    }
                                                                                                                </View>
                                                                                                            
                                                                                                            
                                                                                                        </View>
                                                                                                    )
                                                                                                })
                                                                                            }

                                                                                            </View>
                                                                                        
                                                                                </RBSheet>
                                                                                <RBSheet
                                                                                ref={ref => {
                                                                                    this[RBSheet +'replyOption'+ reply.id] = ref;
                                                                                }}
                                                                                draggableIcon={true}
                                                                                closeOnDragDown={true}
                                                                                height={140}
                                                                                customStyles={{
                                                                                    container: {
                                                                                    //   justifyContent: "center",
                                                                                    //   alignItems: "center",
                                                                                        borderTopLeftRadius:20,
                                                                                        borderTopRightRadius:20,
                                                                                    }
                                                                                    }}
                                                                            >
                                                                                {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                                                </View> */}
                                                                                    <TouchableOpacity  onPress={() => this.inputText.focus(this.setState({comment_id:reply.id, comment:reply.comment, edit_id: reply.id,reply_comment_id:reply.comment_id})) ?? this[RBSheet+'replyOption'+ reply.id].close()} style={{paddingHorizontal:15,paddingVertical:10}}
                                                                                    //  onPress={() => this.setState({modalVisible: true})}
                                                                                        >
                                                                                            <View style={{flexDirection:'row'}}>
                                                                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                                                                    <Icon name='create-new-pencil-button' width="50%" />
                                                                                                </View>
                                                                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                                                                    <Text style={{fontSize:18}}>Edit</Text>
                                                                                                </View>
                                                                                            </View>
                                                                                    </TouchableOpacity>
                                                                                    <TouchableOpacity onPress={() => this.deleteComment(this.setState({comment_id:reply.id,comment_delete:true} ?? this[RBSheet+'commentOption'+ reply.id].close()))} style={{paddingHorizontal:15,paddingVertical:10}}
                                                                                    //  onPress={() => this.setState({modalVisible: true})}
                                                                                        >
                                                                                            <View style={{flexDirection:'row'}}>
                                                                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                                                                    <Icon name='rubbish-bin-delete-button' width="50%" />
                                                                                                </View>
                                                                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                                                                    <Text style={{fontSize:18}}>Delete</Text>
                                                                                                </View> 
                                                                                            </View>
                                                                                    </TouchableOpacity>
                                                                                    
                                                                            </RBSheet>
                                                                                
                                                                                
                                                                            </View>                                           
                                                                        )
                                                                    })
                                                                }
                                                            </View>
                                                            <RBSheet
                                                                ref={ref => {
                                                                    this[RBSheet +'commentOption'+ comment.id] = ref;
                                                                }}
                                                                draggableIcon={true}
                                                                closeOnDragDown={true}
                                                                height={140}
                                                                customStyles={{
                                                                    container: {
                                                                    //   justifyContent: "center",
                                                                    //   alignItems: "center",
                                                                        borderTopLeftRadius:20,
                                                                        borderTopRightRadius:20,
                                                                    }
                                                                    }}
                                                            >
                                                                {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                                </View> */}
                                                                    <TouchableOpacity  onPress={() => this.inputText.focus(this.setState({comment_id:comment.id, comment:comment.comment, edit_id: comment.id,reply_comment_id:''})) ?? this[RBSheet+'commentOption'+ comment.id].close()} style={{paddingHorizontal:15,paddingVertical:10}}
                                                                    //  onPress={() => this.setState({modalVisible: true})}
                                                                        >
                                                                            <View style={{flexDirection:'row'}}>
                                                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                                                    <Icon name='create-new-pencil-button' width="50%" />
                                                                                </View>
                                                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                                                    <Text style={{fontSize:18}}>Edit</Text>
                                                                                </View> 
                                                                            </View>
                                                                    </TouchableOpacity>
                                                                    <TouchableOpacity onPress={() => this.deleteComment(this.setState({comment_id:comment.id,comment_delete:true} ?? this[RBSheet+'commentOption'+ comment.id].close())) } style={{paddingHorizontal:15,paddingVertical:10}}
                                                                        >
                                                                            <View style={{flexDirection:'row'}}>
                                                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                                                    <Icon name='rubbish-bin-delete-button' width="50%" />
                                                                                </View>
                                                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                                                    <Text style={{fontSize:18}}>Delete</Text>
                                                                                </View> 
                                                                            </View>
                                                                    </TouchableOpacity>
                                                                    
                                                            </RBSheet>
                                                            <Modal
                                                                // animationType="slide"
                                                                transparent={true}
                                                                visible={this.state.modalVisible}
                                                                onRequestClose={() => {
                                                                Alert.alert("Modal has been closed.");
                                                                
                                                                }}
                                                            >
                                                                <View style={styles.centeredView}>
                                                                    <View style={styles.modalView}>
                                                                        <Text style={styles.modalHeading}>Delete Comment</Text>
                                                                        <Text style={styles.modalText}> Deleting the comment will remove it from the conversation </Text>
                                                                        <View style={{flexDirection:'row'}}>
                                                                            <View style={{flex:.6}}></View>
                                                                            <TouchableOpacity
                                                                            style={[styles.cancel]}
                                                                            onPress={() => this.setState({modalVisible:false})}
                                                                            >
                                                                            <Text style={styles.textStyle}>Cancel</Text>
                                                                            </TouchableOpacity>
                                                                            <TouchableOpacity
                                                                            style={[styles.cancel]}
                                                                            onPress={() =>  this.deleteComment(this.setState({comment_delete:true}))}
                                                                            
                                                                            >
                                                                            <Text style={styles.textStyle}>Delete</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </Modal>
                                                            <RBSheet
                                                                
                                                                ref={ref => {
                                                                    this[RBSheet +'comment'+ comment.id] = ref;
                                                                }}
                                                                draggableIcon={true}
                                                                closeOnDragDown={true}
                                                                // height={500}
                                                                customStyles={{
                                                                    container: {
                                                                    //   justifyContent: "center",
                                                                    //   alignItems: "center",
                                                                        borderTopLeftRadius:20,
                                                                        borderTopRightRadius:20,
                                                                        height:'90%'
                                                                    }
                                                                    }}
                                                            >
                                                                {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                                </View> */}
                                                                    <View style={styles.notification_container}>

                                                                        <Text style={{fontSize:18,textAlign:'center'}}> 
                                                                            {
                                                                            this.state.postcommentLikes ? this.state.postcommentLikes.length == 0 
                                                                            ? 
                                                                            <View style={{alignItems:'center',paddingVertical:100}}>
                                                                                <Icon color="#ce4061" name='thumb-up-button' style={{}} />
                                                                                <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold',color:'#7d7d7d',paddingTop:20,paddingBottom:5}}>No Likes Yet</Text> 
                                                                                <Text style={{fontSize:14,textAlign:'center'}}>Be the first to like this.</Text>
                                                                            </View>
                                                                            : '' : ''}
                                                                        </Text>
                                                                        {
                                                                            this.state.postcommentLikes.map((post_comment_like,index) => {
                                                                                return(
                                                                                    <View style={{flexDirection:'row',paddingVertical:7}}>
                                                                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                                            <TouchableOpacity onPress={()=>post_comment_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_comment_like.user) : navigation.push('Profile',post_comment_like.user)}>
                                                                                                <Image source={post_comment_like.user ? {uri:image_url+post_comment_like.user.image} : require('../../assests/images/male_user.jpg')} style={[styles.post_profile_image,{height:50}]} />
                                                                                            </TouchableOpacity>
                                                                                        </View>
                                                                                        <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                                                            <TouchableOpacity onPress={()=>post_comment_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_comment_like.user) : navigation.push('Profile',post_comment_like.user)}>
                                                                                                <Text style={{fontWeight:'bold',fontSize:16}}>{post_comment_like.user ? post_comment_like.user.user_name : ''} </Text>
                                                                                                <Text style={{color:'#aaa'}}>{post_comment_like.user ? post_comment_like.user.fname : ''} {post_comment_like.user ? post_comment_like.user.lname : ''}</Text>
                                                                                            </TouchableOpacity>
                                                                                        </View>                                                                                        
                                                                                        <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                                            {
                                                                                                this.state.user_detail.id != post_comment_like.user.id ?
                                                                                                post_comment_like.auth_user_request
                                                                                                ?
                                                                                                <TouchableOpacity onPress={()=>this.removeConnection(post_comment_like.user.id, 'request_me', index, 'comment_like')}>
                                                                                                    <Text style={styles.following_btn}>Requested</Text>
                                                                                                </TouchableOpacity>
                                                                                                :
                                                                                                post_comment_like.auth_user_following
                                                                                                ?
                                                                                                <TouchableOpacity onPress={()=>this.removeConnection(post_comment_like.user.id, 'following', index, 'comment_like')}>
                                                                                                    <Text style={styles.following_btn}>Following</Text>
                                                                                                </TouchableOpacity>
                                                                                                :
                                                                                                <TouchableOpacity onPress={()=>this.addConnection(post_comment_like.user.id, post_comment_like.user.privacy_status == 'private' ? 'requested' : 'followed', index, 'comment_like')}>
                                                                                                    <Text style={styles.follow_btn}>Follow</Text>
                                                                                                </TouchableOpacity>
                                                                                                :
                                                                                                <></>

                                                                                            }
                                                                                        </View>                                                                                        
                                                                                        
                                                                                    </View>
                                                                                )
                                                                            })
                                                                        }

                                                                        </View>
                                                                    
                                                            </RBSheet>
                                                            
                                                        </View>
                                                    )
                                                })
                                            }
                                            
                                            {/* <View style={{flexDirection:'row',paddingVertical:10}}>
                                                <View style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'flex-start'}}>
                                                    <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                                </View>
                                                
                                                <View style={{flex:.9, paddingLeft:10,justifyContent:'center'}}>
                                                    <View style={{backgroundColor:'#eee',padding:5,borderRadius:5}}>
                                                        <Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt</Text>
                                                        <Text>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</Text>
                                                    </View> 
                                                    <View style={styles.comment_react}>
                                                        <View style={{flex:.1}}>
                                                            <Text style={{textAlign:'center'}}>Like</Text>
                                                            
                                                        </View>
                                                        <View style={{flex:.1,}}>
                                                            <Text style={{textAlign:'center'}}>|</Text>
                                                        </View>
                                                        <View style={{flex:.1}}>
                                                            <TouchableOpacity onPress={() => this.inputText.focus()}>
                                                                <Text style={{textAlign:'center'}}>Reply</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{flex:.6}}></View>
                                                        <View style={{flex:.1}}>
                                                            <View style={{flexDirection:'row'}}>
                                                            <View style={{flex:.6}}>
                                                                <Text style={{textAlign:'center'}}>2</Text>
                                                            </View>
                                                            <View style={{flex:.6,justifyContent:'flex-start'}}>
                                                                <Icon name='thumb-up-button' color="#ce4061"  width="100%" style={{marginTop:-4}} />
                                                            </View>
                                                                
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <View style={{flexDirection:'row',paddingVertical:10}}>
                                                        <View style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'flex-start'}}>
                                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_reply_image} />
                                                        </View>
                                                        
                                                        <View style={{flex:.9, paddingLeft:10,justifyContent:'center'}}>
                                                            <View style={{backgroundColor:'#eee',padding:5,borderRadius:5}}>
                                                                <Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt</Text>
                                                                <Text>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</Text>
                                                            </View> 
                                                            <View style={styles.comment_react}>
                                                                <View style={{flex:.1}}>
                                                                    <Text style={{textAlign:'center'}}>Like</Text>
                                                                    
                                                                </View>
                                                                <View style={{flex:.1,}}>
                                                                    <Text style={{textAlign:'center'}}>|</Text>
                                                                </View>
                                                                <View style={{flex:.15}}>
                                                                    <TouchableOpacity onPress={() => this.inputText.focus()}>
                                                                        <Text style={{textAlign:'center'}}>Reply</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={{flex:.6}}></View>
                                                                <View style={{flex:.1}}>
                                                                    <View style={{flexDirection:'row'}}>
                                                                    <View style={{flex:.6}}>
                                                                        <Text style={{textAlign:'center'}}>2</Text>
                                                                    </View>
                                                                    <View style={{flex:.6,justifyContent:'flex-start'}}>
                                                                        <Icon name='thumb-up-button' color="#ce4061"  width="100%" style={{marginTop:-4}} />
                                                                    </View>
                                                                        
                                                                    </View>
                                                                </View>
                                                            </View>                                           
                                                        </View>
                                                        
                                                    </View>                                           
                                                </View>
                                                
                                            </View> */}
                                        </View>
                                    </View> 
                                </View>
                            </View>
                            <RBSheet
                                ref = {ref => this.moreOption = ref}
                                draggableIcon={true}
                                closeOnDragDown={true}
                                height={140}
                                customStyles={{
                                    container: {
                                    //   justifyContent: "center",
                                    //   alignItems: "center",
                                        borderTopLeftRadius:20,
                                        borderTopRightRadius:20,
                                    }
                                    }}
                            >
                                {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                </View> */}
                                    <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}}
                                    //  onPress={() => this.setState({modalVisible: true})}
                                        >
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                    <Icon name='share-button' width="50%" />
                                                </View>
                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                    <Text style={{fontSize:18}}>Share via</Text>
                                                </View> 
                                            </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}}
                                    onPress={() => this.reportPost.open(this.moreOption.close()) }
                                        >
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                    <Icon name='waving-flag' width="50%" />
                                                </View>
                                                <View style={{flex:.9,paddingHorizontal:10}}>
                                                    <Text style={{fontSize:18}}>Report this post</Text>
                                                    <Text style={{fontSize:13}}>This post is offensive or the account is hacked</Text>
                                                </View> 
                                            </View>
                                    </TouchableOpacity>
                                    
                            </RBSheet>
                            <RBSheet
                                ref = {ref => this.reportPost = ref}
                                draggableIcon={true}
                                closeOnDragDown={true}
                                height={700}
                                
                                customStyles={{
                                    container: {
                                    //   justifyContent: "center",
                                    //   alignItems: "center",
                                        borderTopLeftRadius:20,
                                        borderTopRightRadius:20,
                                    }
                                    }}
                            >
                                <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Report</Text>                                            
                                </View>
                                <View style={{padding:15}}>
                                    <Text style={{fontSize:17,fontWeight:'bold'}}>Why are you reporting this post?</Text>
                                    <Text style={{fontSize:15}}>You report is anonymouse, except if you're reporting an intellectual property infringement. If someone is in immediate danger, call the local emergency service - don't wait.</Text>
                                </View>
                                {
                                    this.state.reportPost_reasons.map((reason, index)=>{
                                        return(
                                            <TouchableOpacity onPress={()=>this.postReport(this.state.user_detail.id,item.id,reason.reason)}  style={{paddingHorizontal:15,paddingVertical:10}}
                                            //  onPress={() => this.setState({modalVisible: true})}
                                                >                                    
                                                <View style={{paddingHorizontal:10}}>
                                                    <Text style={{fontSize:17}}>{reason.reason}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    })
                                }
                                    
                                    
                                    
                            </RBSheet>
                            <RBSheet
                                ref={ref => this.getLikeSheet = ref}
                                draggableIcon={true}
                                closeOnDragDown={true}
                                // height={900}
                                customStyles={{
                                    container: {
                                    //   justifyContent: "center",
                                    //   alignItems: "center",
                                    height:'90%',
                                        borderTopLeftRadius:20,
                                        borderTopRightRadius:20,
                                    }
                                    }}
                            >
                                {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                    <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                </View> */}
                                    <View style={styles.notification_container}>

                                        <Text style={{fontSize:18,textAlign:'center'}}> 
                                            {
                                            this.state.postLikes ? this.state.postLikes.length == 0 
                                            ? 
                                            <View style={{alignItems:'center',paddingVertical:100}}>
                                                <Icon color="#ce4061" name='thumb-up-button' style={{}} />
                                                <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold',color:'#7d7d7d',paddingTop:20,paddingBottom:5}}>No Likes Yet</Text> 
                                                <Text style={{fontSize:14,textAlign:'center'}}>Be the first to like this.</Text>
                                            </View>
                                            : '' : ''}
                                        </Text>
                                        {
                                            this.state.postLikes.map((post_like,index) => {
                                                return(
                                                    <View style={{flexDirection:'row',paddingVertical:7}}>
                                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                            <TouchableOpacity onPress={()=>post_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_like.user) : navigation.push('Profile',post_like.user)}>
                                                                <Image source={post_like.user ? {uri: image_url+post_like.user.image} : require('../../assests/images/user.jpg')} style={[styles.post_profile_image,{height:50}]} />
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                            <TouchableOpacity onPress={()=>post_like.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',post_like.user) : navigation.push('Profile',post_like.user)}>
                                                                <Text style={{fontWeight:'bold',fontSize:16}}>{post_like.user ? post_like.user.user_name : ''} </Text>
                                                                <Text style={{color:'#aaa'}}>{post_like.user ? post_like.user.fname : ''} {post_like.user ? post_like.user.lname : ''}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        
                                                            <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                {/* <TouchableOpacity>
                                                                    <Text style={styles.follow_btn}>Follow</Text>
                                                                </TouchableOpacity> */}
                                                                {
                                                                    this.state.user_detail.id != post_like.user.id ?
                                                                    post_like.auth_user_request
                                                                    ?
                                                                    <TouchableOpacity onPress={()=>this.removeConnection(post_like.user.id, 'request_me', index)}>
                                                                        <Text style={styles.following_btn}>Requested</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    post_like.auth_user_following
                                                                    ?
                                                                    <TouchableOpacity onPress={()=>this.removeConnection(post_like.user.id, 'following', index)}>
                                                                        <Text style={styles.following_btn}>Following</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity onPress={()=>this.addConnection(post_like.user.id, post_like.user.privacy_status == 'private' ? 'requested' : 'followed', index)}>
                                                                        <Text style={styles.follow_btn}>Follow</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <></>

                                                                }
                                                            </View>
                                                        
                                                        
                                                    </View>
                                                )
                                            })
                                        }
                                        
                                    </View>
                                    
                            </RBSheet>
                            
                        </ScrollView>
                        <View style={{position:'absolute',bottom:0,left:0,right:0,backgroundColor:'#fff',paddingVertical:5,paddingHorizontal:10,borderTopWidth:2,borderTopColor:'#ccc'}}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:.9}}>
                                    <TextInput 
                                        ref         = {ref => this.inputText = ref}
                                        style       = {{ borderWidth:1,borderColor:'#ccc',borderRadius:10,fontSize:15,paddingHorizontal:10,marginRight:5,paddingVertical:5 }}
                                        placeholder = "Write something here..."
                                        multiline   = {true}
                                        value       = {this.state.comment}
                                        onChangeText= {comment => this.setState({ comment })}
                                    />
                                </View>
                                <View style={{flex:.1,justifyContent:'flex-end'}}>
                                    <TouchableOpacity onPress={() => this.postComment()} style={{backgroundColor:'#ce4061',alignItems:'center',borderRadius:10,height:40,justifyContent:'center'}}>
                                        <Icon name='send-button' color="#fff"  width="50%" />     
                                    </TouchableOpacity>                       
                                </View>
                            </View>
                            
                        </View>
                    </>
                ) 
            } else {
                return(

                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}