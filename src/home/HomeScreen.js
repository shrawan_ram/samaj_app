import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import {
    View,
    Text, 
    TouchableOpacity,
    StyleSheet,
    SafeAreaView,
    Image,
    ScrollView,
    Dimensions,
    Share,
    Linking,
    RefreshControl,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import ReadMore from 'react-native-read-more-text';
import { homepost, user } from "../api/home.api";
import { post, post_like, post_report } from "../api/post.api";
import { url } from "../config/constants.config";
import { styles } from "./style";

var deviceWidth = Dimensions.get('window').width;
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class HomeScreen extends Component {
    state = {
        story:[
            {
                id:1,
                name: 'pb_jatt',
                image: require('../../assests/images/ramdev.jpg'),
                seen : true,
            },
            {
                id:2,
                name: 'Digvijay Singh',
                image: require('../../assests/images/tejaji.jpg'),
                seen : false,
            },
            {
                id:3,
                name: 'Sumer ',
                image: require('../../assests/images/mahadev.jpg'),
                seen : false,
            },
            {
                id:4,
                name: 'Rohan',
                image: require('../../assests/images/user.jpg'),
                seen : false,
            }
        ],
        vip_users   : [],
        posts       : [],
        user_detail :'',
        reportPost_reasons  : [
            {
                reason  : "It's spam"
            },
            {
                reason  : "I just don't like it"
            },
            {
                reason  : "Nudity or sexual activity"
            },
            {
                reason  : "Hate speech or symbols"
            },
            {
                reason  : "Voilence or dangerous organisations"
            },
            {
                reason  : "False information"
            },
            {
                reason  : "Scam or froud"
            },
            {
                reason  : "Bullying or harassment"
            },
            {
                reason  : "Intellectual property voilation"
            },
            {
                reason  : "Suicide or self-injury"
            },
            {
                reason  : "Sale of illegal or regulated goods"
            },
            {
                reason  : "Eating disorders"
            },
        ],
        refreshing:false
    }

    getPosts = async() => {
        let response = await homepost('post');
        console.log('response',response);
        console.log('res',response);
        if(response.data.status){
            this.setState({posts:response.data.data});
        }
    }

    getVipUser = async() => {
        
        let response = await user();
        console.log('response',response);
        if(response.data.status){
            this.setState({vip_users:response.data.data});
        }
    }

    postLike = async(post_id, index) => {

        // let { posts } = this.state;
        // posts.id = !posts.auth_post_like;

        let posts = this.state.posts;

        posts[index].auth_post_like = !posts[index].auth_post_like;
        
        if(posts[index].auth_post_like == true){
            posts[index].count_post_like++ ;
        } else {
            posts[index].count_post_like-- ;            
        }
        this.setState({ posts });

        let data = {
            post_id : post_id,
            user_id : this.state.user_detail.id
        }
        let response = await post_like(data);
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }
    onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'https://www.instagram.com/p/CVIKmn9vsua/?utm_medium=share_sheet',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };

    postReport = async(user_id, post_id, description, index) => {
        let data = {
            user_id     : user_id,
            post_id     : post_id,
            description : description
        }
        let response = await post_report(data);
        // this[RBSheet +'Report'+ index].close();
        

            this[RBSheet +'Report'+ index].close();
        
        console.log('res',response);
    }
    async componentDidMount() {
        this.getPosts();
        this.getVipUser();
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        const initialUrl = await Linking.getInitialURL();
        console.log('initialUrl',initialUrl);
    }
    
      
      
      
    onRefresh = async() =>  {
          
        //   this.setState({refreshing:true})
        this.getPosts();
        this.getVipUser();
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
        // const initialUrl = await Linking.getInitialURL();
        // console.log('initialUrl',initialUrl);
          wait(2000).then(() => this.setState({refreshing:false}));
        }

    

    render(){
        
        
        let image_url = url + 'public/storage/';
       
        let {navigation} = this.props;

        // console.log('vip',this.state.vip_users);
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
           if (this.state.loading) {
        return(
            <SafeAreaView>
                <ScrollView 
                    contentContainerStyle={styles.scrollView}
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={()=>this.onRefresh()}
                      />
                    }
                > 
                                       
                    {/* <View style={{backgroundColor:"#fff",flexDirection:'row',paddingHorizontal:10,paddingVertical:10}}>
                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                            <Image source={require('../../assests/images/user.jpg')} style={styles.profile_image} />
                        </View>
                        
                        <View style={{flex:.75, paddingHorizontal:10,justifyContent:'center',}}>
                            <TextInput 
                                style={{ borderWidth:1,borderColor:'#ccc',borderRadius:50,fontSize:17,paddingHorizontal:10 }}
                                placeholder = "Write something here..."
                             />
                        </View>
                        <View style={{flex:.1, borderRadius:50,overflow:'hidden',width:70,height:70,justifyContent:'center',backgroundColor:'#fff'}}>
                            <Icon name='photo-library' width='100%' height='40%' />
                        </View>
                    </View>                     */}
                    <View style={styles.swiperView}>
                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={styles.storiesContainer}
                        >                            
                            {/* <View style={styles.scrollContent} >
                                <View style={styles.scrollImageView}>
                                    <Image source={require('../../assests/images/user.jpg')} style={styles.story_image}/>
                                </View>
                                <Text style={styles.scrollTextView} numberOfLines={1}>Create Story</Text>
                            </View> */}
                            {
                                this.state.vip_users.map((item, index) => {
                                    return(
                                        <TouchableOpacity onPress={()=>item.id == this.state.user_detail.id ? navigation.navigate('UserProfile',item) : navigation.push('Profile',item)} style={styles.scrollContent} >
                                            <View style={styles.scrollImageView}>
                                                <Image source={{uri:image_url+item.image}} style={ styles.story_image }/>
                                            </View>
                                            <Text style={styles.scrollTextView} numberOfLines={1}>{item.user_name}</Text>
                                        </TouchableOpacity>
                                    );
                                })
                            }
                            
                        </ScrollView>
                    </View>
                    {
                        this.state.posts.map((item, index) => {
                            return(
                                <>
                                <View style={styles.postContainer}>                        
                                    <View>
                                        <View style={{backgroundColor:"#fff",flexDirection:'row',paddingHorizontal:10,paddingVertical:10}}>
                                            <View style={{flex:.1, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                <TouchableOpacity onPress={()=>item.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                    <Image source={item.user ? {uri:image_url+item.user.image } : require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                                </TouchableOpacity>
                                            </View>                                            
                                            <View style={{flex:.8, paddingHorizontal:10,justifyContent:'center',}}>
                                                <TouchableOpacity onPress={()=>item.user.id == this.state.user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                    <Text style={{fontWeight:'bold',fontSize:16}}>{item.user ? item.user.user_name : 'user'}</Text>
                                                    <Text style={{fontSize:14,color:'#aaa'}}>{moment.utc(item.created_at).local().startOf('seconds').fromNow()}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <TouchableOpacity onPress={() => this[RBSheet + index].open()} style={{flex:.1, justifyContent:'center',alignItems:'flex-end'}}>
                                                <Icon name='show-more-button-with-three-dots' width="50%" />
                                            </TouchableOpacity>
                                        </View>
                                        <View>
                                        {
                                            item.image ?
                                                        <>
                                            <View style={styles.post_content}>
                                                <TouchableOpacity onPress={()=>navigation.push('Post',item)}>
                                                <ReadMore
                                                numberOfLines={2}>
                                                    <Text>{item.caption}</Text>
                                                </ReadMore>
                                                </TouchableOpacity>
                                            </View>
                                            <TouchableOpacity onPress={()=>navigation.push('Post',item)}>
                                                <Image source={{uri: image_url+item.image}} style={[styles.post_image, {
                                                    // height:500,
                                                    resizeMode:'center',
                                                    height: scaleHeight({
                                                        source: require('../../assests/images/Veer-Teja.jpg'),
                                                        desiredWidth: deviceWidth
                                                    })
                                                }]}/>
                                            <View style={[styles.post_content,{flexDirection:'row',paddingVertical:8,borderBottomWidth:.5,borderBottomColor:'#ccc'}]}>
                                                <View style={{flex:.07}}>
                                                    <Icon name='thumb-up-button' width="60%" color="#ce4061" />
                                                </View>
                                                <View style={{flex:.4,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'left',alignSelf:'flex-start',color:'#7d7d7d'}}>{item.count_post_like == 0 ? '' : item.count_post_like}</Text>
                                                </View>
                                                <View style={{flex:.36}}></View>
                                                <View style={{flex:.1,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'right',alignSelf:'flex-end'}}>{item.view_post ? item.view_post.length : ''}</Text>
                                                </View>
                                                <View style={{flex:.07,alignItems:'flex-end'}}>
                                                    <Icon name='visibility-button' width="60%" color="#ce4061" />
                                                </View>
                                            </View>
                                            </TouchableOpacity>
                                            </>
                                        :
                                        <View >
                                            <TouchableOpacity onPress={()=>navigation.push('Post',item)} >
                                                <View style={{paddingVertical:40,paddingHorizontal:10,alignItems:'center'}}>

                                                    <ReadMore
                                                    
                                                    numberOfLines={3}>
                                                        <Text style={{fontSize:30}}>{item.caption}</Text>
                                                    </ReadMore>
                                                </View>
                                            <View style={[styles.post_content,{flexDirection:'row',paddingVertical:8,borderBottomWidth:.5,borderBottomColor:'#ccc'}]}>
                                                <View style={{flex:.07}}>
                                                    <Icon name='thumb-up-button' width="60%" color="#ce4061" />
                                                </View>
                                                <View style={{flex:.4,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'left',alignSelf:'flex-start',color:'#7d7d7d'}}>{item.count_post_like == 0 ? '' : item.count_post_like}</Text>
                                                </View>
                                                <View style={{flex:.36}}></View>
                                                <View style={{flex:.1,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'right',alignSelf:'flex-end'}}>{item.view_post ? item.view_post.length : ''}</Text>
                                                </View>
                                                <View style={{flex:.07,alignItems:'flex-end'}}>
                                                    <Icon name='visibility-button' width="60%" color="#ce4061" />
                                                </View>
                                            </View>
                                            </TouchableOpacity>
                                        </View>
                                                
                                        }
                                            <View style={{flexDirection:'row'}}>

                                                {/* <TouchableOpacity style={{flex:.33,justifyContent:'center',alignItems:'flex-start',paddingVertical:15}}>
                                                    <Icon name='thumb-up-button' color="#ce4061" width="50%" />
                                                </TouchableOpacity> */}

                                                
                                                                        
                                                <TouchableOpacity onPress={()=>this.postLike(item.id, index)} style={{flex:.33,justifyContent:'center',alignItems:'flex-start',paddingVertical:15}}>
                                                    <Icon color={item.auth_post_like == true ? "#ce4061" : ''} name='thumb-up-button' width="50%" />
                                                </TouchableOpacity>
                                                                        
                                                                    
                                                <TouchableOpacity onPress={()=>navigation.push('Post',item)} style={{flex:.33,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                    <Icon name='add-comment-button' width="50%" />
                                                </TouchableOpacity>
                                                {/* <TouchableOpacity style={{flex:.33,justifyContent:'center',alignItems:'center',paddingVertical:15}}>
                                                    <Icon name='visibility-button' width="50%" />
                                                </TouchableOpacity> */}
                                                <TouchableOpacity onPress={() => this.onShare()} style={{flex:.33,justifyContent:'center',alignItems:'flex-end',paddingVertical:15}}>
                                                    <Icon name='share-button' width="50%" />
                                                </TouchableOpacity>
                                            </View>
                                        </View> 
                                    </View>
                                </View>
                                <RBSheet
                                    ref={ref => {
                                        this[RBSheet +'Report'+ index] = ref;
                                    }}
                                    draggableIcon={true}
                                    closeOnDragDown={true}
                                    height={700}
                                    
                                    customStyles={{
                                        container: {
                                        //   justifyContent: "center",
                                        //   alignItems: "center",
                                            borderTopLeftRadius:20,
                                            borderTopRightRadius:20,
                                        }
                                        }}
                                >
                                    <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Report</Text>                                            
                                    </View>
                                    <View style={{padding:15}}>
                                        <Text style={{fontSize:17,fontWeight:'bold'}}>Why are you reporting this post?</Text>
                                        <Text style={{fontSize:15}}>You report is anonymouse, except if you're reporting an intellectual property infringement. If someone is in immediate danger, call the local emergency service - don't wait.</Text>
                                    </View>
                                    {
                                        this.state.reportPost_reasons.map((reason, reson_index)=>{
                                            return(
                                                <TouchableOpacity onPress={()=>this.postReport(this.state.user_detail.id, item.id, reason.reason, index)}  style={{paddingHorizontal:15,paddingVertical:10}}
                                                    >                                    
                                                    <View style={{paddingHorizontal:10}}>
                                            {/* //  onPress={() => this.setState({modalVisible: true})} */}
                                                        <Text style={{fontSize:17}}>{reason.reason} </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                        
                                        
                                        
                                </RBSheet>
                                <RBSheet
                                    ref={ref => {
                                        this[RBSheet + index] = ref;
                                    }}
                                    draggableIcon={true}
                                    closeOnDragDown={true}
                                    height={140}
                                    customStyles={{
                                        container: {
                                        //   justifyContent: "center",
                                        //   alignItems: "center",
                                            borderTopLeftRadius:20,
                                            borderTopRightRadius:20,
                                        }
                                        }}
                                >
                                    {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                    </View> */}
                                        <TouchableOpacity  style={{paddingHorizontal:15,paddingVertical:10}}
                                         onPress={() => this.onShare()}
                                            >
                                                <View style={{flexDirection:'row'}}>
                                                    <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                        <Icon name='share-button' width="50%" />
                                                    </View>
                                                    <View style={{flex:.9,paddingHorizontal:10}}>
                                                        <Text style={{fontSize:18}}>Share via</Text>
                                                    </View> 
                                                </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}}
                                        //  onPress={() => this.setState({modalVisible: true})}
                                        onPress={() =>this[RBSheet +'Report'+ index].open(this[RBSheet + index].close())}
                                            >
                                                <View style={{flexDirection:'row'}}>
                                                    <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                        <Icon name='waving-flag' width="50%" />
                                                    </View>
                                                    <View style={{flex:.9,paddingHorizontal:10}}>
                                                        <Text style={{fontSize:18}}>Report this post</Text>
                                                        <Text style={{fontSize:13}}>This post is offensive or the account is hacked</Text>
                                                    </View> 
                                                </View>
                                        </TouchableOpacity>
                                        
                                </RBSheet>
                            </>
                            )
                        })
                    }

                </ScrollView>
            </SafeAreaView>
        );
    } else {
        return(

        <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
          <ActivityIndicator  color="#ce4061" />
        </View>
        )
    }

    }
}

const scaleHeight = ({ source, desiredWidth }) => {
    const { width, height } = Image.resolveAssetSource(source)

    return desiredWidth / width * height
}

