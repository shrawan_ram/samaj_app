import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({

    input: {
        marginVertical: 15,
        height: 40,
        color:'#000',
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        width:'100%'
     },
    submit_btn:{
        backgroundColor:'#ce4061',
        color:'#fff',
        textAlign:'center',
        // width:200,
        // alignSelf:'center',
        borderRadius:5,
        paddingVertical:10,
        fontSize:15,
        fontWeight:'bold',
        // marginVertical:15
    },
    disable_submit_btn:{
        backgroundColor:'#ce4061',
        color:'#fff',
        textAlign:'center',
        // width:200,
        // alignSelf:'center',
        borderRadius:5,
        paddingVertical:10,
        fontSize:20,
        fontWeight:'bold',
        // marginVertical:15,
        opacity:.5
    },
    profile_image: {
        width:'100%',
        resizeMode:"cover",
        height: 100,
        
        // borderRadius:50,
        
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor:'rgba(52, 52, 52, .3)',
        
    },
    modalView: {
        margin: 0,
        width:300,
        backgroundColor: "white",
        borderRadius: 5,
        padding: 35,
        // alignItems: "left",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "#000",
        fontWeight: "bold",
        textAlign: "right",
        fontSize:17,
        textTransform:'uppercase'
    },
    modalText: {
        marginBottom: 15,
        paddingVertical:8,
        fontSize:17
    },
    notification_container: {
        borderBottomColor:'#ccc',
        borderBottomWidth:.8,
        // backgroundColor:"#fff",
        padding:15
    },
    post_profile_image: {
        width:'100%',
        resizeMode:"cover",
        height: 50,
        borderRadius:50,
        borderWidth:.5,
        borderColor: '#ccc'
      },
      post_liked_image: {
        width:'100%',
        resizeMode:"cover",
        height: 50,
        borderWidth:.5,
        borderColor: '#ccc'
      },
      follow_btn:{
          backgroundColor:'#ce4061',
          textAlign:'center',
          color:'#fff',
          padding:8,
          borderRadius:5
      },
      following_btn:{
        textAlign:'center',
        // color:'#fff',
        padding:8,
        borderRadius:5,
        borderWidth:.5,
        borderColor:'#ccc',
    },
    tabTitle:{
        textAlign:'center',
        paddingVertical:10,
        fontSize:14,
        fontWeight:'bold',
        
        // borderBottomWidth:3
      },
      activeTab:{
        borderBottomColor:'#ce4061',
        borderBottomWidth:2,
        backgroundColor:'#fff'
      }
    
})