import AsyncStorage from "@react-native-async-storage/async-storage";
import React,{ Component } from "react";
import { ActivityIndicator, Button, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Input } from "react-native-elements";
import Icon from "react-native-ico-material-design";
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { add_post } from "../api/post.api";
import { user_detail } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
// import  from "react-native-image-picker";

export default class AddPost extends Component{
    state = { 
      image: null, 
      caption: '' ,
      auth_user_detail:'',
      user_detail :''
    }

  onChangeCaption = caption => {
    this.setState({ caption })
  }
  onChangeDescription = description => {
    this.setState({ description })
  }

  addPost = async () => {
    try {
      let data = {
        caption     : this.state.caption,
        user_id     : this.state.user_detail.id
      }
      data = this.createFormData(this.state.image, data);
      let response = await add_post(data);
      console.log('responseee', response);
      if(response.data.status){
         this.props.navigation.push('Home'); 
        this.setState({
          image: null,
          caption: ''
        })
      }
      
    } catch (e) {
      console.error(e)
    }
  }
  createFormData = (image, body = {}) => {
    const data = new FormData();
    
    if(image) {
        data.append('image', {
            name: image.fileName,
            type: image.type,
            uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
        });
    }
    
    
    Object.keys(body).forEach((key) => {
        data.append(key, body[key]);
    });
    
    return data;
}

  selectImage = (type='camer') => {
    let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        
        type === 'camera' ? 
            launchCamera(options, (response) => {

                console.log('res',response);
               

                if (response.assets[0].uri) {
                        self.setState({ image: response.assets[0] });                                
                }
            })
            : launchImageLibrary(options, (response) => {
                    if (response.assets[0].uri) {
                            self.setState({ image: response.assets[0] });
                    }
            });
  }
  userDetail = async() => {
    let {route} = this.props;
    let user_id = this.state.auth_user_detail.id;
    let response = await user_detail(user_id);
    if(response.data.status){
        this.setState({user_detail:response.data.data});
    }
}
  async componentDidMount(){
    let user = await AsyncStorage.getItem('@user');
    user = user !== null ? JSON.parse(user) : '';
    this.setState({auth_user_detail: user});
    this.userDetail();
  }

  render() {

    let {navigation} = this.props;
    let auth_user = this.state.user_detail;
    let image_url = url + 'public/storage/';
    console.log('photo',this.state.auth_user);
    setTimeout(() => {
      this.setState({ loading: true })
    }, 2000)
    
      if (this.state.loading) {
    return (
      <>
        <ScrollView style={{marginBottom:40}}>
          <View style={{ marginTop: 20 }}>
            <View style={{flexDirection:'row',paddingHorizontal:10}}>
              <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                  <TouchableOpacity onPress={() => navigation.navigate('UserProfile',auth_user)}>
                      <Image source={auth_user ? {uri: image_url+auth_user.image} : ''} style={styles.post_profile_image} />
                  </TouchableOpacity>
              </View>
              <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                  <TouchableOpacity onPress={() => navigation.navigate('UserProfile',auth_user)}>
                      <Text style={{fontWeight:'bold',fontSize:16}}>{auth_user ? auth_user.user_name : ''}</Text>
                      <Text style={{color:'#aaa'}}>{auth_user ? auth_user.fname : ''} {auth_user ? auth_user.lname : ''}</Text>
                  </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginTop: 20}}>
              {/* <Text style={{fontSize:17,fontWeight:'bold'}}>Post Details</Text> */}
              {/* <Input
                placeholder='Enter title of the post'
                style={{ margin: 20 }}
                value={this.state.title}
                onChangeText={title => this.onChangeTitle(title)}
              /> */}
              <Input
                placeholder='Something write...'
                style={{paddingHorizontal:5,textAlignVertical:'top',borderRadius:5,backgroundColor:'#fff' }}        
                multiline = {true}
                numberOfLines = {4}
                value={this.state.caption}
                onChangeText={caption => this.onChangeCaption(caption)}
              />
              
            </View>
            <View style={{paddingHorizontal:10}}>
              {this.state.image ? (
                <>
                <Image
                  source={this.state.image}
                  style={{ width: '100%', height: 300 }}
                />
                  <Icon onPress={()=> this.setState({image:null})}  name='round-remove-button' width="100%" color="#000" style={{position:'absolute',top:10,right:-170}}/>
                </>
              ) : (
                <Text
                  onPress={this.selectImage}
                  style={{
                    alignItems: 'center',
                    padding: 10,
                    // margin: 30,
                    backgroundColor:'#ce4061',
                    textAlign:'center',
                    color:'#fff',
                    borderRadius:5
                  }}>
                      Add an image
                </Text>
              )}
            </View>
          </View>
        </ScrollView>
        <View style={{position:'absolute',bottom:0,left:0,right:0,backgroundColor:'#fff',paddingVertical:5,paddingHorizontal:10,borderTopWidth:1,borderTopColor:'#ccc'}}>            
          <TouchableOpacity onPress={() => this.addPost()}>
              <Text style={styles.submit_btn}>POST</Text>
          </TouchableOpacity>
        </View>
      </>
    )
  } else {
      return(

          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
              <ActivityIndicator  color="#ce4061" />
          </View>
          )
  }
  }
}