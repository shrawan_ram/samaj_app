import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import { Alert, Dimensions, FlatList, Image, Modal, Picker, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { change_password, edit_profile, get_city, get_gotra, user_detail } from "../api/user.api";
import { url } from "../config/constants.config";
import RadioGroup from 'react-native-radio-buttons-group';
import { styles } from "./style";
import SelectDropdown from "react-native-select-dropdown";
var deviceWidth = Dimensions.get('window').width;
export default class ChangePassword extends Component{
    state = {
        user_detail     :'',
        auth_user_detail:'',
        
        image           :'',
        user_name       :'',
        old_password    :'',
        new_password    :'',        
        confirm_password:'',

    }
    userDetail = async() => {
        
        let response = await user_detail(this.state.auth_user_detail.id);
       
        if(response.data.status){
            this.setState({user_detail:response.data.data});
            let user_detail = this.state.user_detail;
        
            this.setState({image    : user_detail.image});
            this.setState({user_name: user_detail.user_name}); 
            this.setState({fname    : user_detail.fname}); 
            this.setState({lname    : user_detail.lname}); 
        }
    }
    

    changePassword = async () => {
        
            let data = {
                user_name    : this.state.user_detail.user_name,
                old_password : this.state.old_password,
                new_password : this.state.new_password,
                confirm_password : this.state.confirm_password
            }

            if(this.state.new_password != this.state.confirm_password){         
                
                alert('Please enter the same password in both password fields');
            } else{

            
          
                let response = await change_password(data);

                // console.log('responseee', response);
                
                if ( response.data.status) {
                    this.props.navigation.navigate('UserProfile',this.state.user_detail); 
                }
                
            }
        }

         
  
          
    
    async componentDidMount() {
        

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});
        this.userDetail();       
        
    }
   
    render(){
        let image_url = url + 'public/storage/';
        let user_detail = this.state.user_detail;
        let auth_user_detail = this.state.auth_user_detail;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
//         let ab = Object.keys(this.state.cities).map((key) => [Number(key), this.state.cities[key]])
    
// console.log('city_list', this.state.cities);
     
        let { navigation } = this.props;
        
        return(

          

            <SafeAreaView>
                <ScrollView style={{marginBottom:40}}>
                    <View style={{
                        paddingHorizontal:10,
                        paddingVertical:10
                    }}>
                        <View>
                            <View style={{backgroundColor:"",paddingHorizontal:15,paddingVertical:40,alignItems:'center'}}>
                                <View style={{borderRadius:50,overflow:'hidden',width:100,height:100,justifyContent:'center',backgroundColor:'#fff',alignSelf:'center',borderWidth:.5, borderColor:'#aaa'}}>
                                <Image source={ {uri: image_url+this.state.image} }  style={styles.profile_image} />
                                </View>
                                <TouchableOpacity onPress={() =>this.setState({modalVisible: true, field: 'image'})}>
                                    <Text style={{fontSize:20,paddingTop:8}}>{this.state.user_name}</Text>
                                </TouchableOpacity>
                                
                            </View>
                        </View>
                        <View >
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Old password"
                                placeholderTextColor = "#000"
                                secureTextEntry={true}
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.old_password}  
                                onChangeText={old_password => this.setState({ old_password })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "New password"
                                secureTextEntry={true}
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.new_password}  
                                onChangeText={new_password => this.setState({ new_password })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Confirm password"
                                placeholderTextColor = "#000"
                                secureTextEntry={true}
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.confirm_password}  
                                onChangeText={confirm_password => this.setState({ confirm_password })}
                            />
                            
                            
                        </View>
                            
                    </View>
                    
                </ScrollView>
                <View style={{position:'absolute',bottom:0,left:0,right:0,backgroundColor:'#fff',paddingVertical:5,paddingHorizontal:10,borderTopWidth:1,borderTopColor:'#ccc'}}>
                    {
                        this.state.new_password && this.state.old_password && this.state.confirm_password
                        ?
                        <TouchableOpacity onPress={() => this.changePassword()}>
                            <Text style={styles.submit_btn}>Change Password</Text>
                        </TouchableOpacity>
                        :
                        <Text style={styles.disable_submit_btn}>Change Password</Text>

                    }
                    
                </View>
            </SafeAreaView>
        );
    }
}