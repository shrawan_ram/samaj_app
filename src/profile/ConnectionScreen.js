import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import {View, Text, ScrollView, SafeAreaView, Image, Modal, Pressable, TouchableOpacity, RefreshControl} from "react-native";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import { add_connection, get_connection, get_users, remove_connection } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class ConnectionScreen extends Component {
    state = {
        auth_user_detail: '',
        connections     : [],
        suggestions     : [],
        modalVisible    : false,
        type            : ''
    }

    getConnections = async(user_id, type) => {
        let data = '';
        if(user_id){
            this.setState({type:type})
            data = {
                user_id : user_id,
                type    : type
            }
        } else {

            let {route} = this.props;
            console.log('route',route);
            let params = route.params;
            this.setState({type:params.type})
            // console.log('a_data',params.connections.id);
            data = {
                user_id : params.connections.id,
                type    : params.type
            }
        }
        
        // this.setState({connections: params.connections}) 
        let response = await get_connection(data);
        // let response = await get_users();
        if(response.data.status){
            let s_data = {
                user_id : this.state.auth_user_detail.id,
                type    : 'suggested'
            }
        
            let s_response = await get_connection(s_data);
            if(s_response.data.status){
                this.setState({suggestions: s_response.data.data});
            }
            this.setState({connections: response.data.data});
        }
    }
    getSuggetions = async() => {
        let data = {
                user_id : this.state.auth_user_detail.id,
                type    : 'suggested'
            }
        
        // this.setState({connections: params.connections}) 
        let response = await get_connection(data);
        if(response.data.status){
            this.setState({suggestions: response.data.data});
        }
    }
    addConnection = async(connection_id, status, index, where) => {
        let data = {
            user_id         : this.state.auth_user_detail.id,
            connection_id   : connection_id,
            status          : status,
        }
        let response = await add_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){

            if(where == 'suggetion'){

                let suggestions = this.state.suggestions;
                if(status == 'requested'){
                    suggestions[index].auth_user_request    = true;
                    // suggestions[index].auth_user_follower   = false;            
                    suggestions[index].auth_user_following  = false;            
                } else {
                    suggestions[index].auth_user_request    = false;
                    // suggestions[index].auth_user_follower   = true;
                    suggestions[index].auth_user_following  = true;            
                }
                
                this.setState({ suggestions });

            } else {

                let connections = this.state.connections;
                if(status == 'requested'){
                    connections[index].auth_user_request    = true;
                    // connections[index].auth_user_follower   = false;            
                    connections[index].auth_user_following  = false;            
                } else {
                    connections[index].auth_user_request    = false;
                    // connections[index].auth_user_follower   = true;
                    connections[index].auth_user_following  = true;            
                }
                
                this.setState({ connections });
            }

            let s_data = {
                user_id : this.state.auth_user_detail.id,
                type    : 'suggested'
            }
        
            // this.setState({connections: params.connections}) 
            let s_response = await get_connection(s_data);
            if(s_response.data.status){
                this.setState({suggestions: s_response.data.data});
            }

        }

    }

    removeConnection = async(user_id,status, index, where) => {
        let data = {
            user_id         : user_id,
            status          : status,
        }
        let response = await remove_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){
            
            if(where == 'suggetion'){
                let suggestions = this.state.suggestions;
                suggestions[index].auth_user_following  = false;            
                suggestions[index].auth_user_request    = false;            
                
                this.setState({ suggestions });
                

            } else {

                let connections = this.state.connections;
                connections[index].auth_user_following  = false;            
                connections[index].auth_user_request    = false;            
                
                this.setState({ connections });
                
            }
            let s_data = {
                user_id : this.state.auth_user_detail.id,
                type    : 'suggested'
            }
        
            // this.setState({connections: params.connections}) 
            let s_response = await get_connection(s_data);
            if(s_response.data.status){
                this.setState({suggestions: s_response.data.data});
            }
        }
            
    }

    async componentDidMount(){
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});
        this.getConnections();
        this.getSuggetions();

    }
    onRefresh = async() =>  {
          
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});
        this.getConnections();
        this.getSuggetions();
          wait(2000).then(() => this.setState({refreshing:false}));
        }
    render(){
        console.log('conne', this.state.suggestions);
        let {route} = this.props;
        let image_url = url + 'public/storage/';
        let { navigation } = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <SafeAreaView>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={()=>this.getConnections(route.params.connections.id,'follower')}   style={[this.state.type != 'follower' ? styles.activeTab : '',{flex:.6}]}>
                                <Text style={[styles.tabTitle, this.state.type == 'followers' ? {color:'#ce4061'} : '']}>Followers</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.getConnections(route.params.connections.id,'following')}  style={[this.state.type != 'following' ? styles.activeTab : '',{flex:.6}]}>
                                <Text style={[styles.tabTitle,this.state.type == 'following' ? {color:'#ce4061'} : '']}>Following</Text>
                            </TouchableOpacity>
                        </View>
                        <ScrollView 
                            contentContainerStyle={styles.scrollView}
                            refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={()=>this.onRefresh()}
                            />
                            }
                        >
                            <View style={styles.container}>
                                {
                                    this.state.connections.length != 0 ?
                                    this.state.connections.map((item, index) => {
                                        return(
                                            <>     
                                                {
                                                    this.state.type == 'follower' 
                                                    ?
                                                    <View style={styles.notification_container}>
                                                        <View style={{flexDirection:'row'}}>
                                                            <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                <TouchableOpacity onPress={()=>item.user.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                                    <Image source={item.user ? {uri: image_url+item.user.image} : ''} style={styles.post_profile_image} />
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                                <TouchableOpacity onPress={()=>item.user.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',item.user) : navigation.push('Profile',item.user)}>
                                                                    <Text style={{fontWeight:'bold',fontSize:16}}>{item.user ? item.user.user_name : ''}</Text>
                                                                    <Text style={{color:'#aaa'}}>{item.user ? item.user.fname : ''} {item.user ? item.user.lname : ''}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                {
                                                                    this.state.auth_user_detail.id != item.user.id ?
                                                                    item.auth_user_request
                                                                    ?
                                                                    <TouchableOpacity onPress={()=>this.removeConnection(item.user.id, 'request_me', index)}>
                                                                        <Text style={styles.following_btn}>Requested</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    item.auth_user_following
                                                                    ?
                                                                    <TouchableOpacity onPress={()=>this.removeConnection(item.user.id, 'following', index)}>
                                                                        <Text style={styles.following_btn}>Following</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity onPress={()=>this.addConnection(item.user.id, item.user.privacy_status == 'private' ? 'requested' : 'followed', index)}>
                                                                        <Text style={styles.follow_btn}>Follow</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <></>

                                                                }
                                                            </View>
                                                        </View>
                                                    </View>
                                                    : 
                                                    <View style={styles.notification_container}>
                                                        <View style={{flexDirection:'row'}}>
                                                            <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                <TouchableOpacity onPress={()=>item.connection.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',item.connection) : navigation.push('Profile',item.connection)}>
                                                                    <Image source={item.connection ? {uri: image_url+item.connection.image} : ''} style={styles.post_profile_image} />
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                                <TouchableOpacity onPress={()=>item.connection.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',item.connection) : navigation.push('Profile',item.connection)}>
                                                                    <Text style={{fontWeight:'bold',fontSize:16}}>{item.connection ? item.connection.user_name : ''}</Text>
                                                                    <Text style={{color:'#aaa'}}>{item.connection ? item.connection.fname : ''} {item.connection ? item.connection.lname : ''}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                {
                                                                    this.state.auth_user_detail.id != item.connection.id ?
                                                                    item.auth_user_request
                                                                    ?
                                                                    <TouchableOpacity  onPress={()=>this.removeConnection(item.connection.id, 'request_me', index)}>
                                                                        <Text style={styles.following_btn}>Requested</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    item.auth_user_following
                                                                    ?
                                                                    <TouchableOpacity  onPress={()=>this.removeConnection(item.connection.id, 'following', index)}>
                                                                        <Text style={styles.following_btn}>Following</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity onPress={()=>this.addConnection(item.connection.id, item.connection.privacy_status == 'private' ? 'requested' : 'followed', index)}>
                                                                        <Text style={styles.follow_btn}>Follow</Text>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <></>

                                                                }
                                                                
                                                            </View>
                                                        </View>
                                                    </View>
                                                } 
                                                
                                                
                                            </>

                                            
                                        )
                                    }) 
                                    :
                                    <>
                                    {
                                        this.state.type == 'follower' ?
                                        <View style={{alignItems:'center',paddingVertical:100}}>
                                            <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold'}}>Followers</Text> 
                                            <Text style={{fontSize:14,textAlign:'center'}}>You'll see people who follow you hare.</Text>
                                        </View>
                                        :
                                        <View style={{alignItems:'center',paddingVertical:100}}>
                                            <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold'}}>People You Follow</Text> 
                                            <Text style={{fontSize:14,textAlign:'center'}}>Once you follow people, you'll see them hare.</Text>
                                        </View>
                                    }
                                    </>
                                }
                                <View>
                                    <Text style={[styles.notification_container,{fontSize:20,fontWeight:'bold'}]}>Suggestions for you</Text>
                                    {
                                        this.state.suggestions.map((suggested, index)=>{
                                            return(
                                                <View style={styles.notification_container}>
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                            <TouchableOpacity onPress={()=>suggested.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',suggested) : navigation.push('Profile',suggested)}>
                                                                <Image source={suggested ? {uri: image_url+suggested.image} : ''} style={styles.post_profile_image} />
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                            <TouchableOpacity onPress={()=>suggested.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',suggested) : navigation.push('Profile',suggested)}>
                                                                <Text style={{fontWeight:'bold',fontSize:16}}>{suggested ? suggested.user_name : ''}</Text>
                                                                <Text style={{color:'#aaa'}}>{suggested ? suggested.fname : ''} {suggested ? suggested.lname : ''}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                            {
                                                                suggested.auth_user_request
                                                                ?
                                                                <TouchableOpacity  onPress={()=>this.removeConnection(suggested.id, 'request_me', index, 'suggetion')}>
                                                                    <Text style={styles.following_btn}>Requested</Text>
                                                                </TouchableOpacity>
                                                                :
                                                                suggested.auth_user_following
                                                                ?
                                                                <TouchableOpacity  onPress={()=>this.removeConnection(suggested.id, 'following', index, 'suggetion')}>
                                                                    <Text style={styles.following_btn}>Following</Text>
                                                                </TouchableOpacity>
                                                                :
                                                                <TouchableOpacity onPress={()=>this.addConnection(suggested.id, suggested.privacy_status == 'private' ? 'requested' : 'followed', index, 'suggetion')}>
                                                                    <Text style={styles.follow_btn}>Follow</Text>
                                                                </TouchableOpacity>

                                                            }
                                                            
                                                        </View>
                                                    </View>
                                                </View>
                                
                                            );
                                        })
                                    }                                                                                  
                                
                                </View>
                                

                                
                            </View>
                        </ScrollView>
                    </SafeAreaView>
                );
            } else {
                return(
                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}