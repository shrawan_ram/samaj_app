import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import { Dimensions, FlatList, Image, RefreshControl, SafeAreaView, ScrollView, Switch, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ico";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import ReadMore from "react-native-read-more-text";
import { user } from "../api/home.api";
import { add_connection, edit_user_detail, remove_connection, user_detail } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class ProfileScreen extends Component{
    state = {
        posts           :[],
        user_detail     :'',
        auth_user_detail:'',
        p_status         : ''

    }
    userDetail = async() => {

        let {route} = this.props;
        // if(route ? route.params.id )
        // let get_user_id = '';
        let user_id = route.params ? route.params.id : this.state.auth_user_detail.id;
        console.log('abcdss',user_id);
        let response = await user_detail(user_id);
        if(response.data.status){
            this.setState({user_detail:response.data.data});
            this.setState({p_status:this.state.user_detail.privacy_status == 'private' ? true : false});
        }

    }
    editUserDetail = async(p_status) => {
console.log('p_status',p_status);
        this.setState({p_status : p_status});
        let user_id = this.state.user_detail.id;
        let data = {
            privacy_status : p_status ? 'private' : 'public'
        }
        let response = await edit_user_detail(data, user_id);
        console.log('res',response);
        if(response.data.status){
            
            // this.setState({private : !this.state.private});
            // this.setState({p_status:response.data.data.privacy_status == 'private' ? true : false});
        }

    }
    addConnection = async(connection_id, status, index, where) => {
        let data = {
            user_id         : this.state.auth_user_detail.id,
            connection_id   : connection_id,
            status          : status,
        }
        let response = await add_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){
            if(where == 'suggetion'){

                let suggestions = this.state.suggestions;
                if(status == 'requested'){
                    suggestions[index].auth_user_request    = true;
                    // suggestions[index].auth_user_follower   = false;            
                    suggestions[index].auth_user_following  = false;            
                } else {
                    suggestions[index].auth_user_request    = false;
                    // suggestions[index].auth_user_follower   = true;
                    suggestions[index].auth_user_following  = true;            
                }
                
                this.setState({ suggestions });

            } else {

                let user_detail = this.state.user_detail;
                if(status == 'requested'){
                    user_detail.auth_user_request    = true;
                    // user_detail.auth_user_follower   = false;            
                    user_detail.auth_user_following  = false;            
                } else {
                    user_detail.followers_count++;
                    user_detail.auth_user_request    = false;
                    // user_detail.auth_user_follower   = true;
                    user_detail.auth_user_following  = true;            
                }
                
                this.setState({ user_detail });
            }
        }

    }
    Logout = async() => {
        await AsyncStorage.removeItem('@user');
        await AsyncStorage.removeItem('@token');
        // user = user !== null ? JSON.parse(user) : '';
        // this.setState({auth_user_detail: user});
        this.props.navigation.push('Login');

    }
    removeConnection = async(user_id,status, index, where) => {
        let data = {
            user_id         : user_id,
            status          : status,
        }
        let response = await remove_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){
            
            if(where == 'suggetion'){
                let suggestions = this.state.suggestions;
                suggestions[index].auth_user_following  = false;            
                suggestions[index].auth_user_request    = false;            
                
                this.setState({ suggestions });
                

            } else {

                let user_detail = this.state.user_detail;
                user_detail.followers_count--;
                user_detail.auth_user_following  = false;            
                user_detail.auth_user_request    = false;            
                
                this.setState({ user_detail });
                
            }
        }
            
    }

    async componentDidMount() {
        
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});

        this.userDetail();
        wait(2000).then(() => this.setState({refreshing:false}));

    }
    onRefresh = async() =>  {
          
        this.userDetail();

        // await AsyncStorage.removeItem('@user');
        // await AsyncStorage.removeItem('@token');
        // user = user !== null ? JSON.parse(user) : '';
        // this.setState({auth_user_detail: user});


          wait(2000).then(() => this.setState({refreshing:false}));
        }
    render(){
        let image_url = url + 'public/storage/';
        let user_detail = this.state.user_detail;
        // console.log('posts', user_detail.posts);
        let auth_user_detail = this.state.auth_user_detail;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        let { navigation } = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
        //   const toggleSwitch = () => this.setState({private : !this.state.private});
          
        //   console.log('status',this.state.p_status);
          
            if (this.state.loading) {
                return(
                    <SafeAreaView>
                        
                        <ScrollView
                        contentContainerStyle={styles.scrollView}
                        refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={()=>this.onRefresh()}
                        />
                        }>
                            <View style={{
                                paddingHorizontal:10,
                                paddingVertical:10
                            }}>
                                <View style={{
                                    flexDirection:'row'
                                }}>
                                    <View style={{
                                        flex:.2,                            
                                        
                                        height:100,
                                        overflow:'hidden',
                                        justifyContent:'center'
                                    }}>
                                        <Image 
                                        style={{
                                            resizeMode:'cover',
                                            height:66,
                                            width:66,
                                            borderRadius:50,
                                            borderWidth:.5,
                                            borderColor:'#aaa',
                                        }}
                                        source={ user_detail ? {uri : image_url+user_detail.image} : require('../../assests/images/male_user.jpg')} />
                                    </View>
                                    <View style={{
                                        flex:.6,
                                        justifyContent:'center'
                                    }}>
                                        <Text style={{fontSize:20,fontWeight:'bold'}}>{user_detail.user_name}</Text>
                                        <Text>{user_detail.fname} {user_detail.lname}</Text>
                                    </View>
                                    {
                                        auth_user_detail.id == user_detail.id
                                        ?<>
                                        <View style={{
                                            flex:.1,
                                        }}>
                                            <TouchableOpacity onPress={() => navigation.push('EditProfile',user_detail)} style={{backgroundColor:'#ce4061',alignItems:'center',padding:10,borderRadius:5}}>
                                                <Icon name='create-new-pencil-button' width="100%" color="#fff" />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{
                                            flex:.1,paddingLeft:5
                                        }}>
                                            <TouchableOpacity onPress={() => this[RBSheet + user_detail.id].open()} style={{alignItems:'center',padding:10,borderRadius:5,borderWidth:1,borderColor:'#aaa'}}>
                                                <Icon name='three-dots-more-indicator' width="100%" color="#000" />
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                        :
                                        <></>
                                    }
                                    
                                </View>
                            </View>
                                <View style={{ flexDirection:'row',paddingHorizontal:10,justifyContent:'center',paddingVertical:15 }}>
                                    <View style={{flex:.33,justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{fontSize:16,fontWeight:'bold'}}>{user_detail.posts_count}</Text>
                                        <Text>Posts</Text>
                                    </View>
                                    <TouchableOpacity  onPress={()=> navigation.push('Connections',{connections: user_detail,type:'follower'})} style={{flex:.33,justifyContent:'center',alignItems:'center'}}   disabled={user_detail.id == auth_user_detail.id ? false : user_detail.privacy_status == 'private' ? true : false}>
                                        <Text style={{fontSize:16,fontWeight:'bold'}}>{user_detail.followers_count}</Text>
                                        <Text>Follwers</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=> navigation.push('Connections',{connections: user_detail,type:'following'})} style={{flex:.33,justifyContent:'center',alignItems:'center'}} disabled={user_detail.id == auth_user_detail.id ? false : user_detail.privacy_status == 'private' ? true : false}>
                                        <Text style={{fontSize:16,fontWeight:'bold'}}>{user_detail.following_count}</Text>
                                        <Text>Following</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{paddingVertical:10,paddingHorizontal:10}}>
                                {
                                    auth_user_detail.id == user_detail.id
                                    ?
                                    <></>
                                    :
                                    user_detail.auth_user_request
                                        ?
                                        <TouchableOpacity onPress={()=>this.removeConnection(user_detail.id, 'request_me','')}>
                                            <Text style={styles.following_btn}>Requested</Text>
                                        </TouchableOpacity>
                                        :
                                        user_detail.auth_user_following
                                        ?
                                        <TouchableOpacity onPress={()=>this.removeConnection(user_detail.id, 'following','')}>
                                            <Text style={styles.following_btn}>Following</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={()=>this.addConnection(user_detail.id, user_detail.privacy_status == 'private' ? 'requested' : 'followed')}>
                                            <Text style={styles.follow_btn}>Follow</Text>
                                        </TouchableOpacity>

                                }
                                </View>
                                {
                                    user_detail.id == auth_user_detail.id ?
                                    <>
                                        <View style={{paddingVertical:10,paddingHorizontal:10,borderTopWidth:.2,borderTopColor:'#aaa'}}>
                                            {
                                                user_detail ? user_detail.posts.length != 0  
                                                ?
                                                <Text style={{fontSize:20,fontWeight:'bold'}}>All Posts</Text>
                                                :
                                                <View style={{paddingVertical:50,alignItems:'center'}}>
                                                    {/* <Icon name='locked-padlock-outline' width="100%" style={{marginBottom:15}} /> */}
                                                    <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold'}}>No Posts Yet</Text> 
                                                    {/* <Text style={{fontSize:14,textAlign:'center'}}>Follow this account to see their photos and videos.</Text> */}
                                                </View>
                                                :
                                                <></>

                                            }
                                        </View>
                                        <FlatList
                                            data={user_detail ? user_detail.posts : ''}
                                            numColumns={3}
                                            renderItem={({item}) => (
                                                <TouchableOpacity
                                                onPress={() => navigation.push('Post',item)}
                                                style={{                       
                                                        // flex:1,
                                                        flexDirection: 'column',
                                                        margin: 1,
                                                        justifyContent:'space-evenly',
                                                        // backgroundColor:'#fff',
                                                        // borderRadius:5,
                                                        // overflow:'hidden',
                                                        // shadowColor: "#000",
                                                        // shadowOffset: {
                                                        //     width: 0,
                                                        //     height: 1,
                                                        // },
                                                        // shadowOpacity: 0.20,
                                                        // shadowRadius: 1.41,

                                                        // elevation: 2,
                                                        }}>
                                                    <View>
                                                        {
                                                            item.image ?
                                                            <Image source={{uri: image_url+item.image}} style={[styles.item_image, { width:deviceWidth/3.05,
                                                                height: 130,resizeMode:'cover'
                                                            }]}/>
                                                            :
                                                            <View style={{justifyContent:'center',width:deviceWidth/3.05,height:130}}>

                                                                <Text style={{fontSize:20,textAlign:'center'}}>{item.caption}</Text>
                                                            </View>
                                                        }
                                                        
                                                    </View>
                                                </TouchableOpacity>
                                            
                                            )}
                                            //Setting the number of column
                                            
                                            keyExtractor={(item, index) => index}
                                        />
                                    </>
                                    :
                                    <>
                                        {
                                            user_detail.privacy_status == 'private' && !user_detail.auth_user_following ?
                                            <View style={{paddingVertical:50,borderTopWidth:.2,borderTopColor:'#aaa',alignItems:'center'}}>
                                                <Icon name='locked-padlock-outline' width="100%" style={{marginBottom:15}} />
                                                <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold'}}>This Acount is Private</Text> 
                                                <Text style={{fontSize:14,textAlign:'center'}}>Follow this account to see their photos and videos.</Text>
                                            </View>
                                            :
                                            <>
                                                <View style={{paddingVertical:10,paddingHorizontal:10,borderTopWidth:.2,borderTopColor:'#aaa'}}>
                                                    {
                                                        user_detail ? user_detail.posts.length != 0  
                                                        ?
                                                        <Text style={{fontSize:20,fontWeight:'bold'}}>All Posts</Text>
                                                        :
                                                        <View style={{paddingVertical:50,alignItems:'center'}}>
                                                            {/* <Icon name='locked-padlock-outline' width="100%" style={{marginBottom:15}} /> */}
                                                            <Text style={{fontSize:20,textAlign:'center',fontWeight:'bold'}}>No Posts Yet</Text> 
                                                            {/* <Text style={{fontSize:14,textAlign:'center'}}>Follow this account to see their photos and videos.</Text> */}
                                                        </View>
                                                        :
                                                        <></>

                                                    }
                                                </View>
                                                <FlatList
                                                    data={user_detail ? user_detail.posts : ''}
                                                    numColumns={3}
                                                    renderItem={({item}) => (
                                                        <TouchableOpacity
                                                        onPress={() => navigation.push('Post',item)}
                                                        style={{                       
                                                                // flex:1,
                                                                flexDirection: 'column',
                                                                margin: 1,
                                                                justifyContent:'space-evenly',
                                                                // backgroundColor:'#fff',
                                                                // borderRadius:5,
                                                                // overflow:'hidden',
                                                                // shadowColor: "#000",
                                                                // shadowOffset: {
                                                                //     width: 0,
                                                                //     height: 1,
                                                                // },
                                                                // shadowOpacity: 0.20,
                                                                // shadowRadius: 1.41,

                                                                // elevation: 2,
                                                                }}>
                                                            <View>
                                                                {
                                                                    item.image ?
                                                                    <Image source={{uri: image_url+item.image}} style={[styles.item_image, { width:deviceWidth/3.05,
                                                                        height: 130,resizeMode:'cover'
                                                                    }]}/>
                                                                    :
                                                                    <View style={{justifyContent:'center',width:deviceWidth/3.05,height:130}}>

                                                                        <Text style={{fontSize:20,textAlign:'center'}}>{item.caption}</Text>
                                                                    </View>
                                                                }
                                                                
                                                            </View>
                                                        </TouchableOpacity>
                                                    
                                                    )}
                                                    //Setting the number of column
                                                    
                                                    keyExtractor={(item, index) => index}
                                                />
                                            </>
                                        }
                                    </>
                                }
                                
                                <RBSheet
                                    ref={ref => {
                                        this[RBSheet + user_detail.id] = ref;
                                    }}
                                    draggableIcon={true}
                                    closeOnDragDown={true}
                                    // height={140}
                                    customStyles={{
                                        container: {
                                        //   justifyContent: "center",
                                        //   alignItems: "center",
                                            borderTopLeftRadius:20,
                                            borderTopRightRadius:20,
                                            height:'50%'
                                        }
                                        }}
                                >
                                    {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                    </View> */}
                                    <TouchableOpacity onPress={() => navigation.push('ChangePassword')}  style={{paddingHorizontal:15,paddingVertical:10}}
                                        >
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                    <Icon name="lock" group="essential" width="100%" />
                                                </View>
                                                <View style={{flex:.7,paddingHorizontal:10}}>
                                                    <Text style={{fontSize:18}}>Change Password</Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={{paddingHorizontal:15,paddingVertical:10}}
                                        >
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                    <Icon name="padlock" group="ui-interface" width="100%" />
                                                </View>
                                                <View style={{flex:.7,paddingHorizontal:10}}>
                                                    <Text style={{fontSize:18}}>Private Account</Text>
                                                </View>
                                                <View style={{flex:.2,alignItems:'flex-end'}}>
                                                    <Switch
                                                        trackColor={{ false: "#767577", true: "#ce406155" }}
                                                        thumbColor={this.state.p_status ? "#ce4061" : "#f4f3f4"}
                                                        ios_backgroundColor="#3e3e3e"
                                                        value={this.state.p_status}
                                                        onValueChange={(p_status) => this.editUserDetail(p_status)}
                                                    />    
                                                </View> 
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{paddingHorizontal:15,paddingVertical:10}}
                                        //  onPress={() => this.setState({modalVisible: true})}
                                        onPress={() =>this.Logout()}
                                            >
                                                <View style={{flexDirection:'row'}}>
                                                    <View style={{flex:.1, justifyContent:'center',alignItems:'center'}}>
                                                        <Icon name="logout" group="miscellaneous" width="50%" />
                                                    </View>
                                                    <View style={{flex:.9,paddingHorizontal:10}}>
                                                        <Text style={{fontSize:18}}>Logout</Text>
                                                    </View> 
                                                </View>
                                        </TouchableOpacity>
                                        
                                </RBSheet>
                        </ScrollView>
                    </SafeAreaView>
                ); 
            } else {
                return(
                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}