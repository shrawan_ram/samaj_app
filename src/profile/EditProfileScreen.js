import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import { Dimensions, FlatList, Image, Modal, Picker, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { edit_profile, get_city, get_gotra, user_detail } from "../api/user.api";
import { url } from "../config/constants.config";
import RadioGroup from 'react-native-radio-buttons-group';
import { styles } from "./style";
import SelectDropdown from "react-native-select-dropdown";
var deviceWidth = Dimensions.get('window').width;
export default class EditProfileScreen extends Component{
    state = {
        user_detail     :'',
        auth_user_detail:'',
        modalVisible    :false,
        cities          :{},
        gotra          :{},
        radioButtonsData: [{
            id: '1', // acts as primary key, should be unique and non-empty string
            label: 'Male',
            color:'#ce4061',
            
        }, {
            id: '2',
            label: 'Female',
            selected: true,
            color:'#ce4061',
        }],
        user_name       :'',
        fname           :'',
        lname           :'',
        user_name       :'',
        mobile          :'',
        email           :'',
        gender          :'',
        city_id         :'',
        gotra_id        :'',
        address         :'',
        pincode         :'',
        image           :null,

    }
    userDetail = async() => {
        let {route} = this.props;
        let user_id = route.params.id;
        let response = await user_detail(user_id);
        if(response.data.status){
            this.setState({user_detail:response.data.data});
        }
    }

    getCity = async() => {
        let response = await get_city();
        console.log('city',response);
        if(response.status){
            // let res = JSON.parse(response.data);
                // console.log('response:', response.data);
            this.setState({cities:response.data});
        }
    }
    getGotra = async() => {
        let response = await get_gotra();
        console.log('city',response);
        if(response.status){
            // let res = JSON.parse(response.data);
                // console.log('response:', response.data);
            this.setState({gotra:response.data});
        }
    }

    onPressRadioButton(radioButtonsArray) {
        console.log('radop btns: ', radioButtonsArray);
        this.setState({radioButtonsData: radioButtonsArray});
    }

    handleChoosePhoto (type = 'camera', field = 'image') {
        // console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        
        type === 'camera' ? 
            launchCamera(options, (response) => {

                console.log('res',response);
               

                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });                                
                }
            })
            : launchImageLibrary(options, (response) => {
                    if (response.assets[0].uri) {
                            self.setState({ [this.state.field]: response.assets[0] });
                    }
            });
    }

    editProfile = async () => {
        
            let data = {
                user_name   : this.state.user_name,
                fname       : this.state.fname,
                lname       : this.state.lname,
                mobile      : this.state.mobile,
                email       : this.state.email,
                city_id     : this.state.city_id,
                role_id     : 2,
                gotra_id    : this.state.gotra_id,
                address     : this.state.address,
                pincode     : this.state.pincode,
                
            } 
            console.log('data', data);
            let user_id     = this.state.auth_user_detail.id;
          
            data = this.createFormData(this.state.image, data);
            let response = await edit_profile(data, user_id);
            console.log('responseee', response);
         
          if (response.data.status) {
            this.props.navigation.navigate('UserProfile',this.state.auth_user_detail); 
          }
        }

        createFormData = (image, body = {}) => {
            const data = new FormData();
            
            if(image) {
                data.append('image', {
                    name: image.fileName,
                    type: image.type,
                    uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
                });
            }
            
            
            Object.keys(body).forEach((key) => {
                data.append(key, body[key]);
            });
            
            return data;
        } 
  
          
    
    async componentDidMount() {
        // this.userDetail();
        this.getCity();
        this.getGotra();

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});

        let {route} = this.props;
        let user_detail = route.params;

        this.setState({user_name: user_detail.user_name}); 
        this.setState({fname    : user_detail.fname}); 
        this.setState({lname    : user_detail.lname}); 
        this.setState({mobile   : user_detail.mobile}); 
        this.setState({email    : user_detail.email}); 
        this.setState({gender   : user_detail.gender}); 
        this.setState({address  : user_detail.address}); 
        this.setState({city_id     : user_detail.city_id}); 
        this.setState({gotra_id     : user_detail.gotra_id}); 
        this.setState({pincode  : user_detail.pincode}); 
        // this.setState({image: user_d.image}); 
        this.setState({pincode  : user_detail.pincode}); 
    }
   
    render(){
        let image_url = url + 'public/storage/';
        let user_detail = this.state.user_detail;
        let auth_user_detail = this.state.auth_user_detail;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
//         let ab = Object.keys(this.state.cities).map((key) => [Number(key), this.state.cities[key]])
    
console.log('city_list', this.state.cities);
     
        let { navigation } = this.props;
        
        return(

          

            <SafeAreaView>
                <ScrollView style={{marginBottom:40}}>
                    <View style={{
                        paddingHorizontal:10,
                        paddingVertical:10
                    }}>
                        <View>
                            <View style={{backgroundColor:"",paddingHorizontal:15,paddingVertical:10,alignItems:'center'}}>
                                <View style={{borderRadius:50,overflow:'hidden',width:100,height:100,justifyContent:'center',backgroundColor:'#fff',alignSelf:'center',borderWidth:.5, borderColor:'#aaa'}}>
                                <Image source={ this.state.image ? this.state.image : require('../../assests/images/male_user.jpg')} style={styles.profile_image} />
                                </View>
                                <TouchableOpacity onPress={() =>this.setState({modalVisible: true, field: 'image'})}>
                                    <Text style={{fontSize:20,paddingTop:8}}>Change Profile Photo</Text>
                                </TouchableOpacity>
                                
                            </View>
                        </View>
                        <View >
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Username"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.user_name}  
                                onChangeText={user_name => this.setState({ user_name })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "First name"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.fname}  
                                onChangeText={fname => this.setState({ fname })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Last name"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.lname}  
                                onChangeText={lname => this.setState({ lname })}
                            />

                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Mobile No."
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.mobile}  
                                onChangeText={mobile => this.setState({ mobile })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Email"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.email}  
                                onChangeText={email => this.setState({ email })}
                            />
                            <View style={{flexDirection:'row',justifyContent:'center'}}>
                                <View style={{flex:.2,justifyContent:'center'}}>
                                    <Text style={{justifyContent:'flex-start',fontWeight:'bold'}}>Gender : </Text>
                                </View>
                                <View style={{flex:.8}}>
                                    <RadioGroup 
                                        radioButtons={this.state.radioButtonsData} 
                                        onPress={() => this.onPressRadioButton(this.state.radioButtonsData)}
                                        layout='row'
                                        containerStyle={{width:'100%',color:'#fff'}}                                
                                        
                                    />
                                </View>                                
                            </View>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#000' }}>
                                <Picker
                                    selectedValue={this.state.city_id}
                                    onValueChange={city_id => this.setState({ city_id })}
                                >
                                    <Picker.Item label='Select City' value='' />
                                    {
                                        this.state.cities.length && this.state.cities.map((city,index) => {
                                            return(
                                                <Picker.Item label={city.name} value={city.id} />
                                            )
                                        })
                                    }
                                    
                                </Picker>
                            </View>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#000' }}>
                                <Picker
                                    selectedValue={this.state.gotra_id}
                                    onValueChange={gotra_id => this.setState({ gotra_id })}
                                >
                                    <Picker.Item label='Select Gotra' value='' />
                                    {
                                        this.state.gotra.length && this.state.gotra.map((item,index) => {
                                            return(
                                                <Picker.Item label={item.name} value={item.id} />
                                            )
                                        })
                                    }
                                    
                                </Picker>
                            </View>
                            
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Address"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.address}  
                                onChangeText={address => this.setState({ address })}
                            />
                            <TextInput style = {styles.input}
                                underlineColorAndroid = "transparent"
                                placeholder = "Pincode"
                                placeholderTextColor = "#000"
                                selectionColor={'#000'}
                                autoCapitalize = "none"
                                value={this.state.pincode}  
                                onChangeText={pincode => this.setState({ pincode })}
                            />
                            
                        </View>
                            
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({modalVisible: false});
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                    <Text style={styles.modalText}>Take Photo...</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                    <Text style={styles.modalText}>Choose From library...</Text>
                                </TouchableOpacity>
                            
                                <TouchableOpacity
                                onPress={() => this.setState({modalVisible: false})}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
                <View style={{position:'absolute',bottom:0,left:0,right:0,backgroundColor:'#fff',paddingVertical:5,paddingHorizontal:10,borderTopWidth:1,borderTopColor:'#ccc'}}>
                    {
                        this.state.user_name && this.state.mobile && this.state.fname && this.state.lname && this.state.email && this.state.gender && this.state.city_id && this.state.gotra_id && this.state.address && this.state.pincode
                        ?
                        <TouchableOpacity onPress={() => this.editProfile()}>
                            <Text style={styles.submit_btn}>Edit</Text>
                        </TouchableOpacity>
                        :
                        <Text style={styles.disable_submit_btn}>Edit</Text>

                    }
                    
                </View>
            </SafeAreaView>
        );
    }
}