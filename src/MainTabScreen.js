import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import React, { Component } from "react";
import { Image } from "react-native";
import Icon from "react-native-ico-material-design";
import HomeScreen from "./home/HomeScreen";
import NewsScreen from "./news/NewsScreen";
import NotificationScreen from "./notification/NotificationScreen";
import PlaceScreen from "./place/PlaceScreen";
import ProfileScreen from "./profile/ProfileScreen";
import UserScreen from "./user/UserScreen";
import { SvgUri } from 'react-native-svg';
import PrayerScreen from "./prayer/PrayerScreen";
import UserProfileScreen from "./profile/UserProfileScreen";

const Tab = createMaterialBottomTabNavigator();

export default class MainTabScreen extends Component {
    render(){
        return(
            <Tab.Navigator
                initialRouteName="Home"
                activeColor="#fff"                
                barStyle={{ backgroundColor: '#ce4061' }}
                >
                <Tab.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                    
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <Icon name="home-button" color={color} height="20" width="20" />
                        // <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                    }}
                />
                <Tab.Screen
                    name="News"
                    component={NewsScreen}
                    options={{
                    tabBarLabel: 'News',
                    tabBarIcon: ({ color }) => (
                        <Icon name="window-with-different-sections" color={color} height="20" width="20" />
                        
                    ),
                    }}
                />
                <Tab.Screen
                    name="Users"
                    component={PrayerScreen}
                    options={{
                    tabBarLabel: 'Prayer',
                    tabBarIcon: ({ color }) => (
                        // <Icon name="users-social-symbol" height="20" color={color} width="20" />
                        <Image source={require('../assests/images/prayer.png')} style={{height:20,width:20}}/>
                        
                    ),
                    }}
                />
                
                <Tab.Screen
                    name="Place"
                    component={PlaceScreen}
                    options={{
                    tabBarLabel: 'Places',
                    tabBarIcon: ({ color }) => (
                        <Icon name="map-placeholder" height="20" color={color} width="20" />
                    ),
                    }}
                />
                <Tab.Screen
                    name="UserProfile"
                    component={UserProfileScreen}
                    
                    // options = ({navigation})=>({
                    // tabBarLabel: 'Profile',
                    // tabBarIcon: ({ color }) => (
                    //     <Icon name="user-shape" height="20" color={color} width="20" />
                    // ),
                    // })
                    options={({navigation}) => ({
                        
                        tabBarLabel: 'Profile',
                        tabBarIcon: ({ color }) => (
                            <Icon name="user-shape" height="20" color={color} width="20" />
                        ),
                        
                    })}
                />
               
                </Tab.Navigator>
        );
    }
}