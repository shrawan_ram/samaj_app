import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import {View, Text, ScrollView, SafeAreaView, Image, Modal, Pressable, TouchableOpacity, RefreshControl} from "react-native";
import Icon from "react-native-ico";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import { get_notification } from "../api/notification.api";
import { add_connection, remove_connection, update_connection } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class NotificationScreen extends Component {
    state = {
        notifications   : [],
        auth_user_detail: '',
        modalVisible    : false,
    }
    getNotification = async() => {
        let response = await get_notification();
        if(response.data.status){
            this.setState({notifications:response.data.data});
        }
    }
    updateConnection = async(id, index) => {
        let data = {
            id         :    id,
            status     : 'followed',
        }
        let response = await update_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){

                let notifications = this.state.notifications;
                
                    notifications[index].auth_user_request    = false;
                    notifications[index].auth_user_follower   = true;
                    // notifications[index].auth_user_following  = true;            
                
                
                this.setState({ notifications });


            

        }

    }
    addConnection = async(connection_id, status, index) => {
        let data = {
            user_id         : this.state.auth_user_detail.id,
            connection_id   : connection_id,
            status          : status,
        }
        let response = await add_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){

                let notifications = this.state.notifications;
                if(status == 'requested'){
                    notifications[index].auth_user_request    = true;
                    // notifications[index].auth_user_follower   = false;            
                    notifications[index].auth_user_following  = false;            
                } else {
                    notifications[index].auth_user_request    = false;
                    // notifications[index].auth_user_follower   = true;
                    notifications[index].auth_user_following  = true;            
                }
                
                this.setState({ notifications });


            

        }

    }

    removeConnection = async(user_id,status, index) => {
        let data = {
            user_id         : user_id,
            status          : status,
        }
        let response = await remove_connection(data);
        console.log('res',response);
        // console.log('data',data);
        if(response.data.status){
            
                let notifications = this.state.notifications;
                notifications[index].auth_user_following  = false;            
                notifications[index].auth_user_request    = false;            
                
                this.setState({ notifications });
                
            
        }
            
    }
    async componentDidMount(){
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user}); 

        this.getNotification();
    }
    onRefresh = async() =>  {
          
        this.getNotification();
          wait(2000).then(() => this.setState({refreshing:false}));
        }
    render(){
        let image_url = url + 'public/storage/';
        let { navigation } = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <SafeAreaView>
                        <ScrollView
                            contentContainerStyle={styles.scrollView}
                            refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={()=>this.onRefresh()}
                            />
                            }
                        >
                            
                            <View style={styles.container}>
                                {
                                    this.state.notifications.map((notification, index) => {
                                        return(
                                            <>
                                            {(() => {
                                                    if(notification.type == 'comment'){
                                                        return (
                                                        <TouchableOpacity onLongPress={() => this[RBSheet + index].open()}>
                                                            <View style={styles.notification_container}>
                                                                <View style={{flexDirection:'row'}}>
                                                                    <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'flex-start',backgroundColor:'#fff'}}>
                                                                        <Image source={{uri : image_url+notification.sender.image}} style={styles.post_profile_image} />
                                                                    </TouchableOpacity>
                                                                    
                                                                    <TouchableOpacity onPress={()=>navigation.push('Post',notification.post)} style={{flex:.7, paddingHorizontal:10,justifyContent:'center',}}>
                                                                    <Text><Text style={{fontWeight:'bold',fontSize:16}}>{notification.sender.user_name} </Text>commented. {notification.comment_post.comment} <Text style={{color:'#ccc'}}>{moment.utc(notification.created_at).local().startOf('seconds').fromNow()}</Text></Text>
                                                                        
                                                                    </TouchableOpacity>
                                                                    <TouchableOpacity onPress={()=>navigation.push('Post',notification.post)} style={{flex:.15, justifyContent:'flex-start',alignItems:'flex-end'}}>
                                                                        <Image source={{uri:image_url+notification.post.image}} style={styles.post_liked_image} />
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                {(() => {
                                                    if(notification.type == 'event'){
                                                        return (
                                                        <TouchableOpacity onLongPress={() => this[RBSheet + index].open()} onPress={() => navigation.push('NewsDetail',notification.event)}>
                                                            <View style={styles.notification_container}>
                                                                <View style={{flexDirection:'row'}}>
                                                                    <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'flex-start',backgroundColor:'#fff'}}>
                                                                        <Image source={require('../../assests/images/male_user.jpg')} style={styles.post_profile_image} />
                                                                    </View>
                                                                    
                                                                    <View style={{flex:.7, paddingHorizontal:10,justifyContent:'center',}}>
                                                                    <Text><Text style={{fontWeight:'bold',fontSize:16}}></Text>New event added. <Text style={{color:'#ccc'}}>{moment.utc(notification.created_at).local().startOf('seconds').fromNow()}</Text></Text>
                                                                        
                                                                    </View>
                                                                    <View style={{flex:.15, justifyContent:'flex-start',alignItems:'flex-end'}}>
                                                                        <Image source={notification.event.image ? { uri: image_url+notification.event.image} : require('../../assests/images/news.jpg')} style={styles.post_liked_image} />
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                {(() => {
                                                    if(notification.type == 'like'){
                                                        return (
                                                        <TouchableOpacity onLongPress={() => this[RBSheet + index].open()}>
                                                            <View style={styles.notification_container}>
                                                                <View style={{flexDirection:'row'}}>
                                                                    <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'flex-start',backgroundColor:'#fff'}}>
                                                                        <Image source={{uri : image_url+notification.sender.image}} style={styles.post_profile_image} />
                                                                    </TouchableOpacity>
                                                                    
                                                                    <TouchableOpacity onPress={()=>notification.post_id ? navigation.push('Post',notification.post) : navigation.push('Post',notification.comment_post.post)} style={{flex:.7, paddingHorizontal:10,justifyContent:'flex-start',}}>
                                                                    <Text><Text style={{fontWeight:'bold',fontSize:16}}>{notification.sender.user_name} </Text>liked your { notification.post_id ? 'photo.': 'comment: '+notification.comment_post.comment }  <Text style={{color:'#ccc'}}>{moment.utc(notification.created_at).local().startOf('seconds').fromNow()}</Text></Text>
                                                                        
                                                                    </TouchableOpacity>
                                                                    <TouchableOpacity onPress={()=>notification.post_id ? navigation.push('Post',notification.post) : navigation.push('Post',notification.comment_post.post)} style={{flex:.15, justifyContent:'flex-start',alignItems:'flex-end'}}>
                                                                        {
                                                                            notification.post_id 
                                                                            ?
                                                                            <Image source={{uri:image_url+notification.post.image}} style={styles.post_liked_image} />
                                                                            :
                                                                            <Image source={{uri:image_url+notification.comment_post.post.image}} style={styles.post_liked_image} />

                                                                        }
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                
                                                {(() => {
                                                    if(notification.type == 'follow'){
                                                        return (
                                                            <TouchableOpacity onLongPress={() => this[RBSheet + index].open()} onPress={() => navigation.navigate('Home')}>
                                                                <View style={styles.notification_container}>
                                                                    <View style={{flexDirection:'row'}}>
                                                                        <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                            <Image source={{uri : image_url+notification.sender.image}} style={styles.post_profile_image} />
                                                                        </TouchableOpacity>
                                                                        
                                                                        <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:.60, paddingHorizontal:10,justifyContent:'center',}}>
                                                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>{notification.sender.user_name} </Text>Started following you. <Text style={{color:'#ccc'}}>{moment.utc(notification.created_at).local().startOf('seconds').fromNow()}</Text></Text>
                                                                            
                                                                        </TouchableOpacity>
                                                                        <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                        {
                                                                            this.state.auth_user_detail.id != notification.sender.id ?
                                                                            notification.auth_user_request
                                                                            ?
                                                                            <TouchableOpacity onPress={()=>this.removeConnection(notification.sender.id, 'request_me', index)}>
                                                                                <Text style={styles.following_btn}>Requested</Text>
                                                                            </TouchableOpacity>
                                                                            :
                                                                            notification.auth_user_following
                                                                            ?
                                                                            <TouchableOpacity onPress={()=>this.removeConnection(notification.sender.id, 'following', index)}>
                                                                                <Text style={styles.following_btn}>Following</Text>
                                                                            </TouchableOpacity>
                                                                            :
                                                                            <TouchableOpacity onPress={()=>this.addConnection(notification.sender.id, notification.sender.privacy_status == 'private' ? 'requested' : 'followed', index)}>
                                                                                <Text style={styles.follow_btn}>Follow</Text>
                                                                            </TouchableOpacity>
                                                                            :
                                                                            <></>

                                                                        }
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                {(() => {
                                                    if(notification.type == 'request'){
                                                        return (
                                                            <TouchableOpacity onLongPress={() => this[RBSheet + index].open()} onPress={() => navigation.navigate('Home')}>
                                                                <View style={styles.notification_container}>
                                                                    <View style={{flexDirection:'row'}}>
                                                                        <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                            <Image source={{uri : image_url+notification.sender.image}} style={styles.post_profile_image} />
                                                                        </TouchableOpacity>
                                                                        
                                                                        <TouchableOpacity onPress={()=>notification.sender.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',notification.sender) : navigation.push('Profile',notification.sender)} style={{flex:notification.auth_user_follower ? .6 : .55, paddingHorizontal:10,justifyContent:'center',}}>
                                                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>{notification.sender.user_name} </Text>Requested to follow you. <Text style={{color:'#ccc'}}>{moment.utc(notification.created_at).local().startOf('seconds').fromNow()}</Text></Text>
                                                                            
                                                                        </TouchableOpacity>
                                                                        {
                                                                            notification.auth_user_follower ?
                                                                            <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                                            {
                                                                                this.state.auth_user_detail.id != notification.sender.id ?
                                                                                notification.auth_user_request
                                                                                ?
                                                                                <TouchableOpacity onPress={()=>this.removeConnection(notification.sender.id, 'request_me', index)}>
                                                                                    <Text style={styles.following_btn}>Requested</Text>
                                                                                </TouchableOpacity>
                                                                                :
                                                                                notification.auth_user_following
                                                                                ?
                                                                                <TouchableOpacity onPress={()=>this.removeConnection(notification.sender.id, 'following', index)}>
                                                                                    <Text style={styles.following_btn}>Following</Text>
                                                                                </TouchableOpacity>
                                                                                :
                                                                                <TouchableOpacity onPress={()=>this.addConnection(notification.sender.id, notification.sender.privacy_status == 'private' ? 'requested' : 'followed', index)}>
                                                                                    <Text style={styles.follow_btn}>Follow</Text>
                                                                                </TouchableOpacity>
                                                                                :
                                                                                <></>

                                                                            } 
                                                                            </View>
                                                                            :
                                                                            <>                                                                            
                                                                                <View style={{flex:.2, justifyContent:'center',alignContent:'space-between'}}>
                                                                                    <TouchableOpacity onPress={()=>this.updateConnection(notification.connection_id, index)}>
                                                                                            <Text style={styles.follow_btn}>Confirm</Text>
                                                                                        </TouchableOpacity>
                                                                                    
                                                                                </View>
                                                                                <View style={{flex:.1, justifyContent:'center',alignContent:'space-between',paddingLeft:2}}>
                                                                                    <TouchableOpacity onPress={()=>this.removeConnection(notification.sender.id, 'requested', index)} style={[styles.following_btn,{paddingVertical:6}]}>
                                                                                        <Icon name="cancel" group="ui-interface" width="90%"/>
                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                            </>
                                                                            
                                                                        }
                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                {(() => {
                                                    if(notification.type == 'following'){
                                                        return (
                                                            <TouchableOpacity onLongPress={() => this[RBSheet + index].open()} onPress={() => navigation.navigate('Home')}>
                                                                <View style={styles.notification_container}>
                                                                    <View style={{flexDirection:'row'}}>
                                                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                                                            <Image source={notification.dp} style={styles.post_profile_image} />
                                                                        </View>
                                                                        
                                                                        <View style={{flex:.64, paddingHorizontal:10,justifyContent:'center',}}>
                                                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>{notification.name} </Text>{notification.desc}. <Text style={{color:'#ccc'}}>{notification.time}</Text></Text>
                                                                            
                                                                        </View>
                                                                        <View style={{flex:.21, justifyContent:'center',alignContent:'space-between'}}>
                                                                            <TouchableOpacity>
                                                                                <Text style={styles.following_btn}>Following</Text>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    
                                                    return null;
                                                })()}
                                                <RBSheet
                                                    ref={ref => {
                                                        this[RBSheet + index] = ref;
                                                    }}
                                                    draggableIcon={true}
                                                    closeOnDragDown={true}
                                                    height={100}
                                                    customStyles={{
                                                        container: {
                                                        //   justifyContent: "center",
                                                        //   alignItems: "center",
                                                            borderTopLeftRadius:20,
                                                            borderTopRightRadius:20,
                                                        }
                                                        }}
                                                >
                                                    {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                    </View> */}
                                                        <TouchableOpacity
                                                        //  onPress={() => this.setState({modalVisible: true})}
                                                        >
                                                                <View>
                                                                    <Text style={{color:'red',fontSize:18,padding:10}}>Delete Notification</Text>
                                                                </View>
                                                        </TouchableOpacity>
                                                        
                                                </RBSheet>
                                                {/* <Modal
                                                    // animationType="slide"
                                                    transparent={true}
                                                    visible={this.state.modalVisible}
                                                    onRequestClose={() => {
                                                    Alert.alert("Modal has been closed.");
                                                    this.setState({modalVisible: false});
                                                    }}
                                                >
                                                    <View style={styles.centeredView}>
                                                        <View style={styles.modalView}>
                                                            <TouchableOpacity >
                                                                    <Text style={styles.modalText}>Take Photo...</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity >
                                                                    <Text style={styles.modalText}>Choose From library...</Text>
                                                            </TouchableOpacity>                                                
                                                            <TouchableOpacity
                                                            onPress={() => this.setState({modalVisible: false})}
                                                            >
                                                                <Text style={styles.textStyle}>Cancel</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </Modal> */}
                                            </>

                                            
                                        )
                                    })
                                }
                                
                                {/* <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.7, paddingHorizontal:10,justifyContent:'center',}}>
                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                        </View>
                                        <View style={{flex:.15, justifyContent:'center',alignItems:'flex-end'}}>
                                            <TouchableOpacity>
                                                <Text style={styles.follow_btn}>Follow</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.64, paddingHorizontal:10,justifyContent:'center',}}>
                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                        </View>
                                        <View style={{flex:.21, justifyContent:'center',alignItems:'flex-end'}}>
                                            <TouchableOpacity>
                                                <Text style={styles.following_btn}>Following</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.8, paddingHorizontal:10,justifyContent:'center',}}>
                                            <Text style={{fontWeight:'bold',fontSize:16}}>Jaipal Saini</Text>
                                        </View>
                                        <View style={{flex:.1, justifyContent:'center',alignItems:'flex-end'}}>
                                            <Icon name='show-more-button-with-three-dots' width="50%" />
                                        </View>
                                    </View>
                                </View> */}

                                
                            </View>
                        </ScrollView>
                    </SafeAreaView>
                );
            } else {
                return(

                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                        <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}