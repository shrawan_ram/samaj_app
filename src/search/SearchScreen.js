import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Image, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { get_users, search_users } from "../api/user.api";
import { url } from "../config/constants.config";
import { styles } from "./style";

export default class SearchScreen extends Component{
    state = {
        auth_user_detail:'',
        users : [],
        name:''
    }
    getUsers = async(name) => {
        this.setState({name});
        
        console.log('data',name);
        let response = await search_users(name);
        console.log('res',response);
        if(response.data.status){
            this.setState({users: response.data.data});
        }
    }
    

    async componentDidMount(){
        this.getUsers();

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({auth_user_detail: user});
    }
    render(){
        let image_url = url + 'public/storage/';
        let {navigation} = this.props;
        console.log('navigation', this.state.name);
        return(
            <SafeAreaView>
                <ScrollView>
                {/* <TextInput style={styles.input}
                            underlineColorAndroid = "transparent"
                            placeholder = "Search"
                            placeholderTextColor = "#aaa"
                            autoCapitalize = "none"
                            value={this.state.name}  
                            onChangeText={(name) =>  this.getUsers(name)}
                            
                            
                        /> */}
                <View style={{flexDirection:'row',padding:15}}>
                    <View style={{flex:1}}>
                        <View>
                            <TextInput style={styles.input}
                                autoFocus={true}
                                underlineColorAndroid = "transparent"
                                placeholder = "Search"
                                placeholderTextColor = "#aaa"
                                autoCapitalize = "none"
                                value={this.state.name}  
                                onChangeText={(name) =>  this.getUsers(name)}
                                
                            />
                            
                            <View  style={{position:'absolute',right:10,bottom:'17%'}}>
                                <Icon color='#a1a1a1' size={20}  name='searching-magnifying-glass' />
                            </View>
                        </View>
                    </View>
                    
                </View>
                {
                    this.state.name ?
                    this.state.users.length == 0 ?
                    <Text style={{textAlign:'center',paddingVertical:8,backgroundColor:'#fff'}}>User Not Found</Text>
                    :
                    <></>
                    :
                    <></>
                }
                <View style={styles.container}>
                        {   
                            this.state.users.map((user, index) => {
                                return(
                                    <>                                                   
                                        <View style={styles.notification_container}>
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.15, borderRadius:50, overflow:'hidden', justifyContent:'center', backgroundColor:'#fff'}}>
                                                    <TouchableOpacity onPress={()=>user.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',user) : navigation.push('Profile',user)}>
                                                        <Image source={{uri: image_url+user.image}} style={styles.post_profile_image} />
                                                    </TouchableOpacity>
                                                </View>
                                                
                                                <View style={{flex:.6, paddingHorizontal:10,justifyContent:'center',}}>
                                                    <TouchableOpacity onPress={()=>user.id == this.state.auth_user_detail.id ? navigation.navigate('UserProfile',user) : navigation.push('Profile',user)}>
                                                        <Text style={{fontWeight:'bold',fontSize:16}}>{user.user_name} </Text>
                                                        <Text style={{color:'#aaa'}}>{user.fname} {user.lname}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{flex:.25, justifyContent:'center',alignContent:'space-between'}}>
                                                    {/* <TouchableOpacity onPress={()=>this.addConnection(user.id, user.privacy_status == 'private' ? 'requested' : 'followed')}>
                                                        <Text style={styles.follow_btn}>Follow</Text>
                                                    </TouchableOpacity> */}
                                                </View>
                                            </View>
                                        </View>
                                       
                                        
                                    </>

                                    
                                )
                            })
                        }
                        

                        
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}