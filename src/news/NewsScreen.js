import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import {View, Text, ScrollView, SafeAreaView, Image, Modal, Pressable, TouchableOpacity, Dimensions, Share, TextInput, RefreshControl} from "react-native";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from "react-native-scrollable-tab-view";
import { event_like, get_news } from "../api/news.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class NewsScreen extends Component {
    state = {
        news : [
            // {
            //     id      : 1,
            //     title   : 'चेन्नई की कंपनी बना रही फ्लाइंग कार',
            //     heading : 'ये बैटरी और बायो फ्यूल दोनों से उड़ेगी, 2 लोग कर पाएंगे हवाई सफर; डच कंपनी की ऐसी कार 1 घंटे में 321km करती है सफर',
            //     desc    : 'नागरिक उड्डयन मंत्री (Civil Aviation Minister) ज्योतिरादित्य सिंधिया ने स्टार्ट-अप आईडिया की प्रशंसा की और इस हाइब्रिड कार की पेशकश की संभावनाओं के बारे में उत्साह व्यक्त किया। नागरिक उड्डयन मंत्री ज्योतिरादित्य सिंधिया के हवाले से एएनआई ने कहा, "विनाटा एयरोमोबिलिटी (VINATA AeroMobility) की युवा टीम द्वारा जल्द ही एशिया की पहली हाइब्रिड फ्लाइंग (Asia’s First Hybrid Flying Automobile) कार बनाने की कंसेप्ट मॉडल को पेश किया गया है।" बता दें, निर्माताओं के अनुसार हाइब्रिड वर्टिकल टेक-ऑफ और लैंडिंग (Vertical Take-off and Landing) वाहन 3,000 फीट की ऊंचाई तक पहुंच सकता है। इस कार में दो लोगों के बैठने की जगह होगी। फ्लाइंग कार ज्यादा से ज्यादा 1,300 किलोग्राम वजन लेकर उड़ सकता है। रिपोर्ट्स के अनुसार, यह एक इलेक्ट्रिक हाइब्रिड है और निर्माता 60 मिनट तक के उड़ान समय और 120 किलोमीटर प्रति घंटे की तेज रफ्तार से 100 किलोमीटर की दूरी तय करने का दावा करते हैं। इस कार को आठ बीएलडीसी (BLDC) मोटर्स द्वारा संचालित एक सह-अक्षीय क्वाड-रोटर सिस्टम द्वारा चलाया जाता है। इसमें समान संख्या में फिक्स्ड-पिच प्रोपेलर होते हैं। विनाटा एयरोमोबिलिटी ने कहा कि यदि फ्लाइंग हाइब्रिड के रोटरों में से एक विफल हो जाता है, तो अन्य ऑपरेटिंग मोटर बिना किसी नुकसान के विमान को सुरक्षित रूप से उतार सकते हैं। चेन्नई में योगेश रामनाथन (Yogesh Ramanathan) ने विनाटा एयरोमोबिलिटी की शुरुआत की है। विनाटा एयरोमोबिलिटी को इसरो (ISRO) के टॉप अंतरिक्ष वैज्ञानिकों ने बनाया है, जिसमें डॉ एई मुथुनायगम (with Dr AE Muthunayagam) प्रमुख सलाहकार के रूप में कार्यरत हैं। पूर्व अमेरिकी वायु सेना कर्नल डॉन ज़ोल्डी (Dawn Zoldi) के पास 28 से अधिक वर्षों का अनुभव है और इस कंपनी में वह अर्बन एयर मोबिलिटी (Urban Air Mobility) सलाहकार के रूप में शामिल हैं। Vinata Aeromobility सेल्फ-ड्राइविंग हाइब्रिड फ्लाइंग कारों पर काम कर रही है। कंपनी की योजना उनकी आधिकारिक वेबसाइट के अनुसार शहर की यात्रा को काफी तेज, आसान और कम खर्चीला बनाना है। विनाटा का डिजाइन वर्टिकल टेक-ऑफ और लैंडिंग (VTOL), इलेक्ट्रिक और लो या नो एमिशन फ्यूल हाइब्रिड पावर टेक्नोलॉजी को अत्याधुनिक ऑटोमोटिव इंजीनियरिंग के साथ मिलाया गया है।',                
            //     image   : require('../../assests/images/Veer-Teja.jpg'),
            //     time    : '1d' ,
            //     type    : 'like'
            // },
            // {
            //     id      : 2,
            //     title   : 'अमेजन का बड़ा एक्शन',
            //     heading : 'ई-कॉमर्स प्लेटफॉर्म पर एक साथ 600 चीनी ब्रांड्स को बैन किया, फेक रिव्यू के लिए कंपनी 2500 रुपए देती थी',
            //     desc    : 'नागरिक उड्डयन मंत्री (Civil Aviation Minister) ज्योतिरादित्य सिंधिया ने स्टार्ट-अप आईडिया की प्रशंसा की और इस हाइब्रिड कार की पेशकश की संभावनाओं के बारे में उत्साह व्यक्त किया। नागरिक उड्डयन मंत्री ज्योतिरादित्य सिंधिया के हवाले से एएनआई ने कहा, "विनाटा एयरोमोबिलिटी (VINATA AeroMobility) की युवा टीम द्वारा जल्द ही एशिया की पहली हाइब्रिड फ्लाइंग (Asia’s First Hybrid Flying Automobile) कार बनाने की कंसेप्ट मॉडल को पेश किया गया है।" बता दें, निर्माताओं के अनुसार हाइब्रिड वर्टिकल टेक-ऑफ और लैंडिंग (Vertical Take-off and Landing) वाहन 3,000 फीट की ऊंचाई तक पहुंच सकता है। इस कार में दो लोगों के बैठने की जगह होगी। फ्लाइंग कार ज्यादा से ज्यादा 1,300 किलोग्राम वजन लेकर उड़ सकता है। रिपोर्ट्स के अनुसार, यह एक इलेक्ट्रिक हाइब्रिड है और निर्माता 60 मिनट तक के उड़ान समय और 120 किलोमीटर प्रति घंटे की तेज रफ्तार से 100 किलोमीटर की दूरी तय करने का दावा करते हैं। इस कार को आठ बीएलडीसी (BLDC) मोटर्स द्वारा संचालित एक सह-अक्षीय क्वाड-रोटर सिस्टम द्वारा चलाया जाता है। इसमें समान संख्या में फिक्स्ड-पिच प्रोपेलर होते हैं। विनाटा एयरोमोबिलिटी ने कहा कि यदि फ्लाइंग हाइब्रिड के रोटरों में से एक विफल हो जाता है, तो अन्य ऑपरेटिंग मोटर बिना किसी नुकसान के विमान को सुरक्षित रूप से उतार सकते हैं। चेन्नई में योगेश रामनाथन (Yogesh Ramanathan) ने विनाटा एयरोमोबिलिटी की शुरुआत की है। विनाटा एयरोमोबिलिटी को इसरो (ISRO) के टॉप अंतरिक्ष वैज्ञानिकों ने बनाया है, जिसमें डॉ एई मुथुनायगम (with Dr AE Muthunayagam) प्रमुख सलाहकार के रूप में कार्यरत हैं। पूर्व अमेरिकी वायु सेना कर्नल डॉन ज़ोल्डी (Dawn Zoldi) के पास 28 से अधिक वर्षों का अनुभव है और इस कंपनी में वह अर्बन एयर मोबिलिटी (Urban Air Mobility) सलाहकार के रूप में शामिल हैं। Vinata Aeromobility सेल्फ-ड्राइविंग हाइब्रिड फ्लाइंग कारों पर काम कर रही है। कंपनी की योजना उनकी आधिकारिक वेबसाइट के अनुसार शहर की यात्रा को काफी तेज, आसान और कम खर्चीला बनाना है। विनाटा का डिजाइन वर्टिकल टेक-ऑफ और लैंडिंग (VTOL), इलेक्ट्रिक और लो या नो एमिशन फ्यूल हाइब्रिड पावर टेक्नोलॉजी को अत्याधुनिक ऑटोमोटिव इंजीनियरिंग के साथ मिलाया गया है।',                                
            //     image   : require('../../assests/images/tejaji.jpg'),
            //     time    : '1d' ,
            //     type    : 'follow'
            // },
            // {
            //     id      : 3,
            //     title   : 'एपल iOS 15 अपडेट लॉन्च',
            //     heading : 'नोटिफिकेशन को फ्री टाइम के हिसाब से कर सकेंगे मैनेज; शेयर प्ले, फेसटाइम कॉल पर वीडियो और ऑडियो को लाइव शेयर भी कर पाएंगे', 
            //     desc    : 'नागरिक उड्डयन मंत्री (Civil Aviation Minister) ज्योतिरादित्य सिंधिया ने स्टार्ट-अप आईडिया की प्रशंसा की और इस हाइब्रिड कार की पेशकश की संभावनाओं के बारे में उत्साह व्यक्त किया। नागरिक उड्डयन मंत्री ज्योतिरादित्य सिंधिया के हवाले से एएनआई ने कहा, "विनाटा एयरोमोबिलिटी (VINATA AeroMobility) की युवा टीम द्वारा जल्द ही एशिया की पहली हाइब्रिड फ्लाइंग (Asia’s First Hybrid Flying Automobile) कार बनाने की कंसेप्ट मॉडल को पेश किया गया है।" बता दें, निर्माताओं के अनुसार हाइब्रिड वर्टिकल टेक-ऑफ और लैंडिंग (Vertical Take-off and Landing) वाहन 3,000 फीट की ऊंचाई तक पहुंच सकता है। इस कार में दो लोगों के बैठने की जगह होगी। फ्लाइंग कार ज्यादा से ज्यादा 1,300 किलोग्राम वजन लेकर उड़ सकता है। रिपोर्ट्स के अनुसार, यह एक इलेक्ट्रिक हाइब्रिड है और निर्माता 60 मिनट तक के उड़ान समय और 120 किलोमीटर प्रति घंटे की तेज रफ्तार से 100 किलोमीटर की दूरी तय करने का दावा करते हैं। इस कार को आठ बीएलडीसी (BLDC) मोटर्स द्वारा संचालित एक सह-अक्षीय क्वाड-रोटर सिस्टम द्वारा चलाया जाता है। इसमें समान संख्या में फिक्स्ड-पिच प्रोपेलर होते हैं। विनाटा एयरोमोबिलिटी ने कहा कि यदि फ्लाइंग हाइब्रिड के रोटरों में से एक विफल हो जाता है, तो अन्य ऑपरेटिंग मोटर बिना किसी नुकसान के विमान को सुरक्षित रूप से उतार सकते हैं। चेन्नई में योगेश रामनाथन (Yogesh Ramanathan) ने विनाटा एयरोमोबिलिटी की शुरुआत की है। विनाटा एयरोमोबिलिटी को इसरो (ISRO) के टॉप अंतरिक्ष वैज्ञानिकों ने बनाया है, जिसमें डॉ एई मुथुनायगम (with Dr AE Muthunayagam) प्रमुख सलाहकार के रूप में कार्यरत हैं। पूर्व अमेरिकी वायु सेना कर्नल डॉन ज़ोल्डी (Dawn Zoldi) के पास 28 से अधिक वर्षों का अनुभव है और इस कंपनी में वह अर्बन एयर मोबिलिटी (Urban Air Mobility) सलाहकार के रूप में शामिल हैं। Vinata Aeromobility सेल्फ-ड्राइविंग हाइब्रिड फ्लाइंग कारों पर काम कर रही है। कंपनी की योजना उनकी आधिकारिक वेबसाइट के अनुसार शहर की यात्रा को काफी तेज, आसान और कम खर्चीला बनाना है। विनाटा का डिजाइन वर्टिकल टेक-ऑफ और लैंडिंग (VTOL), इलेक्ट्रिक और लो या नो एमिशन फ्यूल हाइब्रिड पावर टेक्नोलॉजी को अत्याधुनिक ऑटोमोटिव इंजीनियरिंग के साथ मिलाया गया है।',                               
            //     image   : require('../../assests/images/mahadev.jpg'),
            //     time    : '1d' ,
            //     type    : 'following'
            // },
        ],
        modalVisible: false,
        type:'',
        user_detail:''
    }

    getNews = async(type="news") => {
        
        let data = {
            type : 'news'
        }
        let response = await get_news(type);
        
        this.setState({type:type});
        console.log('res',response);
        if(response.data.status){
            this.setState({news:response.data.data});
        }
    }
    eventLike = async(event_id, index) => {

        // let { posts } = this.state;
        // posts.id = !posts.auth_post_like;

        let news = this.state.news;

        news[index].auth_event_like = !news[index].auth_event_like;
        
        if(news[index].auth_event_like == true){
            news[index].like_event_count++ ;
        } else {
            news[index].like_event_count-- ;            
        }
        this.setState({ news });

        let data = {
            event_id : event_id,
            user_id : this.state.user_detail.id
        }
        let response = await event_like(data);
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }
    onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'https://www.instagram.com/p/CVIKmn9vsua/?utm_medium=share_sheet',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };
    async componentDidMount(){
        this.getNews();

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
    }
    onRefresh = async() =>  {
          
        this.getNews();

        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});

          wait(2000).then(() => this.setState({refreshing:false}));
        }
    render(){
        let image_url = url+'public/storage/';
        let { navigation } = this.props;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <SafeAreaView>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={()=>this.getNews('news')} style={[this.state.type == 'news' ? styles.activeTab : '',{flex:.6}]}>
                                <Text style={[styles.tabTitle, this.state.type == 'news' ? {color:'#ce4061'} : '']}>News</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.getNews('event')} style={[this.state.type == 'event' ? styles.activeTab : '',{flex:.6}]}>
                                <Text style={[styles.tabTitle,this.state.type == 'event' ? {color:'#ce4061'} : '']}>Events</Text>
                            </TouchableOpacity>
                        </View>
                        <ScrollView style={{marginBottom:50}}
                        refreshControl={
                            <RefreshControl
                              refreshing={this.state.refreshing}
                              onRefresh={()=>this.onRefresh()}
                            />
                          }
                        >                   
                            <View>
                                {
                                    this.state.news.map((item, index) => {
                                        return(
                                            <>                                       
                                                <View style={styles.notification_container}>
                                                    <TouchableOpacity 
                                                    // onLongPress={() => this[RBSheet + index].open()} 
                                                    onPress={() => navigation.push('NewsDetail',item)}>
                                                        <View style={{flexDirection:'row'}}>                                                    
                                                            <View style={{flex:.75, paddingRight:10,justifyContent:'center',}}>
                                                                <Text style={{fontSize:18}}><Text style={{fontWeight:'bold',fontSize:18,color:'#ce4061'}}>{item.name} : </Text>{item.short_description} <Text style={{color:'#7e90a0'}}></Text></Text>
                                                                {
                                                                    

                                                                    item.type == 'event'
                                                                    ?
                                                                    item.date_type == 'single' ?
                                                                    <Text><Text style={{fontWeight:'bold'}}>Event Date : </Text>{moment(item.start_date).format('D MMM y')}</Text>
                                                                    :
                                                                    <>
                                                                        <Text><Text style={{fontWeight:'bold'}}>Start Event : </Text>{moment(item.start_date).format('D MMM y')}</Text>
                                                                        <Text><Text style={{fontWeight:'bold'}}>End Event : </Text>{moment(item.end_date).format('D MMM y')}</Text>
                                                                    </>
                                                                    :
                                                                    <></>
                                                                }
                                                                <Text></Text>
                                                                <Text style={{color:'#7e90a0'}}>{item.area}{item.area && item.city_id ? ',':''}  {item.city ? item.city.name : ''} | {moment.utc(item.created_at).local().startOf('seconds').fromNow()}</Text>
                                                            </View>
                                                            <View style={{flex:.25, justifyContent:'center',alignItems:'flex-end'}}>
                                                                {/* <Image source={item.image} style={styles.post_liked_image} /> */}
                                                                <Image source={item.image ? { uri: image_url+item.image} : require('../../assests/images/news.jpg')} style={[styles.post_image, { width:deviceWidth/4.7,
                                                                height:100
                                                                    // height: scaleHeight({
                                                                    //     source: item.image,
                                                                    //     desiredWidth: deviceWidth/4.7
                                                                    // })
                                                                }]}/>
                                                            </View>
                                                            
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{flexDirection:'row',paddingTop:15}}>
                                                        <View style={{flex:.75, paddingRight:10,justifyContent:'center',}}>
                                                            <View style={{flexDirection:'row'}}>

                                                            <Text style={{fontSize:18, color:'#7e90a0',fontWeight:'bold'}}>                                                        
                                                                <TouchableOpacity onPress={()=> this.eventLike(item.id, index)}>
                                                                    <Icon name="thumb-up-button" color={item.auth_event_like == true ? '#ce4061' : '#7e90a0'} height="20" width="20" style={{marginHorizontal:15}}/>                                                            
                                                                </TouchableOpacity>
                                                            </Text>
                                                            
                                                            <Text>{item.like_event_count == 0 ? '' : item.like_event_count}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{flex:.25,flexWrap:'wrap-reverse',flexDirection:'row', paddingHorizontal:10,justifyContent:'flex-end',}}>
                                                            {/* <TouchableOpacity>
                                                                <Icon name="thumb-up-button" color='#7e90a0' height="20" width="20" style={{marginHorizontal:15}}/>
                                                            </TouchableOpacity> */}
                                                            <TouchableOpacity onPress={()=>this.onShare()}>
                                                                <Icon name="share-button" color="#7e90a0" height="20" width="20" style={{marginLeft:5}} />
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>
                                                
                                                <RBSheet
                                                    ref={ref => {
                                                        this[RBSheet + index] = ref;
                                                    }}
                                                    draggableIcon={true}
                                                    closeOnDragDown={true}
                                                    height={100}
                                                    customStyles={{
                                                        container: {
                                                        //   justifyContent: "center",
                                                        //   alignItems: "center",
                                                            borderTopLeftRadius:20,
                                                            borderTopRightRadius:20,
                                                        }
                                                        }}
                                                >
                                                    {/* <View style={{borderBottomColor:'#ccc',borderBottomWidth:.5,paddingBottom:5}}>
                                                        <Text style={{fontWeight:'bold',fontSize:18,textAlign:'center'}}>Choose</Text>                                            
                                                    </View> */}
                                                        <TouchableOpacity
                                                        //  onPress={() => this.setState({modalVisible: true})}
                                                        >
                                                                <View>
                                                                    <Text style={{color:'red',fontSize:18,padding:10}}>Delete Notification</Text>
                                                                </View>
                                                        </TouchableOpacity>
                                                        
                                                </RBSheet>
                                                {/* <Modal
                                                    // animationType="slide"
                                                    transparent={true}
                                                    visible={this.state.modalVisible}
                                                    onRequestClose={() => {
                                                    Alert.alert("Modal has been closed.");
                                                    this.setState({modalVisible: false});
                                                    }}
                                                >
                                                    <View style={styles.centeredView}>
                                                        <View style={styles.modalView}>
                                                            <TouchableOpacity >
                                                                    <Text style={styles.modalText}>Take Photo...</Text>
                                                            </TouchableOpacity>
                                                            <TouchableOpacity >
                                                                    <Text style={styles.modalText}>Choose From library...</Text>
                                                            </TouchableOpacity>                                                
                                                            <TouchableOpacity
                                                            onPress={() => this.setState({modalVisible: false})}
                                                            >
                                                                <Text style={styles.textStyle}>Cancel</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </Modal> */}
                                            </>

                                            
                                        )
                                    })
                                }
                                
                                {/* <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.7, paddingHorizontal:10,justifyContent:'center',}}>
                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                        </View>
                                        <View style={{flex:.15, justifyContent:'center',alignItems:'flex-end'}}>
                                            <TouchableOpacity>
                                                <Text style={styles.follow_btn}>Follow</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.64, paddingHorizontal:10,justifyContent:'center',}}>
                                        <Text><Text style={{fontWeight:'bold',fontSize:16}}>pb_jatt </Text>Started following you. <Text style={{color:'#ccc'}}>1d</Text></Text>
                                        </View>
                                        <View style={{flex:.21, justifyContent:'center',alignItems:'flex-end'}}>
                                            <TouchableOpacity>
                                                <Text style={styles.following_btn}>Following</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.notification_container}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:.15, borderRadius:50,overflow:'hidden',justifyContent:'center',backgroundColor:'#fff'}}>
                                            <Image source={require('../../assests/images/user.jpg')} style={styles.post_profile_image} />
                                        </View>
                                        
                                        <View style={{flex:.8, paddingHorizontal:10,justifyContent:'center',}}>
                                            <Text style={{fontWeight:'bold',fontSize:16}}>Jaipal Saini</Text>
                                        </View>
                                        <View style={{flex:.1, justifyContent:'center',alignItems:'flex-end'}}>
                                            <Icon name='show-more-button-with-three-dots' width="50%" />
                                        </View>
                                    </View>
                                </View> */}

                                
                            </View>
                        </ScrollView>
                    </SafeAreaView>
                );
            } else {
                return(

                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}