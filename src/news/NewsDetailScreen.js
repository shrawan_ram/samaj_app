import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { Dimensions, Image, Share, Text, TouchableOpacity } from "react-native";
import { SafeAreaView, ScrollView, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import RBSheet from "react-native-raw-bottom-sheet";
import { event_like, get_news_detail } from "../api/news.api";
import { url } from "../config/constants.config";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
export default class NewsDetailScreen extends Component{
    state = {
        news        : [],
        user_detail : '',
        modalVisible: false,
    }
    getNewsDetail = async() => {
        let {route} = this.props;
        let news_id = route.params.id;
        let response = await get_news_detail(news_id);
        if(response.data.status){
            this.setState({news:response.data.data});
        }
    }
    eventLike = async(event_id, index) => {
        console.log('event_id',event_id,index);
        let data = '';
        if(event_id){
            let { news } = this.state;
            news.latest[index].auth_event_like = !news.latest[index].auth_event_like;
            if(news.latest[index].auth_event_like == true){
    
                news.latest[index].like_event_count++ ;
            } else {
                news.latest[index].like_event_count--;
                
            }
            this.setState({news})
            data = {
                event_id    : event_id,
                user_id     : this.state.user_detail.id
            }

        } else {

            let { news } = this.state;
            news.auth_event_like = !news.auth_event_like;
            if(news.auth_event_like == true){
    
                news.like_event_count++ ;
            } else {
                news.like_event_count--;
                
            }
            this.setState({news})
            data = {
                event_id : this.state.news.id,
                user_id : this.state.user_detail.id
            }
        }
        let response = await event_like(data);
        console.log('response',response);
        // console.log('res',response.data.status);
        let item = this.state.post_detail;
        if(response.data.status){
            // this.props.navigation.push('Post',item);
        }
    }
    onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'https://www.instagram.com/p/CVIKmn9vsua/?utm_medium=share_sheet',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };
    async componentDidMount(){
        this.getNewsDetail();

        
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : '';
        this.setState({user_detail: user});
    }
    render(){
        let { route, navigation } = this.props;
        let item = this.state.news;
        let image_url = url+'public/storage/';
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <SafeAreaView>
                        <ScrollView>
                            <View style={[styles.container,{backgroundColor:'#fff'}]}>
                                    <Text style={{fontSize:18,fontWeight:'bold',}}><Text style={{fontWeight:'bold',fontSize:18,color:'#ce4061'}}>{item.name} : </Text>{item.short_description}
                                    </Text>
                                    <Text style={{color:'#7e90a0'}}>{item.area}{item.area && item.city_id ? ',':''}{item.city ? item.city.name : ''} | {moment.utc(item.created_at).local().startOf('seconds').fromNow()}</Text>
                                    <Image source={item.image ? { uri: image_url+item.image} : require('../../assests/images/news.jpg')} style={[styles.post_image, { width:deviceWidth-20,
                                        height: scaleHeight({
                                            source: require('../../assests/images/Veer-Teja.jpg'),
                                            desiredWidth: deviceWidth
                                        }),marginVertical:10
                                    }]}/>
                                    <View style={{flexDirection:'row',paddingVertical:15}}>
                                        <View style={{flex:.65,justifyContent:'center'}}>

                                            {/* <Text style={{fontSize:18, color:'#7e90a0',fontWeight:'bold',textAlign:'left'}}>{moment.utc(item.created_at).local().startOf('seconds').fromNow()}</Text> */}
                                            <View style={{flexDirection:'row'}}>

                                            <TouchableOpacity onPress={()=> this.eventLike()}>
                                                <Icon name="thumb-up-button" color={item.auth_event_like == true ? '#ce4061' : '#7e90a0'} height="20" width="20" />
                                            </TouchableOpacity>
                                            <Text style={{paddingLeft:8}}>{item.like_event_count==0 ? '' : item.like_event_count}</Text>
                                            </View>
                                        </View>
                                        <View style={{flex:.35,flexWrap:'wrap-reverse',flexDirection:'row', paddingHorizontal:10,justifyContent:'flex-end',}}>
                                            
                                            <TouchableOpacity onPress={()=>this.onShare()}>
                                                <Icon name="share-button" color="#7e90a0" height="20" width="20" style={{marginLeft:5}} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    {                                                          

                                        item.type == 'event'
                                        ?
                                        item.date_type == 'single' ?
                                        <Text><Text style={{fontWeight:'bold',fontSize:18}}>Event Date : </Text>{moment(item.start_date).format('D MMM y')}</Text>
                                        :
                                        <>
                                            <Text style={{fontSize:18}}><Text style={{fontWeight:'bold',fontSize:18}}>Event Date : </Text>{moment(item.start_date).format('D MMM y')} - {moment(item.end_date).format('D MMM y')}</Text>
                                            {/* <Text style={{fontSize:18}}><Text style={{fontWeight:'bold',fontSize:18}}>Start Event : </Text>{moment(item.start_date).format('D MMM y')}</Text>
                                            <Text style={{fontSize:18}}><Text style={{fontWeight:'bold',fontSize:18}}>End Event : </Text>{moment(item.end_date).format('D MMM y')}</Text> */}
                                        </>
                                        :
                                        <></>
                                    }
                                    <Text style={{fontSize:18}}>
                                        {item.description}
                                    </Text>
                            </View>
                            <View style={{backgroundColor:'#fff'}}>
                                {
                                    item.latest ?
                                    <Text style={{ fontSize:20,fontWeight:'bold',padding:10}}>Latest News</Text>
                                    :
                                    <></>

                                }
                                {
                                    item.latest && item.latest.map((latest_news, latest_index) => {
                                        return(
                                            <>                                       
                                                <View style={styles.notification_container}>
                                                    <TouchableOpacity 
                                                    // onLongPress={() => this[RBSheet + latest_index].open()}
                                                    onPress={() => navigation.push('NewsDetail',latest_news)}>
                                                        <View style={{flexDirection:'row'}}>                                                    
                                                            <View style={{flex:.75, paddingRight:10,justifyContent:'center',}}>
                                                                <Text style={{fontSize:18}}><Text style={{fontWeight:'bold',fontSize:18,color:'#ce4061'}}>{latest_news.name} : </Text>{latest_news.short_description}. <Text style={{color:'#7e90a0'}}></Text></Text>
                                                            </View>
                                                            <View style={{flex:.25, justifyContent:'center',alignItems:'flex-end'}}>
                                                                <Image source={{uri : image_url+latest_news.image}} style={[styles.post_image, { width:deviceWidth/4.7,
                                                                height:100
                                                                    // // height: scaleHeight({
                                                                    // //     source: item.image,
                                                                    // //     desiredWidth: deviceWidth/4.7
                                                                    // })
                                                                }]}/>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{flexDirection:'row',paddingTop:15}}>
                                                        <View style={{flex:.75,justifyContent:'center',alignItems:'flex-start'}}>
                                                            {/* <Text style={{fontSize:18, color:'#7e90a0',fontWeight:'bold'}}>{item.time}</Text> */}
                                                            
                                                            <View style={{flexDirection:'row'}}>

                                                                <TouchableOpacity onPress={()=> this.eventLike(latest_news.id, latest_index)}>
                                                                    <Icon name="thumb-up-button" color={latest_news.auth_event_like == true ? '#ce4061' : '#7e90a0'} height="20" width="20" />
                                                                </TouchableOpacity>
                                                                <Text style={{paddingLeft:8}}>{latest_news.like_event_count==0 ? '' : latest_news.like_event_count}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{flex:.25,flexWrap:'wrap-reverse',flexDirection:'row', paddingHorizontal:10,justifyContent:'flex-end',}}>
                                                            
                                                            <TouchableOpacity onPress={()=>this.onShare()}>
                                                                <Icon name="share-button" color="#7e90a0" height="20" width="20" style={{marginLeft:5}} />
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>
                                                
                                                <RBSheet
                                                    ref={ref => {
                                                        this[RBSheet + latest_index] = ref;
                                                    }}
                                                    draggableIcon={true}
                                                    closeOnDragDown={true}
                                                    height={100}
                                                    customStyles={{
                                                        container: {
                                                            borderTopLeftRadius:20,
                                                            borderTopRightRadius:20,
                                                        }
                                                        }}
                                                >
                                                    
                                                        <TouchableOpacity
                                                        >
                                                                <View>
                                                                    <Text style={{color:'red',fontSize:18,padding:10}}>Delete Notification</Text>
                                                                </View>
                                                        </TouchableOpacity>
                                                        
                                                </RBSheet>
                                                
                                            </>

                                            
                                        )
                                    })
                                }

                            </View>
                        </ScrollView>
                    </SafeAreaView>
                ) 
            } else {
                return(

                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                      <ActivityIndicator  color="#ce4061" />
                    </View>
                )
            }
    }
}