import React, { Component } from "react";
import { Text } from "react-native";
import { SafeAreaView, ScrollView, View } from "react-native";

export default class ComingSoonScreen extends Component{
    render(){
        return(
                <View style={{position:'absolute',left:0,top:0,right:0,backgroundColor:'#fff',bottom:0,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:50,fontWeight:'bold',color:'#ce4061'}}>Coming</Text>
                    <Text style={{fontSize:40,fontWeight:'bold'}}>Soon</Text>
                </View>
        );
    }
}