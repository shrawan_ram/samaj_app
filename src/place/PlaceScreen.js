import React, { Component } from "react";
import { Dimensions, FlatList, Image, RefreshControl, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { place } from "../api/place.api";
import { styles } from "./style";
var deviceWidth = Dimensions.get('window').width;
let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
export default class PlaceScreen extends Component{
    state = {
        places : [
            // {
            //     id      : '1',
            //     name    : 'Ramgadhi Jat Hostel',                
            //     image   : require('../../assests/images/mahadev.jpg'),
            //     location: 'Plot No 1, Bhagwati colony, High Court Colony Road, Near Apex Bharti School, Ratanada, Jodhpur',
            //     desc    : 'Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.'
            // },
            // {
            //     id      : '2',
            //     name    : 'Sri Ramnarayan Choudhary Hostel',
            //     image   : require('../../assests/images/jinda-hostel.jpg'),
            //     location: 'Plot No 1, Bhagwati colony, High Court Colony Road, Near Apex Bharti School, Ratanada, Jodhpur',
            //     desc    : 'Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.'
            // },
            // {
            //     id      : '3',
            //     name    : 'Ramnarayan Jinda hostel (Jodhpur)',
            //     image   : require('../../assests/images/jinda-hostel.jpg'),
            //     location: 'Plot No 1, Bhagwati colony, High Court Colony Road, Near Apex Bharti School, Ratanada, Jodhpur',
            //     desc    : 'Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.'
            // },
            // {
            //     id      : '4',
            //     name    : 'Ramnarayan Jinda hostel (Jodhpur)',
            //     image   : require('../../assests/images/tejaji.jpg'),
            //     location: 'Plot No 1, Bhagwati colony, High Court Colony Road, Near Apex Bharti School, Ratanada, Jodhpur',
            //     desc    : 'Our company aim not only to integrate your online and offline presence but also generate rise and increased revenues for your business. Suncity Techno Pvt. Ltd. strategy services structured with the objective to help organizations achieve their digital transformation journey.'
            // },
        ]
    }
    getPlaces = async() => {
        let response = await place();
        console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            this.setState({places:response.data.data});
        }
    }
    async componentDidMount() {
        this.getPlaces();
    }
    onRefresh = async() =>  {
          
        this.getPlaces();
          wait(2000).then(() => this.setState({refreshing:false}));
        }
    render(){
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        console.log('place',this.state.places);
        let {navigation} = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
           if (this.state.loading) {
                return(
                    <SafeAreaView style={styles.container}>
                        <ScrollView 
                            contentContainerStyle={styles.scrollView}
                            refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={()=>this.onRefresh()}
                            />
                            }
                        > 
                            <FlatList
                                data={this.state.places}
                                numColumns={2}
                                renderItem={({item}) => (
                                    <TouchableOpacity
                                    onPress={() => navigation.push('PlaceDetail',item)}
                                    style={{                       
                                            flex:1,
                                            flexDirection: 'column',
                                            margin: 5,
                                            justifyContent:'space-evenly',
                                            backgroundColor:'#fff',
                                            borderRadius:5,
                                            overflow:'hidden',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 1,
                                            },
                                            shadowOpacity: 0.20,
                                            shadowRadius: 1.41,

                                            elevation: 2,
                                            }}>
                                        <View
                                            >
                                            {/* <Image style={styles.imageThumbnail} source={item.image} /> */}
                                            <Image source={require('../../assests/images/jinda-hostel.jpg')} style={[styles.post_image, { width:deviceWidth/2.1 - 11,
                                                height: 150
                                            }]}/>
                                            <View style={{padding:8}}>
                                                <Text style={{fontWeight:'bold',fontSize:20}}>{item.name}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                
                                )}
                                //Setting the number of column
                                
                                keyExtractor={(item, index) => index}
                            />
                        </ScrollView>
                    </SafeAreaView>
                );
            } else {
                return(

                <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                    <ActivityIndicator  color="#ce4061" />
                </View>
                )
            }
    }
}