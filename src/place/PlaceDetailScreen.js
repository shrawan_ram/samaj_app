import React, { Component } from "react";
import { Dimensions, Image, SafeAreaView, ScrollView, Text, View } from "react-native";
import Icon from "react-native-ico-material-design";
import { ActivityIndicator } from "react-native-paper";
import { styles } from "./style";

var deviceWidth = Dimensions.get('window').width;
export default class PlaceDetailScreen extends Component{
    state = {
        
    }
    render(){
        let {route, navigation} = this.props;
        let item = route.params;
        const scaleHeight = ({ source, desiredWidth }) => {
            const { width, height } = Image.resolveAssetSource(source)
        
            return desiredWidth / width * height
        }
        setTimeout(() => {
            this.setState({ loading: true })
          }, 2000)
          
            if (this.state.loading) {
                return(
                    <View style={{position:'absolute',top:0,bottom:0,left:0,right:0,backgroundColor:'#fff'}}>
                        <ScrollView>
                            <View>
                                <Image source={require('../../assests/images/jinda-hostel.jpg')} style={[styles.post_image, { width:deviceWidth,
                                    height: scaleHeight({
                                        source: require('../../assests/images/jinda-hostel.jpg'),
                                        desiredWidth: deviceWidth
                                    })
                                }]}/>
                                <View style={styles.container}>
                                    <Text style={{fontWeight:'bold',fontSize:25}}>{item.name}</Text>
                                    <View style={{flexDirection:'row',paddingVertical:5}}>
                                        <View style={{flex:.1}}>
                                            <Icon name='map-placeholder'color="#ce4061"/>
                                        </View>
                                        <View style={{flex:.9}}>                                    
                                            <Text style={{fontSize:16,fontWeight:'bold'}}>{item.address}{ item.city ? <>, {item.city.name} </> : <></>}{ item.pincode ? <>, {item.pincode} </> : <></>}</Text>
                                        </View>
                                    </View>
                                    <Text style={{fontSize:20,fontWeight:'bold',paddingVertical:5,color:'#838383'}}>About</Text>
                                    <Text style={{fontSize:16,}}>{item.description}</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                )
            } else {
                return(

                    <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                        <ActivityIndicator  color="#ce4061" />
                    </View>
                    )
            }
    }
}