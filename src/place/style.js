import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal:10,
        paddingVertical:10
      },
      imageThumbnail: {
        // justifyContent: 'center',
        // alignItems: 'center',        
        height: 200,
        resizeMode:'contain',
        width:'100%'
      },
      row: {
        flex: 1/3,
        justifyContent: "space-around"
    },
    post_image:{
        // borderTopLeftRadius:5,
        // borderTopRightRadius:5,
    },
    
    
});